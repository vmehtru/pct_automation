package PCTProjects;


import  Common.Log;

import org.testng.SkipException;

import Common.PCTLogin;
import Common.SingleWebDriverObject;


public class LaunchProject {
	
	
	public static void Launch_PCTProjectID(String ProjectID)
	{
		try{
			SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		
			DriverObject.driver.get(""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTUI/PCTHome.aspx?pjtid="+ProjectID);
			Log.info("PCT Project:"+ProjectID+" launched");
		}
		catch(Exception e){
			
			Log.error("Something went wrong while launching PCTProject: "+ProjectID);
			throw new SkipException ("Something went wrong while launching PCTProject: "+ProjectID);
		}

	}

}
