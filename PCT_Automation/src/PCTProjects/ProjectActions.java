package PCTProjects;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;

import Common.Log;
import Common.PCTLogin;
import Common.SingleWebDriverObject;


@SuppressWarnings("deprecation")

public class ProjectActions {
	
	public static String cookies; 
	public static String projName="";
	@BeforeClass
	public static void loginToPCT(String Version) throws IOException
	{
		Log.info("Inside ProjectActions-loginToPCT-Backend");
		try {
		/*Login to PCT*/
		String url = ""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/LogIn.ashx?wsid=&user="+PCTLogin.userName+"&pass="+PCTLogin.pswd+"&proj=&csuser=&mode=none&moduleName="+Version+"";
		
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
	
		 BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
		
			// cookies = conn.getHeaderField("Set-Cookie");
			 Map<String, List<String>> headerFields = conn.getHeaderFields();
	         Set<String> headerFieldsSet = headerFields.keySet();
			 Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();
			 
			 while (hearerFieldsIter.hasNext()) {
				 String headerFieldKey = hearerFieldsIter.next();
			     if ("Set-Cookie".equalsIgnoreCase(headerFieldKey)) {
			    	 List<String> headerFieldValue = headerFields.get(headerFieldKey);
			    	 
			    	 for(int i = 0; i<headerFieldValue.size(); i++)
			    	 {
			    		 if(headerFieldValue.get(i).contains("PCTFORMS"))
			    		 {
			    			 cookies = headerFieldValue.get(i);
			    			 break;
			    		 }
			    	 }
			    	 
			    	 break;
			     }
			 }

			 Log.info("Successfully logged in to PCT- Backend");
		}
			 catch (Exception e) {
				 
				 Log.error("Something went wrong while Logging in to PCT....");
				
				 throw new SkipException("Something went wrong while Logging in to PCT....");
	    }
		
	}
	
	
	
	public static boolean isInteger(String str) {
	    try {
	        Integer.parseInt(str);
	        return true;
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	}
	

	public static String createPCTProject(int AppID, String Project_Name, String Project_Desc) throws IOException
	{
		Log.info("Inside ProjectActions-createPCTProject. \nAppID: "+AppID+" \nProject_Name: "+Project_Name +"\nProject_Desc: "+Project_Desc);
		
		String projID="-1";
		
		 StringBuffer response = new StringBuffer();
		try {
			String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/GetNewProjectID";
			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestMethod("POST");
		    conn.setRequestProperty("Cookie", cookies);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			String urlParameters ="{\"appid\":"+AppID+",\"name\":\""+Project_Name+"\",\"desc\":\""+Project_Desc+"\",\"series\":\"\",\"chapter\":\"\"}";
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(urlParameters);
	        wr.flush();
	        wr.close();
			 
	        BufferedReader  in = new BufferedReader(
				        new InputStreamReader(conn.getInputStream()));
				 
			 
				String inputLine;
				
			while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
		 
			    projID=response.toString().substring(22, response.toString().indexOf(","));
			       if(projID.contains("-1")  || (!(isInteger(projID))))
			    	throw new Exception();
			  }catch (Exception e) 
			  {
				  projID="-1";
				  Log.error("Something went wrong while creating Project: "+Project_Name+"....\n Response: "+response.toString());
				   throw new SkipException("Something went wrong while creating Project: "+Project_Name+"....\n Response: "+response.toString());
	
			    }
			
		Log.info("Project created successfully. Name: "+Project_Name+" ID: "+projID);
		return projID;
	}
	
		
	public static void uploadlDocumentToPCTProject(String ProjectID, String Doc_Path, String DocType) throws IOException
			{
		Log.info("Inside ProjectActions-uploadlDocumentToPCTProject. \n ProjectID: "+ProjectID+" \nDoc_Path: "+Doc_Path +"\nDocType: "+DocType);
		
			StringBuffer response = new StringBuffer(); 
			
			try {
				String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/UploadFile.ashx";
				URL obj = new URL(url);
				HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
				
			
				conn.setRequestMethod("POST");
			    conn.setRequestProperty("Cookie", cookies);
				conn.setDoInput(true);
				conn.setDoOutput(true);
				
			
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("Cache-Control", "no-cache");
				String BOUNDARY = "----WebKitFormBoundaryr574JNN1uj8Prcai"; // Delimiter
				conn.setRequestProperty("Content-Type", "multipart/form-data; boundary="+BOUNDARY);
				
				FileInputStream fileInputStream=null;
				
				File file = new File(Doc_Path);
				FileInputStream is = new FileInputStream(file);
				OutputStream output  = conn.getOutputStream();
				byte[] bFile = new byte[(int) file.length()];
				
				
				try {
			            //convert file into array of bytes
				    fileInputStream = new FileInputStream(file);
				    fileInputStream.read(bFile);
				    fileInputStream.close();
			        }catch(Exception e){
			        	e.printStackTrace();}
			        	
			        	
				 String[] props={"type","projid"};
				 String[] values = {DocType,ProjectID};
				
				
				 StringBuffer sb = new StringBuffer();
				for(int i=0; i<props.length;i++)
				 {
					sb = sb.append("--");
					sb = sb.append(BOUNDARY);
					sb = sb.append("\r\n");
					sb = sb.append("Content-Disposition: form-data; name=\""+ props[i] + "\"\r\n\r\n");
					sb = sb.append(URLEncoder.encode(values[i]));
					sb = sb.append("\r\n");
				 }
				
				// Send the file:
					sb = sb.append("--");
					sb = sb.append(BOUNDARY);
					sb = sb.append("\r\n");
					sb = sb.append("Content-Disposition: form-data; name=\"ProjectAssetsFile\"; filename=\""+Doc_Path+"\"\r\n");
					
					sb = sb.append("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document\r\n\r\n");
				
					byte[] data = sb.toString().getBytes();
					byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
					
				
					// Output:
					output = conn.getOutputStream();
					output.write(data);
					output.write(bFile);
					output.write(end_data);
				
						 
					BufferedReader in = new BufferedReader(
					        new InputStreamReader(conn.getInputStream()));
					
					 response = new StringBuffer();
					 String inputLine;
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					if(response.toString().contains("\"returnData\":null"))
						throw new Exception(); 
							
					
			 } catch (Exception e) {
				 
			  
				  Log.error("Something went wrong while uploading document to PCT Project: "+ProjectID+"\n Response: "+response.toString());
				  throw new SkipException("Something went wrong while uploading document to PCT Project: "+ProjectID+"\n Response: "+response.toString());
	

			    }
			
			Log.info("Successfully uploaded document: "+DocType+" to PCT Project:" +ProjectID);
			  
			 
			}
				
	public static String getFinalDocID(String ProjectID) throws IOException
	{
		Log.info("Inside ProjectActions-getFinalDocID. ProjectID: "+ProjectID);
		
		
		String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/GetDocList";
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestMethod("POST");
	    conn.setRequestProperty("Cookie", cookies);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		
		String urlParameters ="{\"projid\":\""+ProjectID+"\"}";
		
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(urlParameters);
        wr.flush();
        wr.close();
		 
        BufferedReader  in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
			 
		 
		String inputLine;
		StringBuffer response = new StringBuffer();	
		while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
		in.close();
	 
		String docID=response.toString().substring(46, response.toString().indexOf(","));

		 Log.info("Returning final doc Id: "+docID +" for PCT Project: "+ProjectID);
			
		return docID;
		
	}
	
	public static void setMasterProject(String ProjectID) throws IOException
	{
		Log.info("Inside ProjectActions-setMasterProject. ProjectID: "+ProjectID);
		
		
		String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCTManageProjects.asmx/ToggleMasterProject";
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestMethod("POST");
	    conn.setRequestProperty("Cookie", cookies);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		
		String urlParameters ="{\"pjtId\":"+ProjectID+",\"state\":true}";
		
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(urlParameters);
        wr.flush();
        wr.close();
		 
        BufferedReader  in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
			 
		 
		String inputLine;
		StringBuffer response = new StringBuffer();	
		while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
		in.close();
	 
		if(response.toString().contains("{\"d\":\"{\\\"returnData\\\":true,\\\"statusData\\\":{\\\"status\\\":\\\"2\\\",\\\"errormsg\\\":null,\\\"exceptionmsg\\\":null},\\\"updateData\\\":null}\"}"))
		{
			Log.info("ProjectID:"+ProjectID+ "set as Master Project");
		}
		else
		{
			Log.error("ProjectID:"+ProjectID+ " set as Master Project- FAILED");
			 throw new SkipException ("ProjectID:"+ProjectID+ " set as Master Project- FAILED");
			
		}

	}
	
	
	public static void downloadDoc(String ProjectID, String DocID) throws IOException
	{
		Log.info("Inside ProjectActions-downloadDoc. ProjectID: "+ProjectID+" DocID: "+DocID);
		
		/*Download Final Doc*/
		 String url = ""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/DownLoadDoc.ashx?ID="+DocID+"&DocType=1&projectid="+ProjectID;
		 URL obj = new URL(url);
		 HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		 conn.setRequestProperty("Cookie", cookies);
		 int responseCode = conn.getResponseCode();
		 String fileName = "";
		 if (responseCode == HttpURLConnection.HTTP_OK) {
	           
	            String disposition = conn.getHeaderField("Content-Disposition");
	            String contentType = conn.getContentType();
	            int contentLength = conn.getContentLength();
	 
	            if (disposition != null) {
	                // extracts file name from header field
	                int index = disposition.indexOf("filename=");
	                if (index > 0) {
	                    fileName = disposition.substring(index + 10,
	                            disposition.length() - 1);
	                }
	            } 
	 
	         
	           // System.out.println("fileName = " + fileName);
	 
	            // opens input stream from the HTTP connection
	            InputStream inputStream =conn.getInputStream();
	            String saveFilePath = "DownloadedDocs\\" + fileName;
	             
	            // opens an output stream to save into file
	            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
	 
	            int bytesRead = -1;
	       
	            byte[] buffer = new byte[4096];
	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	                outputStream.write(buffer, 0, bytesRead);
	            }
	 
	            outputStream.close();
	            inputStream.close();
	 
	           // System.out.println("File downloaded");
	            Log.info("Successfully downloaded document from PCT Project: "+ProjectID);
				
	        } else {
	           
	            Log.error("Something went wrong while downloading document from PCT Project: "+ProjectID);
	            throw new SkipException("Something went wrong while downloading document from PCT Project: "+ProjectID);
	
	        }
	        conn.disconnect();
	
	}

	public static String testSubmission(String ProjectID, String filePath) throws Exception
		{
		Log.info("Inside ProjectActions-testSubmission. ProjectID: "+ProjectID+" filePath: "+filePath);
		
			String filename=filePath.substring(filePath.lastIndexOf("\\"));
			String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/TestSubmissionUpload.ashx";
			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			
		
			conn.setRequestMethod("POST");
		    conn.setRequestProperty("Cookie", cookies);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
		
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Cache-Control", "no-cache");
			String BOUNDARY = "----WebKitFormBoundaryr574JNN1uj8Prcai"; // Delimiter

			conn.setRequestProperty("Content-Type", "multipart/form-data; boundary="+BOUNDARY);
			
			FileInputStream fileInputStream=null;
			
			File file = new File(filePath);
			FileInputStream is = new FileInputStream(file);
			OutputStream output  = conn.getOutputStream();
			byte[] bFile = new byte[(int) file.length()];
			
			
			  try {
		            //convert file into array of bytes
			    fileInputStream = new FileInputStream(file);
			    fileInputStream.read(bFile);
			    fileInputStream.close();
		 
			    for (int i = 0; i < bFile.length; i++) {
			     //  	System.out.print((char)bFile[i]);
		            }
		 
			   // System.out.println("Done");
		        }catch(Exception e){
		        	e.printStackTrace();}
		        	
		        	
			 String[] props={"projid"};
			 String[] values = {ProjectID};
			
			
			
			
			StringBuffer sb = new StringBuffer();
			
			for(int i=0; i<props.length;i++)
			{
				sb = sb.append("--");
				sb = sb.append(BOUNDARY);
				sb = sb.append("\r\n");
				sb = sb.append("Content-Disposition: form-data; name=\""+ props[i] + "\"\r\n\r\n");
				sb = sb.append(URLEncoder.encode(values[i]));
				sb = sb.append("\r\n");
			}
			
			// Send the file:
				sb = sb.append("--");
				sb = sb.append(BOUNDARY);
				sb = sb.append("\r\n");
				sb = sb.append("Content-Disposition: form-data; name=\"testUpload\"; filename=\""+filename+"\"\r\n\r\n");
				
				//sb = sb.append("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document\r\n\r\n");
				
				byte[] data = sb.toString().getBytes();
				byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
				
			
				// Output:
				output = conn.getOutputStream();
				output.write(data);
				output.write(bFile);
				output.write(end_data);
			
					 
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(conn.getInputStream()));
				 
				StringBuffer response = new StringBuffer();
				String inputLine;
		 
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
		

	//String subID=response.toString().substring(15,response.toString().indexOf("\","));
	String subID=response.toString().substring(62,response.toString().indexOf(",\"skillUIDataOfTheSkillForWhichGradingFailed\""));
							
	Log.info("Test Submission done for PCT Project: "+ProjectID+"\n Returning Submision ID: "+subID);
		
	return subID;
   }
	
   public static void validateScoreCard(String subID, String projectScore, String expectedScore) throws Exception
	{
	   Log.info("Inside ProjectActions-TestSubmission: SubID-"+subID+" ProjectScore- "+projectScore+"Expected Score-"+expectedScore);
		
		int callSummaryReportAgain=1;
		int count=0;
		
		do
		{
				callSummaryReportAgain=0;
				count++;
					
			String  url = ""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/reportingmodule/ScoreCard.aspx?SubmissionID="+subID;
			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			 conn.setRequestProperty("Cookie", cookies);
			 BufferedReader in = new BufferedReader(
				        new InputStreamReader(conn.getInputStream()));
			 
			 StringBuffer response = new StringBuffer();
			 String inputLine;
			 				while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);}
			in.close();
			
			if(response.toString().contains("UNAUTHORIZED REQUEST"))
			{
				Log.error("TestSubmission: UNAUTHORIZED REQUEST");
				
				throw new Exception("TestSubmission: UNAUTHORIZED REQUEST");
			}
			else if(response.toString().contains("grading of your submission is failed"))
			 {
				Log.error("TestSubmission: Grading of your submission is failed");
				
				throw new Exception("TestSubmission: Grading of your submission is failed");
				
			 }
			else if(response.toString().contains("THE SUBMISSION DETAILS COULD NOT BE RETRIEVED"))
			{
				Log.error("TestSubmission: THE SUBMISSION DETAILS COULD NOT BE RETRIEVED");
				
				throw new Exception("TestSubmission: THE SUBMISSION DETAILS COULD NOT BE RETRIEVED");
				
			 }
			else if(response.toString().contains("not yet published, please try again shortly."))
			{
				Log.info("Result not published yet...Calling Report again");	
			 callSummaryReportAgain=1;
			}
			else {
				int p1=response.toString().indexOf("<td class=\"tdCenter\">");
				int p2=response.toString().indexOf("</td>",p1);
				int p3=response.toString().indexOf("<td class=\"tdCenter\">",p2);
				int p4=response.toString().indexOf("</td>",p3);
				
				String tscore=response.substring(p1+"<td class=\"tdCenter\">".length(),p2);
				String oscore=response.substring(p3+"<td class=\"tdCenter\">".length(),p4);
				
				
				if(tscore.trim().equals(projectScore.trim()))
				{
					Log.info("TestSubmission: Total Score on score card matches Project score");
				}
				else
				{
					
					Log.error("TestSubmission: Total Score on score Card not same as Project Score");
					Log.error("Total Score: "+tscore.trim()+"\tProject Score: "+projectScore.trim());

					
				    throw new Exception("TestSubmission: Total Score on score Card not same as Project Score");
				}
				
				if(oscore.trim().equals(expectedScore.trim()))
				{
					Log.info("TestSubmission: Actual Score matches Expected score");

				}
				else
				{
					
					Log.error("TestSubmission: Actual Score does not match Expected score");
					Log.error("Actual Score: "+oscore.trim()+"\tExpected Score: "+expectedScore.trim());
					
					
					throw new Exception("TestSubmission: Actual Score does not match Expected score");
				}
				
			}
			Thread.sleep(1000);
				
		}while(callSummaryReportAgain==1 && count<180);
		
		if(count>=100)
		{
			Log.error("TestSubmission: Result not yet published.");
			throw new Exception("TestSubmission: Result not yet published.");
		}
	}
	
		
   
	
   @SuppressWarnings("deprecation")
public static void validateSummaryReportFeedbackText(String subID, String expecetdResponse) throws Exception
	{
	   Log.info("Inside ProjectActions-validateSummaryReportFeedbackText for subID: "+subID);
	   int callSummaryReportAgain=1;
		int count=0;
		
		do
		{
				callSummaryReportAgain=0;
				count++;
				
					
			String  url = ""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/reportingmodule/SummaryReport.aspx?SubmissionID="+subID;
			 //url = ""+PCTLogin.url_Ext+"://"+server_Name+"//ReportingModule/summaryreport.aspx?submissionid="+subID;
			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			conn.setRequestProperty("Cookie", cookies);
			 BufferedReader in = new BufferedReader(
				        new InputStreamReader(conn.getInputStream()));
			 
			 StringBuffer response = new StringBuffer();
			 String inputLine;
			 				while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);}
			in.close();
			
			
			if(response.toString().contains("UNAUTHORIZED REQUEST"))
			{
			
				Log.error("UNAUTHORIZED REQUEST");
				
				throw new Exception("UNAUTHORIZED REQUEST");
			}
			else if(response.toString().contains("grading of your submission is failed"))
			 {
				
				Log.error("Grading of your submission is failed");
				
				throw new Exception("Grading of your submission failed");
				
			 }
			else if(response.toString().contains("THE SUBMISSION DETAILS COULD NOT BE RETRIEVED"))
			{
				Log.error("THE SUBMISSION DETAILS COULD NOT BE RETRIEVED");
				
				throw new Exception("THE SUBMISSION DETAILS COULD NOT BE RETRIEVED");
				
			 }
			else if(response.toString().contains("not yet published, please try again shortly."))
			{
				Log.info("Result not published yet...Calling Report again");
			 callSummaryReportAgain=1;
			}
			else 
			{
				File f = new File("tempSummary.htm");
				f.delete();
				
				PrintWriter out1 = new PrintWriter(new BufferedWriter(new FileWriter("tempSummary.htm", true)));
				out1.println(response.toString());
				out1.close();

				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("tempSummary.htm")));
				String myText = null;
				String line = null;
				while ((line = reader.readLine()) != null) {
				    myText = line;
				}

				//toCompareActual=myText; 
				
				String toCompareActual=myText.substring(myText.indexOf("id=\"Table01\""),myText.indexOf("JavaScript:window.close()")).replace("\\s","").replaceAll("\\u00a0"," ").trim();
				String toCompareExpected=expecetdResponse.substring(expecetdResponse.toString().indexOf("id=\"Table01\""),expecetdResponse.toString().indexOf("JavaScript:window.close()")).replace("\\s","").replaceAll("\\u00a0"," ").trim();
				
								
				if(toCompareActual.equals(toCompareExpected))			
					Log.info("Summary report  matched");
				else
					{
					Log.error("Summary report doesn't match");
					Log.error("Expected: \n"+toCompareExpected);
					Log.error("Actual: \n"+toCompareActual);
					Assert.failNotEquals("SummaryReport doesn't match",toCompareExpected, toCompareActual);

					}
			}
			Thread.sleep(1000);
				
		}while(callSummaryReportAgain==1 && count<180);
		
		if(count>=100)
		{
			Log.error("TestSubmission: Result not yet published.");
			throw new Exception("TestSubmission: Result not yet published.");
		}
	}
	
   public static void deletePCTProject(String ProjectID)
	{
		
	}
	
	
	public static String clonePCTProject(String ProjectID) throws IOException
	{
		 Log.info("Inside ProjectActions-clonePCTProject for ProjectID: "+ProjectID);
		  
		 String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/CloneProject";
			
		 URL obj = new URL(url);
		 HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		 conn.setRequestProperty("Content-Type", "application/json");
		 conn.setRequestProperty("Accept", "application/json");
		
		conn.setRequestMethod("POST");
	    conn.setRequestProperty("Cookie", cookies);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		String urlParameters ="{\"srcprojid\":"+ProjectID+",\"name\":\""+ProjectID+"_Cloned_"+Calendar.getInstance().getTime().toString()+"\",\"desc\":\" \",\"series\":\"\",\"chapter\":\"\"}";
		
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(urlParameters);
        wr.flush();
        wr.close();
		 
        BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
		String inputLine;
        StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 

			int i=response.toString().indexOf("returnData");
				String ClonedProjectID=response.toString().substring(22,response.toString().indexOf(","));
				
				Log.info("ProjectID: "+ProjectID +"cloned successfully by "+ClonedProjectID);
				 
			return (ClonedProjectID);
			
		
	}
	
	public static String reUploadProjectFinalDocAndGetNewID(String ProjectID) throws IOException
	{
		Log.info("Inside ProjectActions-reUploadProjectFinalDocAndGetNewID for projectID:"+ProjectID);
		
		String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/ReUploadProjectFinalDocAndGetNewID";
		
		
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestMethod("POST");
	    conn.setRequestProperty("Cookie", cookies);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		 String urlParameters ="{\"projId\":\""+ProjectID+"\"}";
		
		 OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		 wr.write(urlParameters);
         wr.flush();
         wr.close();
		 
        BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
			 
        StringBuffer response = new StringBuffer();
        String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
	
			int i=response.toString().indexOf("returnData");
			String newfinalDocID=response.toString().substring(i+13,response.toString().indexOf(","));
			
			Log.info("Successfully reUploaded ProjectFinalDocAnd got NewID: "+newfinalDocID+" for projectID:"+ProjectID);
			return newfinalDocID;
			
	}
	

	public static void QueryFinalDocStatus(String finalDocID) throws IOException
	{
		Log.info("Inside ProjectActions-QueryFinalDocStatus for finalDocID:"+finalDocID);
		
		String newfinalDocStatus="INQUEUE";
		 do
		 {
			String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/QueryFinalDocStatus";
			
			
			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			

				conn.setRequestProperty("Content-Type", "application/json");

				conn.setRequestProperty("Accept", "application/json");

				
			conn.setRequestMethod("POST");
		    conn.setRequestProperty("Cookie", cookies);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			String urlParameters ="{\"FInalDocID\":\""+finalDocID+"\"}";
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(urlParameters);
	        wr.flush();
	        wr.close();
			 
	        BufferedReader in = new BufferedReader(
				        new InputStreamReader(conn.getInputStream()));
				 
	        StringBuffer response = new StringBuffer();
		    String inputLine;
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				 int i=response.toString().indexOf("status");
				 newfinalDocStatus=response.toString().substring(i+11,response.toString().indexOf(","));
			
		 }while(newfinalDocStatus.contains("INQUEUE")|| newfinalDocStatus.contains("INPROC"));
		 Log.info("Returning with FinalDoc Status: "+newfinalDocStatus+" for ID:"+finalDocID);
			
	}
	
	public static void upgradeProject(String ProjectID) throws IOException
	{
		Log.info("Inside ProjectActions-upgradeProject for ProjectID: "+ProjectID);
		
		String newfinalDocID=reUploadProjectFinalDocAndGetNewID(ProjectID);
		/*Get Status*/	
		QueryFinalDocStatus(newfinalDocID);
		
		String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/UpgradeProjectAndItsSkillsAndApplyFinalDocument";
			 URL obj = new URL(url);
			 HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			 conn.setRequestProperty("Content-Type", "application/json");
			 conn.setRequestProperty("Accept", "application/json");
			 conn.setRequestMethod("POST");
			 conn.setRequestProperty("Cookie", cookies);
			 conn.setDoInput(true);
			 conn.setDoOutput(true);
			
			 String urlParameters ="{\"projId\":\""+ProjectID+"\",\"finalDocID\":"+newfinalDocID+"}";
				
			 OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(urlParameters);
		        wr.flush();
		        wr.close();
				 
		        BufferedReader in = new BufferedReader(
					        new InputStreamReader(conn.getInputStream()));
					 
		        StringBuffer response = new StringBuffer();
		        String inputLine;
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					
					
					Log.info("Launching Project to map Skills. ProjectID: "+ProjectID);
					
					SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
					DriverObject.driver.get(""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/pctui/PCTHome.aspx?pjtid="+ProjectID);
					WebDriverWait wait= new WebDriverWait(DriverObject.driver,180);
					wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("ApplicationName")));
				
					
		
	}


	
	public static void reportSkillsinWarningMode(String ProjectID) throws IOException
	{
		Log.info("Inside ProjectActions-reportSkillsinWarningMode for ProjectID: "+ProjectID);
		
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		List<WebElement>  list_skillsInWarning=DriverObject.driver.findElements(By.xpath(".//*[@class='skillstatus skillstatus_WARNING']/../div[@class='SkillElement']//div[@class='skillUItitlearea']/p/span"));
		ListIterator<WebElement> list_skillsInWarning_Itr=list_skillsInWarning.listIterator();
		
		if(list_skillsInWarning.size()==0)
		{
			Log.info(ProjectID+"->No Skill Opened in Warning mode");

		}
		
		while(list_skillsInWarning_Itr.hasNext())
		{
		String SkillName=list_skillsInWarning_Itr.next().getText();
		Log.info("Warning mode: "+SkillName);
		
		} 
		
	}
	public static void reportSkillsinErrorMode(String ProjectID) throws IOException
	{
		Log.info("Inside ProjectActions-reportSkillsinErrorMode for ProjectID: "+ProjectID);
		
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		List<WebElement>  list_skillsInError=DriverObject.driver.findElements(By.xpath(".//*[@class='skillstatus skillstatus_ERROR']/../div[@class='SkillElement']//div[@class='skillUItitlearea']/p/span"));
		ListIterator<WebElement> list_skillsInError_Itr=list_skillsInError.listIterator();
		
		if(list_skillsInError.size()==0)
		{
			Log.info(ProjectID+"->No Skill Opened in Error mode");
	
		}
		
		while(list_skillsInError_Itr.hasNext())
		{
		String SkillName=list_skillsInError_Itr.next().getText();
		Log.error("ProjectId: "+ProjectID+" Skill in Error mode: "+SkillName);
		
		} 
	}
	

	public static void reportSkillsinOpenState(String ProjectID) throws IOException
	{
		Log.info("Inside ProjectActions-reportSkillsinOpenState for ProjectID: "+ProjectID);
		
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		List<WebElement>  list_skillsInOpenState=DriverObject.driver.findElements(By.xpath(".//div[@class='skillUItitlearea']/p/span"));
		ListIterator<WebElement> list_skillsInOpenState_Itr=list_skillsInOpenState.listIterator();
		
		if(list_skillsInOpenState.size()==0)
		{
			Log.info(ProjectID+"->No Skill Opened in Open State");

		}
		
		while(list_skillsInOpenState_Itr.hasNext())
		{
		String SkillName=list_skillsInOpenState_Itr.next().getText();
		Log.error("ProjectId: "+ProjectID+" Skill in Open State: "+SkillName);
		
		} 
	}
	
	
	public static void saveSkillsinWarningMode(String ProjectID) throws IOException, InterruptedException
	{
			Log.info("Inside ProjectActions- saveSkillsinWarningMode for ProjectID: "+ProjectID);
		
			SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
			List<WebElement>  list_skillsInWarning=DriverObject.driver.findElements(By.xpath(".//*[@class='skillstatus skillstatus_WARNING']/../div[@class='SkillCancelSaveGroup']/div[@class='OkDiv']/div"));
			ListIterator<WebElement> list_skillsInWarning_Itr=list_skillsInWarning.listIterator();
			
			if(list_skillsInWarning.size()==0)
			{
				

			}
			
			while(list_skillsInWarning_Itr.hasNext())
			{
			WebElement ele=list_skillsInWarning_Itr.next(); 
			Actions actions= new Actions(DriverObject.driver);
			actions.moveToElement(ele);
			actions.perform();
			Thread.sleep(000);
			ele.click();	
			
			Log.info("ProjectID: "+ProjectID+" WarningMode skills Saved");
			
			} 
			
	}
	
	public static void deleteSkillsinErrorMode(String ProjectID) throws IOException, InterruptedException
	{
		Log.info("Inside ProjectActions-deleteSkillsinErrorMode for ProjectID: "+ProjectID);
			
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
			List<WebElement>  list_skillsInError=DriverObject.driver.findElements(By.xpath(".//*[@class='skillstatus skillstatus_ERROR']/../div[@class='SkillCancelSaveGroup']/div[@class='CancelDiv']/div"));
			ListIterator<WebElement> list_skillsInError_Itr=list_skillsInError.listIterator();
			
			if(list_skillsInError.size()==0)
			{
				PrintWriter out1 = new PrintWriter(new BufferedWriter(new FileWriter("Output_ProjectUpgrade.csv", true)));
				 out1.println("->No Skill Opened in Error mode");
				 out1.close();
			}
			
			while(list_skillsInError_Itr.hasNext())
			{
				WebElement ele=list_skillsInError_Itr.next(); 
				Actions actions= new Actions(DriverObject.driver);
				actions.moveToElement(ele);
				actions.perform();
				Thread.sleep(000);
				ele.click();	
				
				Log.info("ProjectID: "+ProjectID+" Error Mode skills Deleted");
			} 
		
			
	}
	

	public static int publishCheckpoints(String ProjectID) throws Exception
	{
		Log.info("Inside ProjectActions-publishCheckpoints for ProjectID: "+ProjectID);
		
		String url = ""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/GetPublishConfigData";
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestMethod("POST");
		    conn.setRequestProperty("Cookie", cookies);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			String urlParameters ="{\"projID\":"+ProjectID+"}";
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(urlParameters);
	        wr.flush();
	        wr.close();
	        BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
		    String inputLine;
	        StringBuffer response = new StringBuffer();
		 				while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);}	
		in.close();
		
		if(response.toString().contains("ERROR"))
		{
			Log.error("Publish Checks failed for ProjectID: "+ProjectID);
			
			return 0;
		}
		
		else
			{
			Log.info("Publish Checks passed for ProjectID: "+ProjectID);
			
			return 1;
			}
	}
	
	public static String publish(String ProjectID) throws Exception
	{
		Log.info("Inside ProjectActions-publish for ProjectID: "+ProjectID);
		
		int status = publishCheckpoints(ProjectID);
		if(status==1)
		{
		String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/LMSPublish";
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			 conn.setRequestProperty("Content-Type", "application/json");
			 conn.setRequestProperty("Accept", "application/json");
				conn.setRequestMethod("POST");
			    conn.setRequestProperty("Cookie", cookies);
				conn.setDoInput(true);
				conn.setDoOutput(true);
				String urlParameters ="{\"projId\":"+ProjectID+",\"deprecateId\":-1}";
				
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(urlParameters);
		        wr.flush();
		        wr.close();
				 
		        BufferedReader  in = new BufferedReader(
					        new InputStreamReader(conn.getInputStream()));
					 
		        StringBuffer response = new StringBuffer();
		        String inputLine;
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					
			String PublishedID=response.toString().substring(response.toString().indexOf("varid")+8,response.toString().indexOf(","));
			PrintWriter out1 = new PrintWriter(new BufferedWriter(new FileWriter("Output_ProjectUpgrade.csv", true)));
			out1.println("\nPublished ID:"+PublishedID);
			out1.close();
			
			Log.info("ProjectID: "+ProjectID+" published with PublishedID:"+PublishedID);
			
			return PublishedID;
		}
		else
			return "-1";
		
	}
	
	public static void  PCTShareProject_View(String ProjectID, String Project_Name) throws Exception
	{
		 Log.info("Inside ProjectActions-PCTShareProject for ProjectID: "+ProjectID);
		  
		 String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/ShareProjectWithUsers";
			
		 URL obj = new URL(url);
		 HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		 conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		 conn.setRequestProperty("Accept", "pplication/json, text/plain, */*");
		 conn.setRequestProperty("contentType", "application/json");
		 conn.setRequestMethod("POST");
		 conn.setRequestProperty("Cookie", cookies);
		 conn.setDoInput(true);
		 conn.setDoOutput(true);
		String urlParameters = "{\"inviteOptions\":\"{\\\"userList\\\":[{\\\"nameemail\\\":\\\"WS3 User\\\",\\\"name\\\":\\\"WS3 User\\\",\\\"email\\\":null,\\\"id\\\":5}],\\\"roleName\\\":\\\"View\\\",\\\"roleId\\\":1,\\\"notifyByEmail\\\":false,\\\"message\\\":\\\"\\\",\\\"projectId\\\":"+ProjectID+",\\\"projectName\\\":\\\""+Project_Name+"\\\",\\\"userWithEmailAdded\\\":false,\\\"pctProjectLink\\\":\\\""+PCTLogin.url_Ext+"://gradertest/PCTUI/LaunchProject.aspx?pjtid=MTAyNDU=\\\"}\"}";
	     OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(urlParameters);
        wr.flush();
        wr.close();
		 
        BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
		String inputLine;
        StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	}
	
	public static void PCTShareProject_Edit(String ProjectID, String Project_Name) throws Exception
	{
		 Log.info("Inside ProjectActions-PCTShareProject for ProjectID: "+ProjectID);
		  
		 String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/PCT_Methods.asmx/ShareProjectWithUsers";
			
		 URL obj = new URL(url);
		 HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		 conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		 conn.setRequestProperty("Accept", "pplication/json, text/plain, */*");
		 conn.setRequestProperty("contentType", "application/json");
		 conn.setRequestMethod("POST");
		 conn.setRequestProperty("Cookie", cookies);
		 conn.setDoInput(true);
		 conn.setDoOutput(true);
		String urlParameters = "{\"inviteOptions\":\"{\\\"userList\\\":[{\\\"nameemail\\\":\\\"WS3 User\\\",\\\"name\\\":\\\"WS2 User\\\",\\\"email\\\":null,\\\"id\\\":4}],\\\"roleName\\\":\\\"Edit\\\",\\\"roleId\\\":2,\\\"notifyByEmail\\\":false,\\\"message\\\":\\\"\\\",\\\"projectId\\\":"+ProjectID+",\\\"projectName\\\":\\\""+Project_Name+"\\\",\\\"userWithEmailAdded\\\":false,\\\"pctProjectLink\\\":\\\""+PCTLogin.url_Ext+"://gradertest/PCTUI/LaunchProject.aspx?pjtid=MTAyNDU=\\\"}\"}";
	     OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(urlParameters);
        wr.flush();
        wr.close();
		 
        BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
		String inputLine;
        StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	}


public static String ExportPCTProject(String ProjectID, String Project_Name) throws IOException
{
	
	 String Project_Name1 = URLEncoder.encode(Project_Name, "UTF-8");
	
	 String url = ""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/ImportExportProject.ashx?Mode=Export&projectid="+ProjectID+"&projectName="+Project_Name1;
	 URL obj = new URL(url);
	 HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
	 conn.setRequestMethod("GET");
	 conn.setRequestProperty("Cookie", cookies);
	 int responseCode = conn.getResponseCode();
	 String fileName = "";
	 if (responseCode == HttpURLConnection.HTTP_OK) {
            String disposition = conn.getHeaderField("Content-Disposition");
            String contentType = conn.getContentType();
            int contentLength = conn.getContentLength();
            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } 
            InputStream inputStream =conn.getInputStream();
            String saveFilePath = "DownloadedDocs\\" + fileName;
             
            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
            int bytesRead = -1;
            byte[] buffer = new byte[4096];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
           
            outputStream.close();
            inputStream.close();
           
        } else {
           
            Log.error("Something went wrong while export PCT Project: "+ProjectID);
            throw new SkipException("Something went wrong while export PCT Project: "+ProjectID);

        }
        conn.disconnect();
        return(fileName);
        
	
}
public static void ImportPCTProject(String ProjectID, String filePath,String Project_Name) throws Exception
{ 
	Thread.sleep(3000);
	StringBuffer response = new StringBuffer(); 
	String filePath1="DownloadedDocs\\" + filePath;
	try{
	String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/ImportExportProject.ashx?mode=ImportOverExistingProject";
	URL obj = new URL(url);
	HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
	conn.setRequestMethod("POST");
   conn.setRequestProperty("Cookie", cookies);
	conn.setDoInput(true);
	conn.setDoOutput(true);
	

	conn.setRequestProperty("Connection", "Keep-Alive");
	//conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");	
	String BOUNDARY = "----WebKitFormBoundaryr574JNN1uj8Prcai"; // Delimiter
	conn.setRequestProperty("Content-Type", "multipart/form-data; boundary="+BOUNDARY);
	FileInputStream fileInputStream=null;
	
	File file = new File(filePath1);
	FileInputStream is = new FileInputStream(file);
	OutputStream output  = conn.getOutputStream();
	byte[] bFile = new byte[(int) file.length()];
	
	
	try {
           //convert file into array of bytes
	    fileInputStream = new FileInputStream(file);
	    fileInputStream.read(bFile);
	    fileInputStream.close();

	    for (int i = 0; i < bFile.length; i++) {
	      //	System.out.print((char)bFile[i]);
           }
       }
	catch(Exception e){
       	e.printStackTrace();}
       	
       	
	 String[] props={"projectName","projectId","overWriteProjectName","overWriteProjectDescription","overWriteProjectTags","migrate"};
	 String[] values = {Project_Name,ProjectID,"false","false","false","false"};
	
	
	 StringBuffer sb = new StringBuffer();
	 StringBuffer sb1 = new StringBuffer();
	for(int i=0; i<props.length;i++)
	 {
		sb1 = sb1.append("--");
		sb1 = sb1.append(BOUNDARY);
		sb1 = sb1.append("\r\n");
		sb1 = sb1.append("Content-Disposition: form-data; name=\""+ props[i] + "\"\r\n\r\n");
		if (i==0)
			{
				sb1 = sb1.append(values[i]);
			}
			else
			{
				sb1 = sb1.append(URLEncoder.encode(values[i]));
			}
		//sb = sb.append(URLEncoder.encode(values[i]));
		sb1 = sb1.append("\r\n");
	 }
	//System.out.println(sb);
	// Send the file:
		sb = sb.append("--");
		sb = sb.append(BOUNDARY);
		sb = sb.append("\r\n");
		sb = sb.append("Content-Disposition: form-data; name=\"file\"; filename=\""+filePath+"\"\r\n");
		
		sb = sb.append("Content-Type: application/octet-stream\"\r\n");
		sb = sb.append("\r\n");
		byte[] data = sb.toString().getBytes();
		byte[] data1 = sb1.toString().getBytes();
		byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
		
		// Output:
		output = conn.getOutputStream();

		output.write(data);
		output.write(bFile);
		output.write(data1);
		output.write(end_data);
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(conn.getInputStream()));
		
		 response = new StringBuffer();
		 String inputLine;
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		System.out.println(response);
		in.close();
		if(response.toString().contains("status\":\"2"))
		{}
		else{
			throw new Exception(); }
				
	 }
	 catch (Exception e) {
	 
 
	  Log.error("Something went wrong while uploading document to PCT Project: "+ProjectID+"\n Response: "+response.toString());
	  throw new SkipException("Something went wrong while uploading document to PCT Project: "+ProjectID+"\n Response: "+response.toString());


   }}
public static String ImportPCTProjectDefault(String filePath, String Migrate_Value, String Project_Name) throws Exception
{ 
	Thread.sleep(3000);
	String Project=Project_Name+Calendar.getInstance().getTime().toString();
	StringBuffer response = new StringBuffer(); 
	try{
	String url=""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/PCTInterface/ImportExportProject.ashx?mode=Import";
	URL obj = new URL(url);
	HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
	conn.setRequestMethod("POST");
   conn.setRequestProperty("Cookie", cookies);
	conn.setDoInput(true);
	conn.setDoOutput(true);
	

	conn.setRequestProperty("Connection", "Keep-Alive");
	String BOUNDARY = "----WebKitFormBoundaryr574JNN1uj8Prcai"; // Delimiter
	conn.setRequestProperty("Content-Type", "multipart/form-data; boundary="+BOUNDARY);
	FileInputStream fileInputStream=null;
	
	File file = new File(filePath);
	FileInputStream is = new FileInputStream(file);
	OutputStream output  = conn.getOutputStream();
	byte[] bFile = new byte[(int) file.length()];
	
	
	try {
           //convert file into array of bytes
	    fileInputStream = new FileInputStream(file);
	    fileInputStream.read(bFile);
	    fileInputStream.close();

	    for (int i = 0; i < bFile.length; i++) {
	      //	System.out.print((char)bFile[i]);
           }
       }
	catch(Exception e){
       	e.printStackTrace();}
       	
       	
	 String[] props={"projectName","Migrate"};
	 String[] values = {Project,Migrate_Value};
	
	
	 StringBuffer sb = new StringBuffer();
	 StringBuffer sb1 = new StringBuffer();
	for(int i=0; i<props.length;i++)
	 {
		sb1 = sb1.append("\r\n");
		sb1 = sb1.append("--");
		sb1 = sb1.append(BOUNDARY);
		sb1 = sb1.append("\r\n");
		sb1 = sb1.append("Content-Disposition: form-data; name=\""+ props[i] + "\"\r\n\r\n");
		if (i==0)
			{
				sb1 = sb1.append(values[i]);
			}
			else
			{
				sb1 = sb1.append(URLEncoder.encode(values[i]));
			}
		sb1 = sb1.append("\r\n");
	 }
	// Send the file:
		sb = sb.append("--");
		sb = sb.append(BOUNDARY);
		sb = sb.append("\r\n");
		sb = sb.append("Content-Disposition: form-data; name=\"file\"; filename=\""+filePath+"\"\r\n");
		
		sb = sb.append("Content-Type: application/octet-stream\"\r\n");
		sb = sb.append("\r\n");
		byte[] data = sb.toString().getBytes();
		byte[] data1 = sb1.toString().getBytes();
		byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
		
		// Output:
		output = conn.getOutputStream();

		output.write(data);
		output.write(bFile);
		output.write(data1);
		output.write(end_data);
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(conn.getInputStream()));
		
		 response = new StringBuffer();
		 String inputLine;
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		String ProjId= response.toString().substring(27, response.toString().indexOf(","));
		System.out.println(response);
		in.close();
		
		if(response.toString().contains("status\":\"2"))
		{}
		else{
			throw new Exception(); }
		ProjId = ProjId.replaceAll("\\p{P}","");
		//System.out.println(ProjId);
		return ProjId;	
		
	 }
	 catch (Exception e) {
	 
 
	  Log.error("Something went wrong while Import PCT Project Response: "+response.toString());
	  throw new SkipException("Something went wrong while Import PCT ProjectResponse: "+response.toString());


   }

}
}