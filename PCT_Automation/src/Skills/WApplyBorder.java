package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WApplyBorder extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Apply Border "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Aastha";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Apply Borders";
	 super.FinalDoc="Border.docx";
	 super.StartingDoc="Border.docx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=6;

	 super.Setup("WApplyBorder");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Apply_Borders);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		
		if(Selector1.equals("Table"))
		{super.selectValueFromDropDown(Selector.Table,Selector2);
		}
		else if(Selector1.equals("Cell"))
		{super.selectValueFromDropDown(Selector.Table,Selector2);
		super.selectValueFromMultiSelect(Selector.Cell,Selector3);
		}
		else
		{super.selectValueFromMultiSelect(Selector.Para,Selector2);
		}
		
		super.selectAllProperties_groupcheckbox();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
