package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EPowerViewMapFields extends PCTSkill {


		@BeforeClass
		public void Setup() throws Exception
		{
		 super.App="Excel";
		 super.Project_Name="Excel - Power View Map Fields "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Rupsi";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Power View Map Fields";
		 super.FinalDoc="Power View Map Fields.xlsx";
		 super.StartingDoc="Power View Map Fields.xlsx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=7;
		 super.excelColCount=6;
		 super.Setup("EPowerViewMapFields");

		}


		@Test(dataProvider="dp", groups= {"testCase","Mar15"})
		public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
		{
			String instNo=super.addInstructiontoPCTProject();
			super.addSkill(instNo, SkillCategory.POWER_VIEW, SkillName.Excel.Power_View_Map_Fields);
			super.selectValueFromDropDown(Selector.Sheet,Selector1);
			super.selectValueFromDropDown(Selector.Map,Selector2);
			super.selectValueFromMultiSelect(Selector.Entity,Selector3);

			saveSkill();
			super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		}

		@Test(groups= {"testSubmission"})
		public void projectScoreAndFeedback() throws Exception
		{
			testPerfectDocAndScore();
			testStartingDocAndFeedback();
		}

                @Test(groups= {"saveProject"})
	        public void saveProject() throws Exception
	        {
		        setMasterProjectAndSaveProjectID();
	         }

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}


}
