package Skills;

import java.util.Calendar;
import Skills.PCTSkill;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WPageBackground_PageColour extends PCTSkill {
	@BeforeClass
	public void Setup1() throws Exception
	{
		super.App="Word";
		super.SkillDocsPath="PCT_Skill_Documents\\Word\\Page Background - Page Color";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=5;
		super.excelColCount=6;
	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases(String Iteration_no,String FinalDocument,String StartDocument,String SummaryFile, String ValidateOn, String ValidateFor) throws Exception
	{
		super.Project_Name="Word Page Background-Page Color"+Calendar.getInstance().getTime().toString();
		super.Project_Desc=" by Palak";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WPageBackground_PageColor");

		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DESIGN, SkillName.Word.Page_Background_Page_Color);

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);

		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
