package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PProtectPresentation extends PCTSkill {

	@BeforeClass
	public void TestCase_1() throws Exception
	{
		super.App="PPT";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Protect Presentation";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=2;
		super.excelColCount=6;


	};

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCase_v15(String Iteration_no,String FinalDocument,String StartDocument, String ValidateOn, String ValidateFor, String SummaryFile) throws Exception
	{
		super.Project_Name="PPT - Protect Presentation "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Ajay";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("PProtectPresentation");
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.FILE, SkillName.PPT.Protect_Presentation);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		testPerfectDocAndScore();

		testStartingDocAndFeedback(SummaryFile);
        setMasterProjectAndSaveProjectID();

	}



}
