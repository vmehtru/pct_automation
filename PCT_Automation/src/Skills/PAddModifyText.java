package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PAddModifyText extends PCTSkill
{
		@BeforeClass
		public void Setup() throws Exception
		{
			super.App="PPT";
			super.Project_Name="PPT - Add/Modify Text"+Calendar.getInstance().getTime().toString();
			super.Project_Desc="Created by Prateek";
			super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Add Modify Text";
			super.FinalDoc="AddModifyText.pptx";
			super.StartingDoc="AddModifyText.pptx";
			super.excelDataSourcePath="DataSource.xlsx";
			super.excelSheetname="Sheet1";
			super.excelRowCount=4;
			super.excelColCount=8;
			super.Setup("PAdd/ModifyText");
			
		}
			
		@Test(dataProvider="dp", groups= {"testCase"})
		public void TestCases_v15(String Iteration_no,String Selector1,String Selector2 , String Selector3, String Selector4,String Selector5, String ValidateOn, String ValidateFor) throws Exception
		{
			String instNo=super.addInstructiontoPCTProject();
			super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Add_Modify_Text);
			super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
			if(Selector1.equals("Slides"))
			{
				super.selectValueFromDropDown(Selector.Slide,Selector2);	
				super.selectValueFromDropDown(Selector.Shape,Selector3);	
				super.selectValueFromMultiSelect(Selector.Para,Selector4);
			}
			
			if(Selector1.equals("Slide Layout"))
			{
				super.selectValueFromDropDown(Selector.Slide_Master,Selector2);
				super.selectValueFromDropDown(Selector.Slide_Layout,Selector3);
		     	super.selectValueFromDropDown(Selector.Shape,Selector4);
		     	super.selectValueFromMultiSelect(Selector.Para,Selector5);
			}
			if(Selector1.equals("Slide Master"))
			{
				super.selectValueFromDropDown(Selector.Slide_Master,Selector2);	
				super.selectValueFromDropDown(Selector.Shape,Selector3);	
				super.selectValueFromMultiSelect(Selector.Para,Selector4);
			}
			super.selectAllProperties();
			saveSkill();
			super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		} 
		
		@Test(groups= {"testSubmission"})
		public void testScoreAndFeedback() throws Exception
		{
			testPerfectDocAndScore();
			testStartingDocAndFeedback();
		}
	        
	        @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
}
	
