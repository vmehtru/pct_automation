package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EInsertWorksheet extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Insert Worksheet "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By DhruvD";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Insert Worksheet";
	 super.FinalDoc="Insert Worksheet.xlsx";
	 super.StartingDoc="Insert Worksheet.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=4;

	 super.Setup("EInsertWorksheet");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Insert_Worksheet);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);

		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

    @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
