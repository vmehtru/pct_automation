package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class EDataValidation extends PCTSkill  {
	
	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Excel";
		 super.Project_Name="Excel - Data Validation"+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Rupsi";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Data Validation";
		 super.FinalDoc="Data Validation.xlsx";
		 super.StartingDoc="Data Validation.xlsx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=3;
		 super.excelColCount=5;
		 super.Setup("EDataValidation");
		
	}
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, PCTSkill.SkillCategory.DATA, PCTSkill.SkillName.Excel.Data_Validation);
		super.selectValueFromDropDown(PCTSkill.Selector.Sheet,Selector1);
		//Deselecting the default selected control
		super.selectValueFromMultiSelectByIndex(Selector.Data, "1");
		super.selectValueFromMultiSelect(Selector.Data,Selector2);
		super.selectAllProperties();

		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	
	}
