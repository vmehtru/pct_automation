package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WInsertBookmark extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Word";
		 super.Project_Name="Word - Insert Bookmark "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Manas";
		 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Bookmark";
		 super.FinalDoc="bookmark.docx";
		 super.StartingDoc="bookmark.docx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=2;
		 super.excelColCount=4;

		 super.Setup("WInsertBookmark");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Word.Insert_Bookmark);

	selectValueFromMultiSelect("Bookmark:", Selector1);

		selectAllProperties();
		saveSkill();
		validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
