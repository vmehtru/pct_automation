package Skills;




import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PInsertSlideMaster extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - 	Insert Slide Master "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Insert Slide Master";
		super.FinalDoc="Insert Slide Master.pptx";
		super.StartingDoc="Insert Slide Master.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=4;
		super.Setup("PInsertSlideMaster");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SLIDE_MASTER, SkillName.PPT.Insert_Slide_Master);
		super.selectValueFromMultiSelect(Selector.Slide,Selector1);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
