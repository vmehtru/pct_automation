package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WEnvelopeOptions extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="Word";
	 super.Project_Name="Word - Envelope Options"+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created by Prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Envelope Options";
	 super.FinalDoc="testcase.docx";
	 super.StartingDoc="testcase.docx";
	 
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=4;
	

	 super.Setup("WEnvelopeOptions");
}	
	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.MAILINGS, SkillName.Word.Envelope_Options);
		
		super.selectValueFromMultiSelect(Selector.Section,Selector1);
	
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
}
