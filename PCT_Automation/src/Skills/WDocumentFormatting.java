package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WDocumentFormatting extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Word";
		super.SkillDocsPath="PCT_Skill_Documents\\Word\\Document Formatting";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=6;
	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases_v15(String Iteration_no,String FinalDocument,String StartDocument,String SummaryFile, String ValidateOn, String ValidateFor) throws Exception
	{
		super.Project_Name="Word- Document Formatting "+Calendar.getInstance().getTime().toString();
		super.Project_Desc=" By DhruvD";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WDocumentFormatting");
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DESIGN, SkillName.Word.Document_Formatting);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		testPerfectDocAndScore();

		testStartingDocAndFeedback(SummaryFile);
                setMasterProjectAndSaveProjectID();


	}



@Test(groups= {"upgrade"})
public void testUpgrade() throws Exception
{
	getSavedProjectIDsForSkillandUpgrade();
}

}
