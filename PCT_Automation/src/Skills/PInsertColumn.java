package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PInsertColumn extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Insert Column "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="DhruvD";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Insert Column";
		super.FinalDoc="Insert Column.pptx";
		super.StartingDoc="Insert Column.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=5;
		super.Setup("PInsertColumn");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.TABLE, SkillName.PPT.Insert_Column);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.Table,Selector2);
		super.selectAllFromMultiSelect(Selector.Column);
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
