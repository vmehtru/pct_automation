package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PActionSettings extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="PPT";
	 super.Project_Name="PPT - Action Settings "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Palak";
	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Action Settings";
	 super.FinalDoc="ActionSettings.pptx";
	 super.StartingDoc="ActionSettings.pptx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";	
	 super.excelRowCount=3;
	 super.excelColCount=5;
	 super.Setup("PActionSettings");

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Action_Settings);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromMultiSelect("Action",Selector2);		
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
