package Skills;




import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PInsertChart extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Insert Chart "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created by Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Insert Chart";
		super.FinalDoc="Insert Chart.pptx";
		super.StartingDoc="Insert Chart.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=5;
		super.excelColCount=5;
		super.Setup("PInsertChart");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Insert_Chart);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.Chart,Selector2);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
