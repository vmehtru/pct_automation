package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EFormatControlSize extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Excel";
	 super.Project_Name="Excel - Format Control - Size "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Aastha";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Format Control - Size";
	 super.FinalDoc="FormatControl.xlsx";
	 super.StartingDoc="FormatControl.xlsx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=5;
	 super.Setup("EFormatControlSize");

	}

	
	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DEVELOPER, SkillName.Excel.Format_Control_Size);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromMultiSelect(Selector.Control,Selector2);
		
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
       @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
       

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
	
	
}
