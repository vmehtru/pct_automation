package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PTextAlignmentandDirection extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
		 super.App="PPT";
		 super.Project_Name="Text Alignment and Direction "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="";
		 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Text Alignment and Direction";
		 super.FinalDoc="Text Alignment and Direction.pptx";
		 super.StartingDoc="Text Alignment and Direction.pptx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=5;
		 super.excelColCount=7;
		 super.Setup("PTextAlignmentandDirection");

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String Selector3, String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Text_Alignment_and_Direction);
		selectValueFromDropDown(PCTSkill.Selector.Slide, Selector1);
		selectValueFromDropDown(PCTSkill.Selector.ApplyTo, Selector2);
		if(Selector2.contains("Shape Paragraphs"))
		{
			selectValueFromDropDown(PCTSkill.Selector.Shape, Selector3);
			selectValueFromMultiSelect(PCTSkill.Selector.Para, Selector4);
		}
		else if(Selector2.contains("Table Paragraphs"))
		{
			selectValueFromDropDown(PCTSkill.Selector.Table, Selector3);
			selectValueFromMultiSelect(PCTSkill.Selector.Para, Selector4);

		}
		
		selectAllProperties();
		saveSkill();
		validateSkillSummary(instNo, ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
