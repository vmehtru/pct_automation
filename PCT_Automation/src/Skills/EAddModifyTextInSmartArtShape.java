package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EAddModifyTextInSmartArtShape extends PCTSkill
{

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.Project_Name="Excel - Add Modify Text in smartArt "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created by Prateek";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\AddModifyTextInSmartArtShape";
		super.FinalDoc="Add_Modify text in smartShape.xlsx";
		super.StartingDoc="Add_Modify text in smartShape.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=7;
		super.Setup("EAddModifyTextInSmartArtShape");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SMARTART, SkillName.PPT.Add_Modify_Text_in_SmartArt_Shape);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
	    super.selectValueFromDropDown(Selector.SmartArt,Selector2);
	    super.selectValueFromMultiSelect(Selector.Shape,Selector3);
	    super.selectValueFromMultiSelect(Selector.Para,Selector4);
	    
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
