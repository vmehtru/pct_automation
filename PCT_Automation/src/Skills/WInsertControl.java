package Skills;





import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WInsertControl extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Insert Control "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Control";
	 super.FinalDoc="Insert Control.docx";
	 super.StartingDoc="Insert Control.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=6;

	 super.Setup("WInsertControl");

	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DEVELOPER, SkillName.Word.Insert_Control);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);

		if(Selector1.equals("Document Paragraphs"))
		{
			super.selectValueFromMultiSelect(Selector.Control, Selector2);
		}
		else if(Selector1.equals("Table Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
			super.selectValueFromMultiSelect(Selector.Control, Selector3);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
