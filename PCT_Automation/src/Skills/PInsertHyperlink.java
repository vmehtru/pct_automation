package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PInsertHyperlink extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="PPT";
	 super.Project_Name="PPT - Insert Hyperlink"+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Palak Bansal";
	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Insert Hyperlink";
	 super.FinalDoc="Insert Hyperlink.pptx";
	 super.StartingDoc="Insert Hyperlink.pptx";
	 
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=9;
	 super.excelColCount=8;
	

	 super.Setup("PInsertHyperlink");
}	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ImageSelector, String ValidateOn, String ValidateFor, String ImageSrc_validationOn,String ImageSrc_validationFor ) throws Exception
	{
		
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Insert_Hyperlink);	
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromMultiSelectImageAndText(Selector.Hyperlink, Selector2, ImageSelector);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor,ImageSrc_validationOn,ImageSrc_validationFor);
	}
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
}



