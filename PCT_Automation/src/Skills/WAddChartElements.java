package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WAddChartElements extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Add Chart Elements	 "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="by Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Add Chart Elements";
	 super.FinalDoc="Add Chart Elements.docx";
	 super.StartingDoc="Add Chart Elements.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=11;
	 super.excelColCount=6;

	 super.Setup("WAddChartElements");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2,String Selector3,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.CHART, SkillName.Word.Add_Chart_Elements);
		super.selectValueFromDropDown(Selector.Chart,Selector1);
		super.selectValueFromDropDown(Selector.Element,Selector2);

		if(Selector2.equals("Data Labels"))
		{
			super.selectValueFromMultiSelect(Selector.Data_Label,Selector3);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


}
