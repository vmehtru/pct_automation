package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PApplyShading extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Apply Shading "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Apply Shading";
		super.FinalDoc="Apply Shading.pptx";
		super.StartingDoc="Apply Shading.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=2;
		super.excelColCount=7;
		super.Setup("PApplyShading");

	}

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.TABLE, SkillName.PPT.Apply_Shading);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector2);
		if(Selector2.equals("Table"))
		{
		super.selectValueFromDropDown(Selector.Table,Selector3);
		}
		if(Selector2.equals("Cell"))
		{
		super.selectValueFromDropDown(Selector.Table,Selector3);
		//Deselecting the default selected control
		super.selectValueFromMultiSelectByIndex(Selector.Cell, "1");
		super.selectValueFromMultiSelect(Selector.Cell,Selector4);
			
		}
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	 }


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
