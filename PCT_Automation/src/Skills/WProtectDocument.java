package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WProtectDocument extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Protect Document";
	 

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=6;

	
	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill123(String Iteration_no,String FinalDoc, String StartingDoc, String FeedbackTextFile,String ValidateOn, String ValidateFor) throws Exception
	{  
		
		super.Project_Name="Word - Protect Document "+Calendar.getInstance().getTime().toString();
	    super.Project_Desc="By Aastha";
	    super.FinalDoc=FinalDoc;
		super.StartingDoc=StartingDoc;
		super.Setup("WProtectDocument");

		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.FILE, SkillName.Word.Protect_Document);
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		
		
		testPerfectDocAndScore();
		testStartingDocAndFeedback(FeedbackTextFile);
		setMasterProjectAndSaveProjectID();

	}

	

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
