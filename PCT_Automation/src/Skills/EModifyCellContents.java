package Skills;



import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EModifyCellContents extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Modify Cell Contents "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Sahil";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Modify Cell Contents";
	 super.FinalDoc="Modify Cell Contents.xlsx";
	 super.StartingDoc="Modify Cell Contents.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=5;
	 super.excelColCount=5;

	 super.Setup("EModifyCellContents");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Modify_Cell_Contents);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.addTextToNamedRangeTextBox(Selector.Cell,Selector2);

		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

       @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
