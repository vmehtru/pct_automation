package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EFormatControlControl extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Format Contol: Control "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By DhruvD";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Format Control - Control";
	 super.FinalDoc="TestCase.xlsx";
	 super.StartingDoc="TestCase.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=5;

	 super.Setup("EFormatControlControl");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DEVELOPER, SkillName.Excel.Format_Control_Control);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);

		super.selectValueFromMultiSelect(Selector.Control,Selector2);

		super.selectAllProperties();

		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
