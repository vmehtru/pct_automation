package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EShapeSizeandArrangement extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.Project_Name="Excel - Shape Size and Arrangement  "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Shape Size and Arrangement";
		super.FinalDoc="Shape Size and Arrangement.xlsx";
		super.StartingDoc="Shape Size and Arrangement.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=7;
		super.Setup("EShapeSizeandArrangement");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String Selector4,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DRAWING, SkillName.Excel.Shape_Size_and_Arrangement);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector2);
		if(Selector2.equals("Chart"))
		{
			super.selectValueFromDropDown(Selector.Chart,Selector3);
			super.selectValueFromMultiSelect(Selector.Shapes_starting_with_text,Selector4);
		}
		else
		if(Selector2.equals("Worksheet"))
		{
			super.selectValueFromMultiSelect(Selector.Shapes_starting_with_text,Selector3);
		}
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	 @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
