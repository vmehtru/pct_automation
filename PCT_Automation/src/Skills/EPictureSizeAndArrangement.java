package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EPictureSizeAndArrangement extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.Project_Name="Excel - Picture Size And Arrangement  "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Aastha";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Picture Size and Arrangement";
		super.FinalDoc="Picture Size and Arrangement_Apr'15.xlsx";
		super.StartingDoc="Picture Size and Arrangement_Apr'15.xlsx";
		super.excelDataSourcePath="Datasource_Apr'15.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=5;
		super.Setup("EPictureSizeAndArrangement");
		
	}
		
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.PICTURE, SkillName.Excel.Picture_Size_and_Arrangement);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
	
		super.selectImageFromMultiSelectByTitle(Selector.Picture,Selector2); 
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
