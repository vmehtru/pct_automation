package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WSignatureSetup extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Signature Setup "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" by Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Signature Setup";
	 super.FinalDoc="Signature Setup.docx";
	 super.StartingDoc="Signature Setup.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=4;

	 super.Setup("WSignatureSetup");

	}
	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Word.Signature_Setup);

		super.selectValueFromMultiSelect(Selector.Signature,Selector1);
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
