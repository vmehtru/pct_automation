package Skills;





import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PBulletsAndNumbering extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="PPT";
		 super.Project_Name="Bullets and Numbering "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Manas";
		 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Bullets and Numbering";
		 super.FinalDoc="BulletAndNumbering.pptx";
		 super.StartingDoc="BulletAndNumbering.pptx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=17;
		 super.excelColCount=8;

		 super.Setup("PBulletsAndNumbering");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String Selector3, String Selector4, String ImgSrc, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Bullets_and_Numbering);
		selectValueFromDropDown(PCTSkill.Selector.ApplyTo, Selector1);
		if(Selector1.contains("Slides"))
		{
			selectValueFromDropDown(PCTSkill.Selector.Slide, Selector2);
			selectValueFromMultiSelect(PCTSkill.Selector.Para, Selector3);
		}
		else if(Selector1.contains("Slide Layout"))
		{
			selectValueFromDropDown("Slide Master :", Selector2);
			selectValueFromDropDown("Slide Layout:", Selector3);
			selectValueFromMultiSelect(PCTSkill.Selector.Para, Selector4);


		}
		else if(Selector1.contains("Slide Master"))
		{
			selectValueFromDropDown("Slide Master", Selector2);
			selectValueFromMultiSelect(PCTSkill.Selector.Para, Selector3);
		}

		selectAllProperties();
		saveSkill();
		validateSkillSummary(instNo, ValidateOn,ValidateFor, ImgSrc);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
