package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PFontFormatting extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Font Formatting "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="created by Palak";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Font Formatting";
		super.FinalDoc="Fontformatting_ext.pptx";
		super.StartingDoc="Fontformatting_ext.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=8;
		super.Setup("PFontFormatting");
     }
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String Selector5, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Font_Formatting);
		if(Selector1.equals("Slides"))
		{
			super.selectValueFromDropDownForSelectorOfSameName(Selector.ApplyTo,Selector1);
			super.selectValueFromDropDown(Selector.Slide,Selector2);
			super.selectValueFromDropDownForSelectorOfSameName(Selector.ApplyTo,Selector3);
			super.selectValueFromDropDown(Selector.Shape,Selector4);
			super.selectValueFromMultiSelect(Selector.Para, Selector5);
		}
		
		if (Selector1.equals("Notes Master"))
		{
			super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
			super.selectValueFromDropDown(Selector.Shape,Selector2);
			super.selectValueFromMultiSelect(Selector.Para,Selector3);
		}
		
		if(Selector1.equals("Slide Master"))
		{
			super.selectValueFromDropDown(Selector.ApplyTo, Selector1);
			super.selectValueFromDropDown(Selector.Slide, Selector2);
			super.selectValueFromMultiSelect(Selector.Shape,Selector3);
			super.selectValueFromMultiSelect(Selector.Para,Selector4);
		}
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}

}
