package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WFormatPictureEffects extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Format Picture Effects "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By DhruvD";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Format Picture Effects";
	 super.FinalDoc="Format Picture Effects.docx";
	 super.StartingDoc="Format Picture Effects.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=5;
	 super.excelColCount=4;

	 super.Setup("WFormatPictureEffects");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1 , String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.PICTURE, SkillName.Word.Format_Picture_Effects);

		super.selectImageFromMultiSelectByIndex(Selector.Picture , Selector1);
		super.selectAllProperties();

		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

    @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
