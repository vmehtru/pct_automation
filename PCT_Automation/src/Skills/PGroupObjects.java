package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PGroupObjects extends PCTSkill
{

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - GroupObjects "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created by Prateek under guidance of Rupsi ma'am";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\GroupObjects";
		super.FinalDoc="GroupObjects.pptx";
		super.StartingDoc="GroupObjects.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=5;
		super.Setup("PGroupObjects");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.PICTURE, SkillName.PPT.Group_Objects);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
	    super.selectValueFromMultiSelect(Selector.Group,Selector2);
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
