package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;




public class WInsertCrossReference extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Insert Cross Reference "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Cross Reference";
	 super.FinalDoc="Insert Cross Reference.docx";
	 super.StartingDoc="Insert Cross Reference.docx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=6;
	 super.excelColCount=4;
	 super.Setup("WInsertCrossReference");
	}
	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		
		super.addSkill(instNo, SkillCategory.REFERENCES, SkillName.Word.Insert_Cross_Reference);

		super.selectValueFromMultiSelect(Selector.Cross_Reference,Selector1);

 		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}




}
