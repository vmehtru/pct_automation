package Skills;

import java.util.Calendar;

import org.testng.annotations.Test;



import org.testng.annotations.BeforeClass;



public class EPageSetupMargin extends PCTSkill {
	

		@BeforeClass
		public void Setup() throws Exception
		{
		 super.App="Excel";
		 super.Project_Name="Excel - Page setup - Margin "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Palak";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\PageSetup-Margins";
		 super.FinalDoc="PageSetups-Margin.xlsx";
		 super.StartingDoc="PageSetups-Margin.xlsx";
		 
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=3;
		 super.excelColCount=4;
			
		 super.Setup("E.PageSetups-Margin");

		}

		
		
		@Test(dataProvider="dp", groups= {"testCase"})
		public void TestSkill(String Iteration_no,String Selector1,String ValidateOn, String ValidateFor) throws Exception
		{
			String instNo=super.addInstructiontoPCTProject();
			super.addSkill(instNo, SkillCategory.PAGE_LAYOUT, SkillName.Excel.Page_Setup_Margin);
			
			super.selectValueFromDropDown(Selector.Sheet,Selector1);
			
			super.selectAllProperties();
			super.saveSkill();
			super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		} 
		
		@Test(groups= {"testSubmission"})
		public void testScoreAndFeedback() throws Exception
		{
			testPerfectDocAndScore();
			testStartingDocAndFeedback();
		}
	            
	        @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		
		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
		
		
}
