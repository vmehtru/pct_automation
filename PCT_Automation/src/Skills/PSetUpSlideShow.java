package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PSetUpSlideShow extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="PPT";

	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Set Up Slide Show";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=6;

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String FinalDoc, String StartingDoc, String FeedbackTextFile, String ValidateOn, String ValidateFor) throws Exception
	{
		super.Project_Name="PPT - Set Up Slide Show "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Sahil Gupta";
		super.FinalDoc=FinalDoc;
		super.StartingDoc=StartingDoc;
		super.Setup("PSetUpSlideShow");

		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SLIDE_SHOW, SkillName.PPT.Set_Up_Slide_Show);

		super.selectAllProperties();

		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(FeedbackTextFile);
		setMasterProjectAndSaveProjectID();

	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
