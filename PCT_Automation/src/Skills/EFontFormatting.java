package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EFontFormatting extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Font Formaatting "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created By Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Font formatting";
	 super.FinalDoc="FontFormatting.xlsx";
	 super.StartingDoc="FontFormatting.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=6;

	 super.Setup("EFontFormatting");

	}

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Font_Formatting);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);

		super.selectValueFromDropDown(Selector.Para,Selector2);
		super.addTextToNamedRangeTextBox(Selector.Cell, Selector3);
		super.selectAllProperties();


		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}

}
