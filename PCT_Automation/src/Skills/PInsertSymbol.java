package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PInsertSymbol extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Insert Symbol "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Prateek";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Insert Symbol";
		super.FinalDoc="Insert Symbol.pptx";
		super.StartingDoc="Insert Symbol.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=7;
		super.Setup("PInsertSymbol");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Insert_Symbol);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Slides"))
		{
		super.selectValueFromDropDown(Selector.Slide,Selector2);
		
		super.selectValueFromDropDown(Selector.Shape,Selector3);
		
		super.selectValueFromMultiSelect(Selector.Symbol, Selector4);
		}
		if(Selector1.equals("Handout Master"))
		{
		super.selectValueFromDropDown(Selector.Shape,Selector2);
	
		super.selectValueFromMultiSelect(Selector.Symbol,Selector3);
		}
		if(Selector1.equals("Notes Master"))
		{
		super.selectValueFromDropDown(Selector.Shape,Selector2);
		
		super.selectValueFromMultiSelect(Selector.Symbol,Selector3);
		}
		
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
