package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WApplyPageBorder extends PCTSkill 
{
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Word";
		super.Project_Name="Word - Apply Page Border "+Calendar.getInstance().getTime().toString();
		super.SkillDocsPath="PCT_Skill_Documents\\Word\\Apply Page Border";
				
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=8;
    }
	
	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String FinalDocument,String StartDocument,String Selector1,String Selector2, String ValidateOn, String ValidateFor,String SummaryFile) throws Exception
	{
		super.Project_Name="Page Setup - Margin "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created By Prateek";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WApplyPageBorders");
		
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Apply_Page_Border);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Sections"))
		{
		super.selectValueFromMultiSelect(Selector.Section,Selector2);
		}
		super.selectAllProperties_groupcheckbox();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	
		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}



