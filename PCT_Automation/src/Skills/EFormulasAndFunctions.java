package Skills;

import static Utils.MouseKeyboardActions.click;
import java.util.Calendar;

import org.openqa.selenium.Keys;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static Utils.MouseKeyboardActions.sendKeys;

public class EFormulasAndFunctions extends PCTSkill  {

	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Excel";
		 super.Project_Name="Excel - Formulas and Functions "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Manas";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Formulas and Functions";
		 super.FinalDoc="Final.xlsx";
		 super.StartingDoc="Final.xlsx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=3;
		 super.excelColCount=7;

		 super.Setup("EFormulasAndFunctions");

	}
	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String Permutations, String Functions, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, PCTSkill.SkillCategory.FORMULAS, PCTSkill.SkillName.Excel.Formulas_n_Functions);
		super.selectValueFromDropDown(PCTSkill.Selector.Sheet, Selector1);
		super.addTextToNamedRangeTextBox(Selector.Cell, Selector2);
		click("AddPermutations");
		if(Permutations.contains("Automatic"))
		{
			switchToIFrame("PropertyEditToolIFrame");
			click("AutomaticPermutation");
			click("AddPermutationsDone");
			switchToDefaultContent();
		}
		else
		{
			switchToIFrame("PropertyEditToolIFrame");
			click("FormulaPermutationsTextarea");
			sendKeys("FormulaPermutationsTextarea",Keys.ENTER);
			sendKeys("FormulaPermutationsTextarea",Permutations);
			click("AddPermutationsDone");
			switchToDefaultContent();
		}
		click("AddFormulaName");
		switchToIFrame("PropertyEditToolIFrame");
		click("FormulaPermutationsTextarea");

		//click("FormulaTextarea",Functions);

		sendKeys("FormulaPermutationsTextarea",Functions);
		click("AddPermutationsDone");
		switchToDefaultContent();


		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	}
