package Skills;



import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WVideoSizeandArrangement extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Video Size and Arrangement "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Video Size and Arrangement";
	 super.FinalDoc="Video Size and Arrangement.docx";
	 super.StartingDoc="Video Size and Arrangement.docx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=4;
	 super.Setup("WVideoSizeandArrangement");

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.PICTURE, SkillName.Word.Video_Size_and_Arrangement);
		super.selectVideoFromMultiSelect(Selector.Video,Selector1);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
