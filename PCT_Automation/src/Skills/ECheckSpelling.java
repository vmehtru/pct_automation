package Skills;



import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ECheckSpelling extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Check Spelling "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Sahil";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Check Spelling";
	 super.FinalDoc="Check Spelling.xlsx";
	 super.StartingDoc="Check Spelling.xlsx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=5;
	 super.excelColCount=6;
	 super.Setup("ECheckSpelling");

	}
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.REVIEW, SkillName.Excel.Check_Spelling);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.addTextToTextBox(Selector.Spelling,Selector2);
		//Deselecting the default selected control
		super.selectValueFromMultiSelectByIndex(Selector.Para, "1");
		super.selectValueFromMultiSelect(Selector.Para,Selector3);
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

       @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
