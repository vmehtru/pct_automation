package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WInsertRowsTable extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Insert Rows in a Table "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Dhruv";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Rows in a Table";
	 super.FinalDoc="Insert_Rows.docx";
	 super.StartingDoc="Insert_Rows.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=4;

	 super.Setup("WInsertRowsTable");
	}



	@Test(dataProvider="dp")

	public void TestCases_v15(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();

		super.addSkill(instNo, SkillCategory.TABLE, SkillName.Word.Insert_Rows_in_a_Table);

		super.selectValueFromDropDown(Selector.Table,Selector1);
		super.selectAllFromMultiSelect(Selector.Rows);

		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
}
