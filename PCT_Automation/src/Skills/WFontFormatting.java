package Skills;



import static Utils.MouseKeyboardActions.click;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;



import Utils.MouseKeyboardActions;
public class WFontFormatting extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Word";
		 super.Project_Name="Word - Font Formatting "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Ajay";
		 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Font Formatting";
		 super.FinalDoc="Font Formatting.docx";
		 super.StartingDoc="Font Formatting.docx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=15;
		 super.excelColCount=7;

		 super.Setup("WFontFormatting");

	}


	@Test(groups= {"testCase"})
	public void TestSkillWithSearchBox() throws Exception
	{

		String instNo = super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Font_Formatting);
		String ValidateFor= "Font Name = Calibri, Font Size = 11, Bold = Not Applied, Italics = Not Applied, Font Color = Automatic, Underline = Not Applied, Strikethrough = Not Applied, Double Strikethrough = Not Applied, Super/Sub Script = Not Applied, Small Caps = Not Applied, All Caps = Not Applied, Hidden = Not Applied, Text Effect Style = As per final document, Shadow = As per final document, Scale = 100%, Spacing = Normal,  Highlight Color = No Color";
		String Validateto="Document Paragraphs ([7] Maine has already adopted policies to combat the effects of pollution, ...)";

			super.selectValueFromDropDown(Selector.ApplyTo,"Document Paragraphs");
			MouseKeyboardActions.sendKeys("SearchBox", "adopted");
			super.selectValueFromMultiSelectAdvanced("Specific Paragraph", "[7] Maine has already adopted policies to combat the effects of pollution");
			super.selectAllProperties();
			saveSkill();
			super.validateSkillSummary(instNo,Validateto,ValidateFor);
	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Font_Formatting);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Table"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
			super.selectValueFromMultiSelect(Selector.Cell, Selector3);
			super.selectValueFromMultiSelectByIndex(Selector.Para, "1");
			super.selectValueFromMultiSelect(Selector.Para, Selector4);
			
		}
		else if(Selector1.equals("Shape Paragraphs"))
		{
			super.selectValueFromMultiSelect(Selector.Shape, Selector2);
			super.selectValueFromMultiSelectByIndex(Selector.Para, "1");
			super.selectValueFromMultiSelect(Selector.Para, Selector3);
		}
		else if(Selector1.equals("Document Paragraphs"))
		{
			click("showAllInput");
			Thread.sleep(2000);
			super.selectValueFromMultiSelectAdvanced("Specific Paragraph", Selector2);
		}
		else if(Selector1.equals("Header"))
		{
			super.selectValueFromMultiSelect(Selector.Header, Selector2);
			super.selectValueFromMultiSelectByIndex(Selector.Para, "1");
			super.selectValueFromMultiSelect(Selector.Para, Selector3);
		}
		else if(Selector1.equals("Footer"))
		{
			super.selectValueFromMultiSelect(Selector.Footer, Selector2);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();

		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
