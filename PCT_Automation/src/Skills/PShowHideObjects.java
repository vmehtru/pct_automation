package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PShowHideObjects extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="PPT";
		 super.Project_Name="PPT - Show Hide Objects"+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Manas";
		 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Show Hide Objects";
		 super.FinalDoc="Test1.pptx";
		 super.StartingDoc="Test1.pptx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=3;
		 super.excelColCount=5;

		 super.Setup("PShowHideObjects");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Show_Hide_Objects);

		super.selectValueFromDropDown(Selector.Slide, Selector1);
		super.selectValueFromMultiSelect(Selector.Object, Selector2);
		selectAllProperties();
		saveSkill();
		validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

}
