package Skills;




import java.util.Calendar;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class PApplyModifyAnimationOptions extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="PPT";
	 super.Project_Name="PPT - Apply/Modify Animation Options "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="by Ajay";
	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Apply Modify Animation Options";
	 super.FinalDoc="Apply animation.pptx";
	 super.StartingDoc="Apply animation.pptx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=5;

	 super.Setup("PApplyModifyAnimationOptions");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.ANIMATIONS, SkillName.PPT.Apply_Modify_Animation_Options);
		
        super.selectValueFromDropDown(Selector.Slide, Selector1);
        super.selectValueFromMultiSelect(Selector.Animation, Selector2);
        
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
