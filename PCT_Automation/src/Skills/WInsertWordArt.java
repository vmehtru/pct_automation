package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WInsertWordArt extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Insert WordArt "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert WordArt";
	 super.FinalDoc="Insert WordArt.docx";
	 super.StartingDoc="Insert WordArt.docx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=6;
	 super.excelColCount=4;
	 super.Setup("WInsertWordArt");

	}

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Word.Insert_WordArt);
		super.selectValueFromMultiSelect(Selector.WordArt,Selector1);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
