package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PChartSizeAndArrangement extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Chart Size And Arrangement "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created by Prateek";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Chart Size And Arrangement";
		super.FinalDoc="Chart Size and Arrangement.pptx";
		super.StartingDoc="Chart Size and Arrangement.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=5;
		super.Setup("PChartSizeAndArrangement");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.CHART, SkillName.PPT.Chart_SizeAndArrangement);
		super.selectValueFromDropDown(Selector.Slide, Selector1);
		super.selectValueFromDropDown(Selector.Chart, Selector2);
	
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
