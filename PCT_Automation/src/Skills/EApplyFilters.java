package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class EApplyFilters extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Apply Filters "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="DhruvD";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Apply Filters";
	 super.FinalDoc="Apply Filters.xlsx";
	 super.StartingDoc="Apply Filters.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=6;
	 super.excelColCount=5;

	 super.Setup("EApplyFilters");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DATA, SkillName.Excel.Apply_Filters);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromMultiSelect(Selector.Column,Selector2);

		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
