package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class EAddShapesToSmartArt extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="Excel";
	 super.Project_Name="Excel - Add Shapes To SmartArt "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Palak";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Add Shapes To SmartArt";
	 super.FinalDoc="AddShapesToSmartArt.xlsx";
	 super.StartingDoc="AddShapesToSmartArt.xlsx";
	 
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=8;
		
	 super.Setup("EAddShpesToSmartArt");

	}

	
	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String ImageSelector,String ValidateOn, String ValidateFor,String ImageSrcCheckOn) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SMARTART, SkillName.Excel.Add_Shapes_to_SmartArt);
		
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromDropDown(Selector.SmartArt,Selector2);
		System.out.println(ImageSelector);
		super.selectValueFromMultiSelectImageAndText(Selector.Shape,Selector3,ImageSelector);
		
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary_image_presentInCheckOn(instNo,ValidateOn,ValidateFor,ImageSrcCheckOn);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
            
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	
	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
	
	

}
