package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WDocumentProperties extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";

	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Document Properties";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=6;

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String FinalDoc, String StartingDoc, String FeedbackTextFile, String ValidateOn, String ValidateFor) throws Exception
	{
		super.Project_Name="Word - Document Properties "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Sahil";
		super.FinalDoc=FinalDoc;
		super.StartingDoc=StartingDoc;
		super.Setup("WDocumentProps");

		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.FILE, SkillName.Word.Document_Properties);

		super.selectAllProperties();

		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(FeedbackTextFile);
		setMasterProjectAndSaveProjectID();

	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
