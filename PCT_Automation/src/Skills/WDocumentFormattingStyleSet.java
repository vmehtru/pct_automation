package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WDocumentFormattingStyleSet extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Word";
		super.SkillDocsPath="PCT_Skill_Documents\\Word\\Document Formatting - Style Set";
		
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=8;
	}


	@Test(dataProvider="dp")
	public void TestCases_v15(String Iteration_no,String FinalDocument,String StartDocument,String Selector1, String Selector2, String ValidateOn, String ValidateFor,String SummaryFile) throws Exception
	{
		super.Project_Name="Word- Document Formatting -Style_Set"+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created By Prateek";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WDocumentFormattingStyleSet");
		
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DESIGN, SkillName.Word.Document_Formatting_Style_Set);
		super.selectValueFromDropDown(Selector.Style, Selector1);
		super.selectValueFromMultiSelect(Selector.Para, Selector2);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		testPerfectDocAndScore();

		testStartingDocAndFeedback(SummaryFile);
        setMasterProjectAndSaveProjectID();


	}



@Test(groups= {"upgrade"})
public void testUpgrade() throws Exception
{
	getSavedProjectIDsForSkillandUpgrade();
}
}
