package Skills;


import java.util.Calendar;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EPowerViewFilters extends PCTSkill  {

	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Excel";
		 super.Project_Name="Excel - Power View Filters"+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Manas";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Power View Filters";
		 super.FinalDoc="TestCase.xlsx";
		 super.StartingDoc="TestCase.xlsx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=3;
		 super.excelColCount=5;

		 super.Setup("EPowerViewFilters");

	}
	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, PCTSkill.SkillCategory.POWER_VIEW, PCTSkill.SkillName.Excel.Power_View_Filters);
		super.selectValueFromDropDown(PCTSkill.Selector.Sheet, Selector1);
		super.selectValueFromMultiSelect(Selector.Field, Selector2);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	}
