package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Utils.MouseKeyboardActions;

public class WApplyStylesToParagraphs extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Apply Styles to Paragraphs "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Apply Styles to Paragraphs";
	 super.FinalDoc="Apply Styles to Paragraphs.docx";
	 super.StartingDoc="Apply Styles to Paragraphs.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=6;

	 super.Setup("WApplyStylesToParagraphs");

	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Apply_Styles_to_Paragraphs);

		super.selectValueFromDropDown(Selector.ApplyTo, Selector1);

		if(Selector1.equals("Document Paragraphs"))
		{	
			MouseKeyboardActions.sendKeys("SearchBox",Selector2);
			super.selectValueFromMultiSelectAdvanced(Selector.Para, Selector3);			
		}		
		
		else if(Selector1.equals("Shape Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Shape, Selector2);
			super.selectValueFromMultiSelect(Selector.Para, Selector3);
		}
				
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
