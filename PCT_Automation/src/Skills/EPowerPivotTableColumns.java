package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class EPowerPivotTableColumns extends PCTSkill {


		@BeforeClass
		public void Setup() throws Exception
		{
		 super.App="Excel";
		 super.Project_Name="Excel - PowerPivot Table Columns "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Rupsi";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\PowerPivot Table Columns";
		 super.FinalDoc="PowerPivot Table Columns.xlsx";
		 super.StartingDoc="PowerPivot Table Columns.xlsx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=4;
		 super.excelColCount=5;


		 super.Setup("EPowerPivotTableColumns");

		}


		@Test(dataProvider="dp", groups= {"testCase","Mar15"})
		public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
		{
			String instNo=super.addInstructiontoPCTProject();
			super.addSkill(instNo, SkillCategory.POWERPIVOT, SkillName.Excel.PowerPivot_Table_Columns);
			super.selectValueFromDropDown(Selector.Table,Selector1);
			super.selectValueFromMultiSelect(Selector.Column,Selector2);
			super.selectAllProperties();
			saveSkill();
			super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		}

		@Test(groups= {"testSubmission"})
		public void projectScoreAndFeedback() throws Exception
		{
			testPerfectDocAndScore();
			testStartingDocAndFeedback();
		}

                @Test(groups= {"saveProject"})
	        public void saveProject() throws Exception
	        {
		        setMasterProjectAndSaveProjectID();
	        }

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}


}
