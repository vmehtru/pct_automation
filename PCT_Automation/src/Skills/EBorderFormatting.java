package Skills;


import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common.SingleWebDriverObject;

public class EBorderFormatting extends PCTSkill{
	@BeforeClass
	public void Setup() throws Exception
	{
 
	 super.App="Excel";
	 super.Project_Name="Excel - Border Formaatting "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Aastha";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Border formatting";
	 super.FinalDoc="BorderFormatting.xlsx";
	 super.StartingDoc="BorderFormatting.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=5;
	 super.Setup("EBorderFormatting");
	
	}

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
	
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Border_Formatting);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);
    	super.addTextToNamedRangeTextBox(Selector.Cell, Selector2);
		super.selectAllProperties_groupcheckbox();


		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		
		
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
			
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}

}
