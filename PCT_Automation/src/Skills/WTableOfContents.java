package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WTableOfContents extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Table of Contents";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=6;

	}

	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String FinalDocument,String StartDocument,String ValidateOn, String ValidateFor,String SummaryFile) throws Exception
	{
		super.Project_Name="Table of content "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created By Prateek";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WTableOfContent");

		String instNo=super.addInstructiontoPCTProject();

		super.addSkill(instNo, SkillCategory.REFERENCES, SkillName.Word.Table_Of_Contents);

		super.selectAllProperties_groupcheckbox();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);

		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
