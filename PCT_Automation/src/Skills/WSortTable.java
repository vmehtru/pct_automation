package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WSortTable extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Sort Table "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" Rupsi Mehta ";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Sort Table";
	 super.FinalDoc="Sort Table.docx";
	 super.StartingDoc="Sort Table.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=6;

	 super.Setup("WSortTable");

	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.TABLE, SkillName.Word.Sort_Table);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);

		if(Selector1.equals("Document Tables"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
		}
		else if(Selector1.equals("Shapes"))
		{
			super.selectValueFromDropDown(Selector.Shape,Selector2);
			super.selectValueFromDropDown(Selector.Table,Selector3);
		}

		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
         @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
