package Skills;




import static Utils.MouseKeyboardActions.click;
import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import Utils.MouseKeyboardActions;


public class WFontFormattingWordCharacterLevel extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Word";
	 super.Project_Name="Word - Font Formatting Word Character Level "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Rupsi Mehta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Font Formatting Word Character Level";
	 super.FinalDoc="Font Formatting Word Character Level.docx";
	 super.StartingDoc="Font Formatting Word Character Level.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=7;
	 super.excelColCount=8;

	 super.Setup("WFontFormattingWordCharacterLevel");

	}

	@Test(groups= {"testCase"})
	public void TestSkillWithSearchBox() throws Exception
	{

		String instNo = super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Font_Formatting_Word_Character_Level);
		String ValidateFor= "Ligatures = Standard Only";
		String Validateto="Document Paragraphs ([1] Rupsi (R [with Ligature = Standard Only]))";

			super.selectValueFromDropDown(Selector.ApplyTo,"Document Paragraphs");
			MouseKeyboardActions.sendKeys("SearchBox", "rupsi");
			super.selectValueFromRadio(Selector.Para,"[1] Rupsi");
			super.selectValueFromMultiSelect(Selector.Text, "R [with Ligature = Standard Only]");
			saveSkill();
			super.validateSkillSummary(instNo,Validateto,ValidateFor);
	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String Selector5, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Font_Formatting_Word_Character_Level);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);

		if(Selector1.equals("Document Paragraphs"))
		{
			click("showAllInput");
			super.selectValueFromRadio(Selector.Para,Selector3);
			super.selectValueFromMultiSelect(Selector.Text, Selector4);
		}
		else if(Selector1.equals("Shapes"))
		{
			super.selectValueFromMultiSelect(Selector.Shape, Selector2);
			super.selectValueFromMultiSelect(Selector.Para, Selector3);
			//Thread.sleep(2000);
			super.selectValueFromMultiSelect(Selector.Text, Selector4);
		}
		else if(Selector1.equals("Table"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
			super.selectValueFromDropDown(Selector.Cell,Selector3);
		    super.selectValueFromRadio(Selector.Para,Selector4);
			super.selectValueFromMultiSelect(Selector.Text, Selector5);

		}

		else if(Selector1.equals("Header"))
		{
			super.selectValueFromDropDown(Selector.Header,Selector2);
		    super.selectValueFromRadio(Selector.Para,Selector3);
			super.selectValueFromMultiSelect(Selector.Text, Selector4);

		}
		else if(Selector1.equals("Footer"))
		{
			super.selectValueFromDropDown(Selector.Footer,Selector2);
		    super.selectValueFromRadio(Selector.Para,Selector3);
			super.selectValueFromMultiSelect(Selector.Text, Selector4);

		}
		else if(Selector1.equals("Bibliography"))
		{
			  super.selectValueFromRadio(Selector.Para,Selector2);
			  super.selectValueFromMultiSelect(Selector.Text, Selector3);

		}
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

    @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
