package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ETrackChanges extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Track Changes "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Aastha";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Track Changes";
	 super.FinalDoc="TrackChanges.xlsx";
	 super.StartingDoc="TrackChanges.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=5;

	 super.Setup("ETrackChanges");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.REVIEW, SkillName.Excel.Track_Changes);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);		
		super.addTextToNamedRangeTextBox(Selector.Cell,Selector2);
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
