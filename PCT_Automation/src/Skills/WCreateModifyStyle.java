package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WCreateModifyStyle extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Create Modify Style "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" by Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\CreateModify Style";
	 super.FinalDoc="Create_Modify_Style.docx";
	 super.StartingDoc="Create_Modify_Style.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=6;

	 super.Setup("WCreateModifyStyle");

	}
	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.CreateModify_Style);

		super.selectValueFromDropDown(Selector.Type,Selector1);
		super.selectValueFromDropDown(Selector.Name,Selector2);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector3);
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
