package Skills;

import org.testng.annotations.Test;
import java.util.Calendar;
import org.testng.annotations.BeforeClass;




public class WShapeContent extends PCTSkill{
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Shape Content "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Aastha";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Shape Content";
	 super.FinalDoc="TestCase.docx";
	 super.StartingDoc="TestCase.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=5;
	 super.excelColCount=5;

	 super.Setup("WInsertShape");

	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill_123(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DRAWING, SkillName.Word.Shape_Content);

		super.selectValueFromMultiSelect(Selector.Shape, Selector1);
		super.selectValueFromMultiSelect(Selector.Para, Selector2);
				
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
