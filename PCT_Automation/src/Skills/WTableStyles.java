package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class WTableStyles extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Table Styles "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created by Prateek ";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Table Styles";
	 super.FinalDoc="Table Styles.docx";
	 super.StartingDoc="Table Styles.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=5;

	 super.Setup("WTableStyles");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.TABLE, SkillName.Word.Table_Styles);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		super.selectValueFromDropDown(Selector.Table,Selector2);
		super.selectAllProperties();
		super.saveSkill();

		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
