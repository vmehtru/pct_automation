package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Utils.MouseKeyboardActions;

public class WParagraphIndentsAndSpacing extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Paragraph - Indents and Spacing "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Paragraph - Indents and Spacing";
	 super.FinalDoc="Paragraph - Indents and Spacing.docx";
	 super.StartingDoc="Paragraph - Indents and Spacing.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=9;
	 super.excelColCount=7;

	 super.Setup("WParagraphIndentsAndSpacing");

	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String Selector4,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Paragraph_Indents_and_Spacing);

		super.selectValueFromDropDown(Selector.ApplyTo, Selector1);

		if(Selector1.equals("Document Paragraphs"))
		{	
			MouseKeyboardActions.sendKeys("SearchBox",Selector2);
			super.selectValueFromMultiSelectAdvanced(Selector.Para, Selector3);			
		}
		
		else if((Selector1.equals("Header")) || (Selector1.equals("Footer")))
		{
			super.selectValueFromMultiSelect(Selector1, Selector2);
			super.selectAllFromMultiSelect(Selector.Para);
			super.selectValueFromMultiSelect(Selector.Para, Selector3);				
		}		
		
		
		else if((Selector1.equals("Footnote")) || (Selector1.equals("Endnote")) || (Selector1.equals("Bibliography")))
		{
			super.selectValueFromMultiSelect(Selector.Para, Selector2);					
		}
		
		else if(Selector1.equals("Table"))
		{
			super.selectValueFromDropDown(Selector.Table, Selector2);
			super.selectValueFromMultiSelect(Selector.Cell, Selector3);
			super.selectAllFromMultiSelect(Selector.Para);
			super.selectValueFromMultiSelect(Selector.Para, Selector4);
		}
		
		else if(Selector1.equals("Shape Paragraphs"))
		{
			super.selectValueFromMultiSelect(Selector.Shape, Selector2);
			super.selectAllFromMultiSelect(Selector.Para);
			super.selectValueFromMultiSelect(Selector.Para, Selector3);
		}
				
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
