package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WAddModifyChartData extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Add/Modify Chart Data "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="created by Prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Add Modify Chart Data";
	 super.FinalDoc="Add Modify Chart Data.docx";
	 super.StartingDoc="Add Modify Chart Data.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=7;
	 super.excelColCount=5;

	 super.Setup("WAddModifyChartData");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.CHART, SkillName.Word.Add_Modify_Chart_Data);

		super.selectValueFromDropDown(Selector.Chart,Selector1);
		super.selectValueFromMultiSelect(Selector.ChartData ,Selector2);

		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
