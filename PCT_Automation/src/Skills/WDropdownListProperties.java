package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WDropdownListProperties extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Dropdown List Properties "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Dropdown List Properties";
	 super.FinalDoc="Dropdown List Properties.docx";
	 super.StartingDoc="Dropdown List Properties.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=7;
	 super.Setup("WDropdownListProperties");

	}


	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DEVELOPER,SkillName.Word.Dropdown_List_Properties);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);

		if(Selector1.equals("Document Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Para, Selector2);
			super.selectValueFromMultiSelect(Selector.Item, Selector3);
		}
		else if(Selector1.equals("Table Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
			super.selectValueFromDropDown(Selector.Cell, Selector3);
			super.selectValueFromMultiSelect(Selector.Item, Selector4);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
