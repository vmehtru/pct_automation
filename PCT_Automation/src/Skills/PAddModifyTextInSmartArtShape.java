package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PAddModifyTextInSmartArtShape extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Add Modify text in smartArt shape "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created by Prateek";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Add_Modify Text in smartart Shape";
		super.FinalDoc="Add_Modify text in smartArt.pptx";
		super.StartingDoc="Add_Modify text in smartArt.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=7;
		super.Setup("PAddModifytextinSmartArtShape");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SMARTART, SkillName.PPT.Add_Modify_Text_in_SmartArt_Shape);
		super.selectValueFromDropDown(Selector.Slide, Selector1);
		super.selectValueFromDropDown(Selector.SmartArt,Selector2);
		super.selectValueFromDropDown(Selector.Shape,Selector3);
		super.selectValueFromMultiSelect(Selector.Para,Selector4);
	
		//super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
