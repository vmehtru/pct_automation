package Skills;

import java.util.Calendar;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PAddShapesToSmartArt extends PCTSkill{
	@BeforeClass
	public void Setup() throws Exception
	{
		
		 super.App="PPT";
		 super.Project_Name="PPT Add Shapes to SmartArt "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Palak Bansal";
		 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Add Shapes to SmartArt";
		 super.FinalDoc="Add shapes to smartArt.pptx";
		 super.StartingDoc="Add shapes to smartArt.pptx";
		 
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=6;
		 super.excelColCount=8;
	

	 super.Setup("PAddShapesToSmartArt");
}	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String ImageSelector, String ValidateOn, String ValidateFor,String ImageSrc ) throws Exception
	{
		System.out.println(ImageSelector);
		String instNo=super.addInstructiontoPCTProject();
		
		
		super.addSkill(instNo, SkillCategory.SMARTART, SkillName.PPT.Add_Shapes_to_SmartArt);
		
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.SmartArt,Selector2);
		super.selectValueFromMultiSelectImageAndText(Selector.Shape, Selector3, ImageSelector);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary_image_presentInCheckOn(instNo,ValidateOn,ValidateFor,ImageSrc);
	}
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
}
