package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PCustomSlideShow extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Custom Slide Show "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Sahil Gupta";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Custom Slide Show";
		super.FinalDoc="Custom Slide Show.pptx";
		super.StartingDoc="Custom Slide Show.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=4;
		super.Setup("PCustomSlideShow");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases_v15(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SLIDE_SHOW, SkillName.PPT.Custom_Slide_Show);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
