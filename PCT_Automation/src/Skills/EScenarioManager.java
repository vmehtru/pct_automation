package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EScenarioManager extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.Project_Name="Excel - Scenario Manager  "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Aastha";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Scenario Manager";
		super.FinalDoc="Scenario.xlsx";
		super.StartingDoc="Scenario.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=5;
		super.Setup("EScenarioManager");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases123(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DATA, SkillName.Excel.Scenario_Manager);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromDropDown(Selector.Scenario,Selector2);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
