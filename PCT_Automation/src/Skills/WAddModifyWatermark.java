package Skills;

import java.io.IOException;
import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WAddModifyWatermark extends PCTSkill{
	@BeforeClass
	public void Setup() throws IOException
	{
		super.App="Word";
		super.SkillDocsPath="PCT_Skill_Documents\\Word\\Add-Modify Watermark";
		
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=14;
		super.excelColCount=6;
	}
		

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases_v15(String Iteration_no,String FinalDocument,String StartDocument, String ValidateOn, String ValidateFor,String SummaryFile) throws Exception
	{		
		super.Project_Name="Word_Add_Modify Watermark "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Palak";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WAddModifyWatermark");
		
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DESIGN, SkillName.Word.Add_Modify_Watermark);
		
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		
		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);
		setMasterProjectAndSaveProjectID();

	}
	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
	

}
