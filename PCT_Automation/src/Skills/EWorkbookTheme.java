package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EWorkbookTheme extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Workbook Theme";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=6;
	}


	@Test(dataProvider="dp")
	public void TestCases_v15(String Iteration_no,String FinalDocument,String StartDocument, String ValidateOn, String ValidateFor, String SummaryFile) throws Exception
	{
		super.Project_Name="Excel- Workbook Theme "+Calendar.getInstance().getTime().toString();
		
		super.Project_Desc="By DhruvD";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("EWorkbookTheme");
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.PAGE_LAYOUT, SkillName.Excel.Workbook_Theme);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);
		setMasterProjectAndSaveProjectID();

	}



	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}

