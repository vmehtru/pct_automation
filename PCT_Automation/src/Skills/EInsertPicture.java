package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EInsertPicture extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Excel";
	 super.Project_Name="Excel - Insert picture "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" Created By Prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Insert Picture";
	 super.FinalDoc="Insert Picture.xlsx";
	 super.StartingDoc="Insert Picture.xlsx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=8;
	 super.Setup("EInsertPicture");

	}
	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String ValidateOn, String ValidateFor,String ImageSrc) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Excel.Insert_Picture);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromDropDown(Selector.ApplyTo, Selector2);
		if(Selector2.equals("SmartArt Shapes Pictures"))
		{
			super.selectValueFromDropDown(Selector.SmartArt, Selector3);
			super.selectImageFromMultiSelectByIndex(Selector.Picture, Selector4);
		}
		
		if(Selector2.equals("Sheet Pictures"))
		{
			
			super.selectImageFromMultiSelectByIndex(Selector.Picture, Selector3);
		}
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary_image_presentInValidateFor(instNo,ValidateOn,ValidateFor,ImageSrc);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
	
}
