package Skills;


import java.util.Calendar;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EPivotChartFields extends PCTSkill  {
	
	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Excel";
		 super.Project_Name="Excel - PivotChart Fields"+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Manas";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\PivotChart Fields";
		 super.FinalDoc="PivotChart Fields.xlsx";
		 super.StartingDoc="PivotChart Fields.xlsx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=5;
		 super.excelColCount=6;
		 super.Setup("EPivotChartFields");
	
		
	}
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, PCTSkill.SkillCategory.CHART, PCTSkill.SkillName.Excel.PivotChart_Fields);
		super.selectValueFromDropDown(PCTSkill.Selector.Sheet, Selector1);
		super.selectValueFromDropDown(Selector.Chart, Selector2);
		super.selectValueFromMultiSelect(Selector.Entities , Selector3);
		
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	
	}
