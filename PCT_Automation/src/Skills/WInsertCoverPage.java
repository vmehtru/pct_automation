package Skills;



import java.io.IOException;
import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WInsertCoverPage extends PCTSkill {


	@BeforeClass
	public void Setup() throws IOException
	{

	 super.App="Word";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Cover Page";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=6;

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String FinalDocument,String StartDocument,String SummaryFile, String ValidateOn, String ValidateFor) throws Exception
	{
		super.Project_Name="Insert Cover Page "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="by Rupsi";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WInsertCoverPage");
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Word.Insert_Cover_Page);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

    @Test(groups= {"saveProject"})
public void saveProject() throws Exception
{
	setMasterProjectAndSaveProjectID();
}


}
