package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PSlideProperties extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Slide Properties "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Prateek";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\SlideProperties";
		super.FinalDoc="Slide_Properties.pptx";
		super.StartingDoc="Slide_Properties.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=2;
		super.excelColCount=4;
		super.Setup("PSlideProperties");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Slide_Properties);
		super.selectValueFromMultiSelect(Selector.Slide,Selector1);
	
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
