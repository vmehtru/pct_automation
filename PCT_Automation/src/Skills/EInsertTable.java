package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EInsertTable extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Excel";
		 super.Project_Name="Excel - Insert Table"+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Aastha";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Insert Table";
		 super.FinalDoc="InsertTable.xlsx";
		 super.StartingDoc="InsertTable.xlsx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=4;
		 super.excelColCount=5;
		 super.Setup("EInsertTable");
		
	}
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, PCTSkill.SkillCategory.INSERT, PCTSkill.SkillName.Excel.Insert_Table);
		super.selectValueFromDropDown(PCTSkill.Selector.Sheet, Selector1);
		super.selectValueFromDropDown(Selector.Table, Selector2);
	
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	  
}
