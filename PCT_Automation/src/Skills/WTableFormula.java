package Skills;

import static Utils.MouseKeyboardActions.click;
import static Utils.MouseKeyboardActions.sendKeys;


import java.util.Calendar;

import org.openqa.selenium.Keys;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WTableFormula extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Table Formula "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" by Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Table Formula";
	 super.FinalDoc="Table Formula.docx";
	 super.StartingDoc="Table Formula.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=6;

	 super.Setup("WTableFormula");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();

		super.addSkill(instNo, SkillCategory.TABLE, SkillName.Word.Table_Formula);

		super.selectValueFromDropDown(Selector.Table,Selector1);
		super.selectValueFromMultiSelect(Selector.Cell,Selector2);

		click("AddPermutations");
		super.switchToIFrame("PropertyEditToolIFrame");

		click("FormulaPermutationsTextarea");
		sendKeys("FormulaPermutationsTextarea", Keys.ENTER);
		sendKeys("FormulaPermutationsTextarea", Selector3);
		click("AddPermutationsDone");

		super.switchToDefaultContent();

		super.selectAllProperties();
		super.saveSkill();

		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
