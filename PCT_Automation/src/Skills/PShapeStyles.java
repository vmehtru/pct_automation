package Skills;




import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PShapeStyles extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Shape Styles "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Shape Styles";
		super.FinalDoc="Shape Styles.pptx";
		super.StartingDoc="Shape Styles.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=6;
		super.excelColCount=5;
		super.Setup("PShapeStyles");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DRAWING, SkillName.PPT.Shape_Styles);
		super.selectValueFromDropDown(Selector.Slide, Selector1);
		super.selectValueFromDropDown(Selector.Shape,Selector2);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
