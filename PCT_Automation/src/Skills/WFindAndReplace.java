package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Utils.MouseKeyboardActions;

public class WFindAndReplace extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Find and Replace"+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" by Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Find and Replace";
	 super.FinalDoc="Find and Replace.docx";
	 super.StartingDoc="Find and Replace.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=5;

	 super.Setup("WFindAndReplace");

	}
	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Find_and_Replace);
		MouseKeyboardActions.sendKeys("PartialTextBox",Selector1);
		super.selectValueFromMultiSelect(Selector.Para,Selector2);		
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
