package Skills;





import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class WInsertObject extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Insert Object "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" by Rupsi Mehta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Object";
	 super.FinalDoc="Insert Object.docx";
	 super.StartingDoc="Insert Object.docx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=4;

	 super.Setup("WInsertObject");

	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Word.Insert_Object);

		super.selectValueFromDropDown(Selector.Object,Selector1);

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
