package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PAddLayouttoSlideMaster extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Add Layout to Slide Master "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Add Layout to Slide Master";
		super.FinalDoc="Add Layout to Slide Master.pptx";
		super.StartingDoc="Add Layout to Slide Master.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=8;
		super.excelColCount=5;
		super.Setup("PAddLayouttoSlideMaster");
		
	}
		
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SLIDE_MASTER, SkillName.PPT.Add_Layout_to_Slide_Master);
		super.selectValueFromDropDown(Selector.Slide_Master,Selector1);
		super.selectValueFromDropDown(Selector.Slide_Layout,Selector2);
		
		super.selectAllProperties(); 
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
	
	
}
