package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EGoalSeek extends PCTSkill  {
	
	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Excel";
		 super.Project_Name="Excel - Goal Seek"+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Rupsi";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Goal Seek";
		 super.FinalDoc="Goal Seek.xlsx";
		 super.StartingDoc="Goal Seek.xlsx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=2;
		 super.excelColCount=6;
		 super.Setup("EGoalSeek");
		
	}
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, PCTSkill.SkillCategory.DATA, PCTSkill.SkillName.Excel.Goal_Seek);
		super.selectValueFromDropDown(PCTSkill.Selector.Sheet, Selector1);
		super.addTextToNamedRangeTextBox(Selector.Set_Cell,Selector2);
		super.addTextToNamedRangeTextBox(Selector.Changing_Cell,Selector3);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	
	}
