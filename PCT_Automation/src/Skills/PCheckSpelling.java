package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PCheckSpelling extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="PPT";
	 super.Project_Name="PPT - Check Spelling "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Palak";
	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Check Spelling";
	 super.FinalDoc="CheckSpelling.pptx";
	 super.StartingDoc="CheckSpelling.pptx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";	
	 super.excelRowCount=4;
	 super.excelColCount=6;

	 super.Setup("PCheckSpelling");

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Deselect_first_selector, String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.REVIEW, SkillName.PPT.Check_Spelling);
		super.addTextToTextBox("Spelling", Selector1);
		super.selectValueFromMultiSelectByIndex("paragraph",Deselect_first_selector);
		super.selectValueFromMultiSelectByIndex("paragraph", Selector2);
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
