package Skills;



import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WSmartArtSizeandArrangement extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Word";
		 super.Project_Name="Word - SmartArt Size and Arrangement "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Rupsi";
		 super.SkillDocsPath="PCT_Skill_Documents\\Word\\SmartArt Size and Arrangement";
		 super.FinalDoc="SmartArt Size and Arrangement.docx";
		 super.StartingDoc="SmartArt Size and Arrangement.docx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=4;
		 super.excelColCount=4;

		 super.Setup("WSmartArtSizeandArrangement");

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SMARTART, SkillName.Word.SmartArt_Size_and_Arrangement);
		super.selectValueFromMultiSelect(Selector.SmartArt, Selector1);
		super.selectAllProperties();
		saveSkill();
		validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
