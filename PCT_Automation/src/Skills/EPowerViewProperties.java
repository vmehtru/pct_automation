package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class EPowerViewProperties extends PCTSkill {


		@BeforeClass
		public void Setup() throws Exception
		{
		 super.App="Excel";

		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Power View Properties";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=2;
		 super.excelColCount=7;

		}


		@Test(dataProvider="dp", groups= {"testCase","Mar15"})
		public void TestSkill(String Iteration_no,String FinalDocument,String StartDocument,String SummaryFile,String Selector1 ,String ValidateOn, String ValidateFor) throws Exception
		{

			super.Project_Name="Excel - Power View Properties "+Calendar.getInstance().getTime().toString();
			super.Project_Desc="By Rupsi";
			super.FinalDoc=FinalDocument;
			super.StartingDoc=StartDocument;
			super.Setup("EPowerViewProperties");
			String instNo=super.addInstructiontoPCTProject();
			super.addSkill(instNo, SkillCategory.POWER_VIEW, SkillName.Excel.Power_View_Properties);
			super.selectValueFromDropDown(Selector.Sheet,Selector1);
			super.selectAllProperties();
			saveSkill();
			super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
			testPerfectDocAndScore();
			testStartingDocAndFeedback(SummaryFile);
            setMasterProjectAndSaveProjectID();

		}

		@Test(groups= {"testSubmission"})
		public void projectScoreAndFeedback() throws Exception
		{
			testPerfectDocAndScore();
			testStartingDocAndFeedback();
		}

	    @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}



		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}


}
