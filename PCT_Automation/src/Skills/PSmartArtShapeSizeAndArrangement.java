package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class PSmartArtShapeSizeAndArrangement extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="PPT";
	 super.Project_Name="PPT - SmartArt Shape Size and Arrangement "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\SmartArt Shape Size and Arrangement";
	 super.FinalDoc="SmartArt Shape Size and Arrangement.pptx";
	 super.StartingDoc="SmartArt Shape Size and Arrangement.pptx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";	
	 super.excelRowCount=5;
	 super.excelColCount=6;

	 super.Setup("PSmartArtShapeSizeAndArrangement");
	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SMARTART, SkillName.PPT.SmartArt_Shape_Size_and_Arrangement);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.SmartArt,Selector2);

		super.selectValueFromMultiSelect(Selector.Shape,Selector3);

		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
