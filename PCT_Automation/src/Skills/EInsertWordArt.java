package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EInsertWordArt extends PCTSkill{
	
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Insert WordArt "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By DhruvD";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Insert WordArt";
	 super.FinalDoc="Insert WordArt.xlsx";
	 super.StartingDoc="Insert WordArt.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=5;
	 super.excelColCount=5;

	 super.Setup("EInsertWordArt");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Excel.Insert_WordArt);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);

		super.selectValueFromDropDown(Selector.WordArt,Selector2);

		super.selectAllProperties();

		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

    @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}


