package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class EPageSetupSheet extends PCTSkill{
	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Excel";
		 super.Project_Name="Excel - Page Setup - Sheet "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Aastha";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\PageSetup-Sheet";
		 super.FinalDoc="Pagesetup.xlsx";
		 super.StartingDoc="Pagesetup.xlsx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=3;
		 super.excelColCount=4;
		 super.Setup("EPageSetupSheet");
		
	}
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, PCTSkill.SkillCategory.PAGE_LAYOUT, PCTSkill.SkillName.Excel.Page_Setup_Sheet);
		super.selectValueFromDropDown(PCTSkill.Selector.Sheet, Selector1);
	
	    super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
}
