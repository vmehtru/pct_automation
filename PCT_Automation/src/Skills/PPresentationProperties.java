package Skills;



import java.io.IOException;
import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PPresentationProperties extends PCTSkill {

	@BeforeClass
	public void Setup() throws IOException
	{
		super.App="PPT";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Presentation Properties";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=2;
		super.excelColCount=6;
	

	}

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases(String Iteration_no,String FinalDocument,String StartDocument,String SummaryFile, String ValidateOn, String ValidateFor) throws Exception
	{
		super.Project_Name="PPT - Presentation Properties "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Rupsi";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("PPresentationProperties");
		String instNo=super.addInstructiontoPCTProject();
		
		super.addSkill(instNo, SkillCategory.FILE, SkillName.PPT.Presentation_Properties);

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);
		setMasterProjectAndSaveProjectID();
	}


	

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
