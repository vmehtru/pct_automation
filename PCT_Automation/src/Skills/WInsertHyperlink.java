package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WInsertHyperlink extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Insert Hyperlink "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Dhruv";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Hyperlink";
	 super.FinalDoc="Insert_Hyperlink.docx";
	 super.StartingDoc="Insert_Hyperlink.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=5;
	 super.excelColCount=5;

	 super.Setup("WInsertHyperlink");
	}

	@Test(dataProvider="dp")
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();

		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Word.Insert_Hyperlink);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Text Hyperlinks"))
		{
			super.selectValueFromMultiSelect(Selector.Hyperlink,Selector2);
		}

		else if (Selector1.equals("Image Hyperlinks"))
		{
			super.selectImageFromMultiSelectByIndex(Selector.Hyperlink , Selector2);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}


	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
