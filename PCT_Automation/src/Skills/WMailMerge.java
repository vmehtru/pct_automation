package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WMailMerge extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="Word";
	 super.Project_Name="Word - Mail Merge "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created by Prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Mail Mege";
	 super.FinalDoc="Mail merge.docx";
	 super.StartingDoc="Mail merge.docx";
	 
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=5;
	
	 super.Setup("WMailMerge");

	}	
	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		
		String instNo=super.addInstructiontoPCTProject();
		
		
		super.addSkill(instNo, SkillCategory.MAILINGS, SkillName.Word.Mail_Merge);
		super.selectValueFromDropDown(Selector.Merge,Selector1);
		if(Selector1.equals("Labels"))
			super.selectValueFromMultiSelect(Selector.Table, Selector2);
		else
		super.selectValueFromMultiSelect(Selector.Content, Selector2);
		
		saveSkill();
		
	} 
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
}
