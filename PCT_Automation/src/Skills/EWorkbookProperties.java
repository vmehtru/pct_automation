package Skills;




import java.io.IOException;
import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EWorkbookProperties extends PCTSkill {
	@BeforeClass
	public void Setup() throws IOException
	{
	 super.App="Excel";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Workbook Properties";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=6;

	

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String FinalDoc, String StartingDoc, String FeedbackTextFile, String ValidateOn, String ValidateFor) throws Exception
	{
		 super.Project_Name="Excel - Workbook Properties "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Rupsi";
		 super.FinalDoc=FinalDoc;
		 super.StartingDoc=StartingDoc;
		 super.Setup("EWorkbookProperties");
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.FILE, SkillName.Excel.Workbook_Properties);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
		testPerfectDocAndScore();
		testStartingDocAndFeedback(FeedbackTextFile);
		setMasterProjectAndSaveProjectID();

	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
