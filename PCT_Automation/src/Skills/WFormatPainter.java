package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WFormatPainter extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="Word";
	 super.Project_Name="Word - Format Painter"+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Palak Bansal";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Format Painter";
	 super.FinalDoc="testcase.docx";
	 super.StartingDoc="testcase.docx";
	 super.excelDataSourcePath="Data Source.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=5;
	 super.Setup("WFormatPainter");
    }	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		
		String instNo=super.addInstructiontoPCTProject();
		
		
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Format_Painter);
		
		super.selectValueFromDropDown(Selector.Source_Paragraph,Selector1);
		super.selectValueFromMultiSelect(Selector.Target_Paragraph,Selector2);
		saveSkill();	
	} 
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
		
}
