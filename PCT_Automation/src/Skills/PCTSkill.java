package Skills;


import static Utils.MouseKeyboardActions.mouseMove;

import static Utils.MouseKeyboardActions.sendKeys;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.Set;

import junit.framework.Assert;

import org.testng.annotations.AfterSuite;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import PCTProjects.ProjectActions;
import PCTProjects.LaunchProject;
import Utils.excelutils;
import Common.ElementRepository;

import  Common.Log;
import Common.PCTLogin;
import Common.SingleWebDriverObject;
import static Utils.MouseKeyboardActions.click;



@SuppressWarnings("deprecation")

public class PCTSkill {

	public static  String Skillname;
	public static  String ProjectID;
	String App;
	String Project_Name;
	String Project_Desc;
	int AppID=-1;

	String SkillDocsPath;
	String FinalDoc;
	String StartingDoc;

	String excelDataSourcePath;
	String excelSheetname;
	int excelRowCount;
	int excelColCount;
	String filename;
	
	String blankDocToCheckFeedbackText;
	static boolean saveProjectIDs=false;
	String FeedbackTextFile="Summary.htm";
	public static String Version,Version1;
	public static String Browser;
	public static String Version_ID,Version_ID1;
	public String strStartPath = "";
	
	public static class Selector
	{
		static String ApplyTo="Apply";
		static String Para="aragraph";
		static String Table="Table";
		static String Shape="Shape";
		static String Chart="Chart";
		static String ChartElement="Chart Element";
		static String Slide="Slide";
		static String Sheet="heet";
		static String ComboChart="Combo Chart";
		static String DataSeries="Data Series";
		static String Cell="Cell";
		static String Picture="Picture";
		static String SortRange="Sort Range";
		static String SortingLevel="Sorting Level";
		static String DropCap="Drop Cap";
		static String Rows="Row";
		static String Text="Text";
		static String Header="Header";
		static String Footer="Footer";
		static String Hyperlink = "yperlink";
		static String Animation="Animation";
		static String Column="Column";
		static String Comment = "Comment";
		static String ChartData= "Chart Data";
		static String Item="Item";
		static String Object="Object";
		static String WordArt="WordArt";
		static String Control="ontrol";
		static String Bullet="Bullet";
		static String Citation="Citation";
		static String ChartAxis="Chart Axis";
		static String Entity="Entit";
		static String Group="Group";
		static String CheckSkillOn="Check Skill on";
		static String CheckPCTFeatureon="Check PCT Feature on";
		static String Map="Map";
		static String Slicer="Slicer";
		static String Series="Series";
		static String Relationship="Relationship";
		static String Element="Element";
		static String Data_Label="Data Label";
		static String Field="Field";
		static String Textbox="Textbox";
		static String Shapes_starting_with_text="Shape(s) starting with text";
		static String Property="Property";
		static String Section="ection";
		static String Validation="Validation";
		static String Content="ontent";
		static String Caption="Caption";
		static String Guide="Guide";
		static String Entities="Entities";
		static String Merge="Merge";
		static String Cross_Reference="Cross Reference";
		static String Data="Data";
		static String Set_Cell="Set cell";
		static String Changing_Cell="By changing cell";
		static String Video="Video";
		static String bullet_formatting="without";
		static String Slide_Master="Slide Master";
		static String Slide_Layout="Slide Layout";
		static String Source_Paragraph="Source Paragraph";
		static String Target_Paragraph="Target Paragraph";
		static String Spelling="Spelling";
		static String Tab="Tab";
		static String SmartArt_starting_with_text="SmartArt(s) starting with text";
		static String SmartArt="SmartArt";
		static String Media="Media";
		static String Chart_Data="Chart Data";
		static String Data_Point="Data Point";
		static String Document_Properties ="Document Properties";
		static String Endnote="Endnote";
		static String Footnote="Footnote";
		static String Source="Source";
		static String Named_Range="Named Range";
		static String PivotTable="PivotTable";
		static String Signature="Signature";
		static String Symbol="ymbol";
		static String Trendline="Trendline";
		static String Scenario="Scenario";
		static String Type="Type";
		static String Name="Name";
		static String Constraint="Constraint";
		static String Style="Style";
		static String Sparkline="Sparkline";
		static String Range="Range";
		static String Conditional_Formatting="Conditional Formatting";
		static String Select_the_Text="Select the Text";
		static String Title="Title";
		static String Value="Value";
		static String Debit="Debit";
		static String Credit="Credit";
		static String Total_1="Total #1";
		static String Total_2="Total #2";
		static String AccountEntry="Account Entry";
		static String Trial_Balance_with_title="Trial Balance with title";
		static String Columntitle="Column Title";
		static String Source_cell="Source cell";
		static String Destination_worksheet="Destination worksheet";
		static String Destination_cell="Destination cell";
		
	}

	public static class SkillName
	{
		public static class Excel
		{
			static String Number_Formatting="Number Formatting";
			static String Merge_Cells="Merge Cells";
			static String Cell_Alignment_Indentation="Cell Alignment/Indentation";
			static String Font_Formatting="Font Formatting";
			static String Page_Setup_Margin="Page Setup - Margin";
			static String Headers_and_Footers="Headers and Footers";
			static String Apply_Filters="Apply Filters";
			static String Sort_Data="Sort Data";
			static String Insert_Rows="Insert Rows";
			static String Insert_Worksheet="Insert Worksheet";
			static String Insert_WordArt="Insert WordArt";
			static String Apply_Cell_Styles="Apply Cell Styles";
			static String Column_Properties="Column Properties";
			static String Check_Spelling="Check Spelling";
			static String Insert_Chart="Insert Chart";
			static String Chart_Area_Fill_and_Line_Options="Chart Area Fill and Line Options";
			static String Workbook_Theme="Workbook Theme";
			static String Workbook_Properties="Workbook Properties";
			static String Create_Sparklines="Create Sparklines";
			static String Sparkline_Attributes="Sparkline Attributes";
			static String Border_Formatting="Border Formatting";
			static String Fill_Formatting="Fill Formatting";
			static String Conditional_Formatting="Conditional Formatting";
			static String Row_Properties="Row Properties";
			static String Chart_Elements_Fill_and_Line_options="Chart Elements Fill and Line options";
			static String Chart_Elements_Text_Options="Chart Elements Text Options";
			static String Plot_Area_Fill_and_Line_Options="Plot Area - Fill and Line Options";
			static String Add_Chart_Elements="Add Chart Elements";
			static String Page_Setup_Page="Page Setup - Page";
			static String Data_Label_Data_Label_Options="Data Label - Data Label Options";
			static String Data_Label_Fill_and_Line_Options="Data Label - Fill and Line Options";
			static String Page_Setup_Sheet="Page Setup - Sheet";
			static String Insert_Table="Insert Table";
			static String Table_Properties="Table Properties";
			static String Chart_Data_Point_Options="Chart Data Point Options";
			static String Named_Range="Named Range";
			static String Import_Data_From_File="Import Data From File";
			static String Scenario_Manager="Scenario Manager";
			static String Insert_Control="Insert Control";
			static String Protect_Workbook="Protect Workbook";
			static String Format_Control_Size="Format Control - Size";
			static String Solver_Parameters="Solver Parameters";
			static String Solver_Constraints="Solver Constraints";
			static String Sorting_Table_Columns="Sorting Table Columns";
			static String Formulas_n_Functions="Formulas & Functions";
			static String Unmerge_Cells="Unmerge Cells";
			static String Chart_Axis_Options="Chart Axis Options";
			static String Data_Table="Data Table";
			static String Format_Control_Control="Format Control - Control";
			static String Goal_Seek="Goal Seek";
			static String Data_Validation="Data Validation";
			static String Cell_Protection="Cell Protection";
			static String Insert_Hyperlink="Insert Hyperlink";
			static String Modify_Cell_Contents="Modify Cell Contents";
			static String PivotTable_Properties="PivotTable Properties";
			static String Insert_PivotTable="Insert PivotTable";
			static String PivotTable_Fields="PivotTable Fields";
			static String Insert_Slicer="Insert Slicer";
			static String Format_Slicer_Size_and_Properties="Format Slicer - Size and Properties";
			static String Chart_Properties="Chart Properties";
			static String Excel_Options_Advanced="Excel Options - Advanced";
			static String Track_Changes="Track Changes";
			static String Document_Inspector="Document Inspector";
			static String Insert_Comment="Insert Comment";
			static String Shape_Content="Shape Content";
			static String Shape_Text_Alignment="Shape Text Alignment";
			static String Merge_Workbooks="Merge Workbooks";
			static String Shape_Fill_and_Line_Options="Shape Fill and Line Options";
			static String Shape_Size_and_Arrangement="Shape Size and Arrangement";
			static String Insert_Shape="Insert Shape";
			static String Custom_Combination_Chart_Properties="Custom Combination Chart Properties";
			static String Worksheet_Properties="Worksheet Properties";
			static String Create_Relationship="Create Relationship";
			static String Power_View_Textbox="Power View Textbox";
			static String Power_View_Filters="Power View Filters";
			static String Power_View_Chart_Properties="Power View - Chart Properties";
			static String Power_View_Map_Properties="Power View - Map Properties";
			static String Power_View_Table_Properties="Power View - Table Properties";
			static String PowerPivot_Tables="PowerPivot Tables";
			static String PowerPivot_Table_Columns="PowerPivot Table Columns";
			static String Power_View_Properties="Power View Properties";
			static String Power_View_Slicer="Power View Slicer";
			static String Power_View_Chart_Fields="Power View Chart Fields";
			static String Power_View_Map_Fields="Power View Map Fields";
			static String Power_View_Table_Fields="Power View Table Fields";
			static String  PivotChart_Filters = "PivotChart Filters";
			static String  PivotChart_Fields = "PivotChart Fields";
			static String  Protect_Sheet = "Protect Sheet";
			static String Insert_SmartArt="Insert SmartArt";
			static String Advanced_Filter="Advanced Filter";
			static String Add_Modify_Chart_Data="Add/Modify Chart Data";
			static String SmartArt_Design="SmartArt Design";
			static String Format_Painter="Format Painter";
			public static String SmartArt_Size_and_Arrangement="SmartArt Size and Arrangement";
			public static String Picture_Size_and_Arrangement="Picture Size and Arrangement";
			public static String Insert_Picture="Insert Picture";
			public static String Add_Shapes_to_SmartArt="Add Shapes to SmartArt";


		}
		public static class PPT
		{

			static String Apply_Theme="Apply Theme";
			static String Insert_Slides="Insert Slides";
			static String Insert_SmartArt="Insert SmartArt";
			static String SmartArt_Design="SmartArt Design";
			static String Insert_Media="Insert Media";
			static String Media_Size_and_Arrangement="Media Size and Arrangement";
			static String Audio_Playback_Options="Audio Playback Options";
			static String Apply_Edit_Transitions="Apply/Edit Transitions";
			static String Check_Spelling="Check Spelling";
			static String Find_and_Replace_Text="Find and Replace Text";
			static String Align_Selected_Objects="Align Selected Objects";
			static String Group_Objects="Group Objects";
			static String Paragraph_Indents_and_Spacing="Paragraph Indents and Spacing";
			static String Slide_Properties="Slide Properties";
			static String Presentation_Properties="Presentation Properties";
			static String Add_Modify_Notes_Pane_Text="Add/Modify Notes Pane Text";
			static String Reorder_Slides="Reorder Slides";
			static String Video_Styles="Video Styles";
			static String Picture_Styles="Picture Styles";
			static String Shape_Styles="Shape Styles";
			static String Format_Shape_Effects="Format Shape Effects";
			static String Format_Painter="Format Painter";
			static String Insert_WordArt="Insert WordArt";
			static String Format_Shape_Text_Fill="Format Shape - Text Fill";
			static String Format_Shape_Text_Effects="Format Shape - Text Effects";
			static String Import_Slides="Import Slides";
			static String Remove_Bullets="Remove Bullets";
			static String Video_Playback_Options="Video Playback Options";
			static String Insert_Chart="Insert Chart";
			static String Chart_Properties="Chart Properties";
			static String Insert_Table="Insert Table";
			static String SmartArt_Size_and_Arrangement="SmartArt Size and Arrangement";
			static String SmartArt_Shape_Size_and_Arrangement="SmartArt Shape Size and Arrangement";
			static String Add_Chart_Elements="Add Chart Elements";
			static String Table_Row_Properties="Table Row Properties";
			static String Table_Size_and_Arrangement="Table Size and Arrangement";
			static String Table_Content="Table Content";
			static String Merge_Cells="Merge Cells";
			static String Paragraph_Add_or_Remove_Columns="Paragraph - Add or Remove Columns";
			static String Action_Settings="Action Settings";
			static String Insert_Hyperlink="Insert Hyperlink";
			static String Add_Modify_Text_in_SmartArt_Shape="Add/Modify Text in SmartArt Shape";
			static String Add_Shapes_to_SmartArt="Add Shapes to SmartArt";
			static String Font_Formatting="Font Formatting";
			static String Text_Alignment_and_Direction="Text Alignment and Direction";
			static String Table_Column_Properties="Table Column Properties";
			static String Picture_Clip_Art_Size_and_Arrangement="Picture/Clip Art Size and Arrangement";
			static String Insert_Rows_in_a_Table="Insert Rows in a Table";
			static String Chart_Data_Point_Options="Chart Data Point Options";
			static String Insert_Picture="Insert Picture";
			static String Add_Modify_Chart_Data="Add/Modify Chart Data";
			static String Shape_Size_and_Arrangement="Shape Size and Arrangement";
			static String Insert_Shape="Insert Shape";
			static String Add_Layout_to_Slide_Master="Add Layout to Slide Master";
			static String Document_Inspector="Document Inspector";
			static String Insert_Slide_Master="Insert Slide Master";
			static String Format_Shape_Fill_and_Line="Format Shape - Fill and Line";
			static String Format_Slide_Background="Format Slide Background";
			static String Insert_Comment="Insert Comment";
			static String Slide_Headers_and_Footers="Slide - Headers and Footers";
			static String Picture_Clip_Art_Adjustments="Picture/Clip Art Adjustments";
			static String Show_Hide_Objects="Show/Hide Objects";
			static String Insert_Column="Insert Column";
			static String Insert_Control="Insert Control";
			static String Control_Size_and_Position="Control Size and Position";
			static String Insert_Object="Insert Object";
			static String Add_Modify_Text="Add/Modify Text";
			static String Ink_Annotations="Ink Annotations";
			static String Bullets_and_Numbering="Bullets and Numbering";
			static String Protect_Presentation="Protect Presentation";
			static String Apply_Modify_Animation_Options="Apply/Modify Animation Options";
			static String Format_Picture_Effects="Format Picture Effects";
			static String Character_Spacing="Character Spacing";
			static String Notes_and_Handouts_Headers_and_Footers="Notes and Handouts - Headers and Footers";
			static String Table_Properties="Table Properties";
			static String  Cell_Margins = "Cell Margins";
			static String  Apply_Shading = "Apply Shading";
			static String  Chart_Elements_Text_Options = "Chart Elements Text Options";
			static String  Custom_Slide_Show = "Custom Slide Show";
			static String  Add_Guide = "Add Guide";
			static String Set_Up_Slide_Show="Set Up Slide Show";
			static String Format_Shape_Textbox="Format Shape - Textbox";
			static String Object_Size_and_Arrangement="Object Size and Arrangement";
			static String Order_Objects="Order Objects";
			static String Picture_Border="Picture Border";
			static String  Insert_Symbol="Insert Symbol";
			static String Add_Section="Add Section";
			static String Chart_SizeAndArrangement="Chart Size and Arrangement";
			static String Picture_Size_and_Arrangement="Picture Size and Arrangement";
			static String Picture_Adjustments="Picture Adjustments";
			static String Font_Formatting_Word_Character_Level="Font Formatting - Word/Character Level";


		}
		public static class Word
		{
			static String Add_Modify_Paragraph_Text="Add/Modify Paragraph Text";
			static String Insert_Breaks="Insert Breaks";
			static String Insert_Text_From_File="Insert Text From File";
			static String Add_Modify_Watermark="Add/Modify Watermark";
			static String Format_Painter="Format Painter";
			static String Format_Picture_Effects="Format Picture Effects";
			static String Format_Picture_Fill_and_Line_Options="Format Picture - Fill and Line Options";
			static String Format_Picture_Picture_Options="Format Picture - Picture Options";
			static String Insert_Picture="Insert Picture";
			static String Picture_Size_and_Arrangement="Picture Size and Arrangement";
			static String Mail_Merge="Mail Merge";
			static String Page_Setup_Column="Page Setup - Column";
			static String Page_Setup_Margin ="Page Setup - Margin";
			static String Add_Tabs="Add Tabs";
			static String Paragraph_Line_and_Page_Breaks="Paragraph - Line and Page Breaks";
			static String Paragraph_Indents_and_Spacing="Paragraph - Indents and Spacing";
			static String Add_Modify_Endnotes="Add/Modify Endnotes";
			static String Add_Modify_Footnotes="Add/Modify Footnotes";
			static String Edit_Citation="Edit Citation";
			static String Insert_Bibliography="Insert Bibliography";
			static String Manage_Sources="Manage Sources";
			static String Spelling_and_Grammar="Spelling and Grammar";
			static String Apply_Styles_to_Paragraphs="Apply Styles to Paragraphs";
			static String Find_and_Replace="Find and Replace";
			static String Header_Footer_Properties="Header/Footer Properties";
			static String SmartArt_Styles="SmartArt Styles";
			static String Insert_Shape="Insert Shape";
			static String Insert_SmartArt="Insert SmartArt";
			static String Shape_Styles="Shape Styles";
			static String SmartArt_Content="SmartArt Content";
			static String SmartArt_Size_and_Arrangement="SmartArt Size and Arrangement";
			static String Format_Shape_Fill_and_Line_Options="Format Shape - Fill and Line Options";
			static String Insert_Video="Insert Video";
			static String Video_Size_and_Arrangement="Video Size and Arrangement";
			static String Shape_Size_and_Arrangement="Shape Size and Arrangement";
			static String Shape_Content="Shape Content";
			static String Apply_Page_Borders="Apply Page Borders";
			static String Insert_WordArt="Insert WordArt";
			static String Protect_Document="Protect Document";
			static String Envelope_Options="Envelope Options";
			static String Table_Of_Contents="Table Of Contents";
			static String Table_Of_Figures="Table Of Figures";
			static String Mark_Index_Entry="Mark Index Entry";
			static String Insert_Caption="Insert Caption";
			static String Insert_Cover_Page="Insert Cover Page";
			static String Insert_Index="Insert Index";
			static String Page_Number_Format="Page Number Format";
			static String Insert_Document_Property="Insert Document Property";
			static String Table_Of_Authorities="Table Of Authorities";
			static String Insert_Chart="Insert Chart";
			static String Mark_Citation="Mark Citation";
			static String Add_Chart_Elements="Add Chart Elements";
			static String Chart_Properties="Chart Properties";
			static String Link_Headers_and_Footers="Link Headers and Footers";
			static String Insert_Object="Insert Object";
			static String Content_Control_Properties="Content Control Properties";
			static String Header_and_Footer="Header and Footer";
			static String Dropdown_List_Properties="Drop-down List Properties";
			static String Document_Properties="Document Properties";
			static String Group_Objects="Group Objects";
			static String Insert_Symbol="Insert Symbol";
			static String Document_Formatting="Document Formatting";
			static String Insert_Comment="Insert Comment";
			static String Add_Modify_Chart_Data="Add/Modify Chart Data";
			static String Font_Formatting="Font Formatting";
			static String Chart_Size_And_Arrangement="Chart Size And Arrangement";
			static String Bullets_and_Numbering="Bullets and Numbering";
			static String Insert_Control="Insert Control";
			static String Text_Form_Field_Options="Text Form Field Options";
			static String WordArt_Styles="WordArt Styles";
			static String Page_Background_Page_Color="Page Background - Page Color";
			static String Apply_Borders="Apply Borders";
			static String Apply_Shading="Apply Shading";
			static String Insert_Citation="Insert Citation";
			static String Insert_Table="Insert Table";
			static String Table_Cell_Properties="Table Cell Properties";
			static String Table_Row_Properties="Table Row Properties";
			static String Table_Properties="Table Properties";
			static String Table_Styles="Table Styles";
			static String Table_Formula="Table Formula";
			static String Table_Column_Properties="Table Column Properties";
			static String Insert_Rows_in_a_Table="Insert Rows in a Table";
			static String Insert_Bookmark="Insert Bookmark";
			static String Table_Data="Table Data";
			static String Merge_Cells="Merge Cells";
			static String Sort_Table="Sort Table";
			static String Drop_Cap="Drop Cap";
			static String Insert_Hyperlink="Insert Hyperlink";
			static String Font_Formatting_Word_Character_Level="Font Formatting - Word/Character Level";
			static String Insert_Cross_Reference = "Insert Cross Reference";
			static String Chart_Style = "Chart Style";
			static String Picture_Styles="Picture Styles";
			static String Chart_Styles="Chart Styles";
			static String Page_Setup_Paper="Page Setup - Paper";
			static String Signature_Setup="Signature Setup";
			static String CreateModify_Style="Create/Modify Styles";
			static String Apply_Page_Border="Apply Page Border";
			static String Chart_Elements_Fill_and_Line_Options="Chart Elements Fill and Line options";
			static String Document_Formatting_Style_Set="Document Formatting - Style Set";
		}
		public static class Accounting
		{
			static String Generic_AccountProperties_AccountValue="Generic Account Properties - Account/Value";
			static String Accounting_Generic_Account_Properties_DrCr="Generic Account Properties - Dr/Cr";
			static String Generic_Totals ="Generic - Totals";
			static String Trial_Balance_Account_Properties="Trial Balance - Account Properties";
			static String Trial_Balance_Totals="Trial Balance - Totals";
			static String Generic_AccountProperties_MultipleAccountValue="Generic Account Properties - Account/Multiple Values";
			static String Group_Account_Properties_Account_Value="Group Account Properties - Account/Value";
		}
	}

	public static class SkillCategory
	{
		static String REVIEW="REVIEW";
		static String INSERT="INSERT";
		static String HOME="HOME";
		static String REFERENCES="REFERENCES";
		static String PAGE_LAYOUT="PAGE LAYOUT";
		static String PICTURE="PICTURE";
		static String TABLE="TABLE";
		static String DRAWING="DRAWING";
		static String SMARTART="SMARTART";
		static String MAILINGS="MAILINGS";
		static String FILE="FILE";
		static String DESIGN="DESIGN";
		static String HEADER_AND_FOOTER="HEADER AND FOOTER";
		static String DEVELOPER="DEVELOPER";
		static String CHART="CHART";
		static String DATA="DATA";
		static String FORMULAS="FORMULAS";
		static String SPARKLINE="SPARKLINE";
		static String SLICER="SLICER";
		static String PIVOT="PIVOT";
		static String SHAPE="SHAPE";
		static String ANIMATIONS="ANIMATIONS";
		static String AUDIO="AUDIO";
		static String SLIDE_MASTER="SLIDE MASTER";
		static String TRANSITIONS="TRANSITIONS";
		static String VIDEO="VIDEO";
		static String VIEW="VIEW";
		static String INK="INK";
		static String POWER_VIEW="POWER VIEW";
		static String POWERPIVOT="POWERPIVOT";
		static String SLIDE_SHOW="SLIDE SHOW";
		static String GENERICSKILLS="GENERIC FEATURES";
		static String TRIALBALANACE="TRIAL BALANCE";
		static String GROUPFEATURES="GROUP FEATURES";

	}
	
	
	
	@BeforeSuite
@Parameters({ "Version1" })
	public void addVersion(String Version1)
	{
			Version=Version1;
			System.out.println(Version);
			
			if (Version.equalsIgnoreCase("office2016"))
				Version_ID ="office2016_radiobtn";
			if (Version.equalsIgnoreCase("office2013"))
					Version_ID="office2013_radiobtn";
			//if (Version.equalsIgnoreCase("Accounting"))
				//Version_ID="Accounting";

	}
	
	@BeforeSuite
	@Parameters({ "Version3" })
		public void addAcounitng(String Version3)
		{
				Version1=Version3;
				System.out.println(Version1);
				if (Version1.equalsIgnoreCase("Accounting"))
					Version_ID1="Accounting";

		}
	@BeforeSuite
	@Parameters({ "Version2" })
		public void Browser(String Browser1)
		{
		Browser=Browser1;
				System.out.println(Browser1);
				

		}
	
	/*@BeforeSuite
	public void checkForReportIntegrity() throws IOException
	{
		Log.info("Inside PCTSkill-checkForReportIntegrity");

		String content = new String(Files.readAllBytes(Paths.get("\\\\"+PCTLogin.machine_name+"\\PPH\\PPH_Source\\ReportingModule\\web.config")));
		if(content.contains("key=\"StarterApp.Settings.ReportsSecurityCheck\" value=\"true\""))
		{
			Log.error("Report Security Token not set to false...Test Stopped...");
			System.out.println("Report Security Token not set to false...Test Stopped...");
			//throw new SkipException ("Report Security Token not set to false...Test Stopped...");


		}

	}*/

	@BeforeSuite
	public void createRepository()
	{
		try{
		Log.info("Inside PCTSkill-CreateRepository");

		ElementRepository.createElementRepository();
		
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-CreateRepository");
			throw new SkipException ("Failed: PCTSkill-CreateRepository");


		}

	}

	@BeforeSuite
	public void launchPCTandLogIn() throws Exception
	{
	try
	{
		Log.info("Inside PCTSkill-launchPCTandLogIn");

		PCTLogin.loginToPCT(Version_ID, Browser);
		ProjectActions.loginToPCT(Version);
		//ImportPCTProjectDefault();
		
		
		
	}catch(Exception e)
	{
		Log.error("Failed: PCTSkill-launchPCTandLogIn");
		throw new SkipException("Failed: PCTSkill-launchPCTandLogIn");
	}

	}

	@BeforeSuite
	@Parameters("markProjectsAsFinal")
	public void saveProjectIDforSkill(String markProjectsAsFinal) throws Exception
	{
		try{
			Log.info(Skillname+"("+ProjectID+")->Inside PCTSkill-saveProjectIDforSkill");


		if(markProjectsAsFinal.equalsIgnoreCase("True")||markProjectsAsFinal.equalsIgnoreCase("Yes")||markProjectsAsFinal.equalsIgnoreCase("1"))
			{
			saveProjectIDs=true;
			Log.info(Skillname+"("+ProjectID+")->saveProjectIDs set to True");

			}
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-saveProjectIDforSkill");
			throw new Exception("Failed: PCTSkill-saveProjectIDforSkill");
		}

	}



	public void setMasterProjectAndSaveProjectID() throws Exception
	{
	try{
	 if(saveProjectIDs)
	 {
	 PrintWriter out1 = new PrintWriter(new BufferedWriter(new FileWriter(SkillDocsPath+"\\"+"ProjectIDs.txt", true)));
	 out1.println(ProjectID);
	 out1.close();
	 ProjectActions.publish(ProjectID);
	 ProjectActions.setMasterProject(ProjectID);
	 }
	}catch(Exception e)
	{Log.error("Failed: PCTSkill-setMasterProjectAndSaveProjectID");
	throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-setMasterProjectAndSaveProjectID");
	}
	}

	public void Setup(String Skill) throws Exception
	{
		ProjectID=null;
		Skillname=Skill;
System.out.println(Skillname);

		//strStartPath="";
		 if (Version.equals("office2016") && strStartPath=="")
		 {
			 
			 strStartPath = "PCT16\\";
			 { 
				 if (SkillDocsPath.contains("PCT16"))
			 {
				 SkillDocsPath=SkillDocsPath;
			 }
			 else
			 SkillDocsPath=strStartPath +SkillDocsPath;}
		 }
		
		try{
		Log.info("Inside PCTSkill-Setup");


		 ProjectID= createPCTProjectForSkill();
		 if (ProjectID=="-1")
		 {
			 Log.error("Something went Wrong while Creating PCT Project. Skipping Test...");

			 throw new Exception ("Something went Wrong while Creating PCT Project. Skipping Test... ");
		 }


		 LaunchProject.Launch_PCTProjectID(ProjectID);
	//	PCTCommentonProject(ProjectID);

		 //Setting Document for Feedback Text
		 if(AppID==10 || AppID==14)
			 blankDocToCheckFeedbackText=strStartPath + "PCT_Skill_Documents\\Word\\ForSummaryReport.docx";
		 else if(AppID==11 || AppID==15)
			 blankDocToCheckFeedbackText= strStartPath +"PCT_Skill_Documents\\Excel\\ForSummaryReport.xlsx";		
		 else if(AppID==12 || AppID==16)
			 blankDocToCheckFeedbackText=strStartPath + "PCT_Skill_Documents\\PPT\\ForSummaryReport.pptx";
		 else if(AppID==5 || AppID==18)
			 blankDocToCheckFeedbackText= strStartPath +"PCT_Skill_Documents\\Accounting\\ForSummaryReport.xlsx";	
		
			}catch(Exception e)
			{Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-Setup");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-Setup");} 
		/* if(AppID==10)
			 blankDocToCheckFeedbackText="PCT_Skill_Documents\\Word\\ForSummaryReport.docx";
		 else if(AppID==11)
			 blankDocToCheckFeedbackText="PCT_Skill_Documents\\Excel\\ForSummaryReport.xlsx";
		 else if(AppID==12)
			 blankDocToCheckFeedbackText="PCT_Skill_Documents\\PPT\\ForSummaryReport.pptx";

		}catch(Exception e)
		{Log.error("Failed: PCTSkill-Setup");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-Setup");
		}*/
	}


	@DataProvider(name="dp")
	public Object[][] readExcel() throws Exception
	{
		
		if (Version.equals("office2016") && strStartPath==""){
			 
			 strStartPath = "PCT16\\";
			 { 
				 if (SkillDocsPath.contains("16"))
			 {
				 SkillDocsPath=SkillDocsPath;
			 }
			 else
			 SkillDocsPath=strStartPath +SkillDocsPath;}
		 }
		
		Object[][] obj=new Object[excelRowCount][excelColCount];
		excelutils e= new excelutils(SkillDocsPath+"\\"+excelDataSourcePath,excelSheetname);
		for(int i=1; i<=excelRowCount; i++)
			for(int j=0; j<excelColCount;j++)
			{
				if(j==0)
				obj[i-1][j]=String.valueOf(((Number)NumberFormat.getInstance().parse(e.getCellDataasstring(i, j))).intValue()).replaceAll("\\u00a0"," ");
				else
				obj[i-1][j]=e.getCellDataasstring(i, j).replaceAll("\\u00a0"," ");

			}
		Log.info("Returning object for dataprovider");
		return obj;
	}

	public void addSkill(String inst_no, String skill_category, String skill_name) throws Exception
	{
		try{
		Log.info("Inside PCTSkill-AddSkill for ProjectID: "+ProjectID+"  SkillName: "+skill_name);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		
		WebElement dropbox;
		if (Version.equals("office2016")||Version1.equals("Accounting"))
		{
			dropbox=DriverObject.driver.findElement(By.xpath(".//*[@id='instructions']/div["+inst_no+"]//p[text()='Drop PCT Features Here']"));
			
				}
		else
		{
			dropbox=DriverObject.driver.findElement(By.xpath(".//*[@id='instructions']/div["+inst_no+"]//p[text()='Drop Skills Here']"));
			
			
		}
		WebElement skillCat=DriverObject.driver.findElement(By.xpath(".//*[@id='accordion']/h3/a[text()='"+skill_category+"']"));
		WebElement skill=DriverObject.driver.findElement(By.xpath(".//*[@id='accordion']//ul/li/div/a[text()='"+skill_name+"']"));
		skillCat.click();

		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ElementRepository.findLocator("FinalDocUploadInProcess")));
		Actions actions = new Actions(DriverObject.driver);
		actions.moveToElement(skill);
		actions.perform();
		Thread.sleep(1000);
		//actions.dragAndDrop(skill,dropbox);
		actions.clickAndHold(skill);
		actions.perform();
		Thread.sleep(1000);

		actions.moveToElement(dropbox);
		actions.perform();
		Thread.sleep(1000);

		actions.release(dropbox);
		actions.perform();
		Thread.sleep(1000);

		skillCat.click();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-addSkill");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-addSkill");
		}

	}

	public void waitforSkillStateChange() throws Exception
	{
		try{
		Log.info("Inside PCTSkill-waitforSkillStateChange");

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ElementRepository.findLocator("SkillUpdatingCurtainBackground")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ElementRepository.findLocator("SkillUpdatingIcon")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ElementRepository.findLocator("SkillEditIconCurtain")));
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-waitforSkillStateChange");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-waitforSkillStateChange");
		}

	}
	public void saveSkill() throws Exception
	{
		try{
		Log.info("Inside PCTSkill-saveSkill");

			SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
			   WebElement element = ElementRepository.findElement(DriverObject.driver,"SaveSkill");
			   int elementPosition = element.getLocation().getY();
			   String js = String.format("window.scroll(0, %s)", elementPosition);
			   ((JavascriptExecutor)DriverObject.driver).executeScript(js);
			   Thread.sleep(2000);
			   element.click();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-saveSkill");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-saveSkill");
		}
	}

	public void selectValueFromMultiSelect(String DropDownName, String textToSelect) throws Exception
	{
		try{
			Log.info("Inside PCTSkill-selectValueFromMultiSelect for DropDownName: "+DropDownName+"  and textToSelect: "+textToSelect);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//label[contains(text(),'"+DropDownName+"')]/..//div[@class='rawOptionDiv']/input[contains(@name,'"+textToSelect+"')]";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-selectValueFromMultiSelect");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromMultiSelect");
		}

	}

	public void selectValueFromMultiSelectByIndex(String DropDownName , String index) throws Exception
	{
		try{
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,180);
		String xpath=".//label[contains(text(),'"+DropDownName+"')]/..//div[@class='BorderedDiv']/div["+index+"]/input[@type='checkbox']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-selectValueFromMultiSelectByIndex");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromMultiSelectByIndex");
		}
	}


	public void selectValueFromMultiSelectAdvanced(String DropDownName, String textToSelect) throws Exception
	{
		try{
		Log.info("Inside PCTSkill-selectValueFromMultiSelectAdvanced for DropDownName: "+DropDownName+"  and textToSelect: "+textToSelect);
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//label[contains(text(),'"+DropDownName+"')]/../..//div[@class='searchOptionDiv']/span[contains(text(),'"+textToSelect+"')]/../input";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{Log.error("Failed: PCTSkill-selectValueFromMultiSelectAdvanced");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromMultiSelectAdvanced");
		}

	}

	public void selectValueFromDropDown(String DropDownName, String textToSelect) throws Exception
	{
		try{
		Log.info("Inside PCTSkill-selectValueFromDropDown for DropDownName: "+DropDownName+"  and textToSelect: "+textToSelect);
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//select/../../label[contains(text(),'"+DropDownName+"')]/../div/select";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		Select ele=new Select((DriverObject.driver.findElement(By.xpath(xpath))));
		ele.selectByVisibleText(textToSelect);

		waitforSkillStateChange();
		}catch(Exception e)
		{Log.error("Failed: PCTSkill-selectValueFromDropDown");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromDropDown");
		}
	}

	public void addTextToTextBox(String textboxName, String textToAdd) throws Exception
	{
		try{
		Log.info(Skillname+"("+ProjectID+")->Inside PCTSkill-addTextToTextBox for textboxName: "+textboxName+"  and textToAdd: "+textToAdd);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String xpath=".//div[@class='LabelInputDiv']/input[contains(@type,'text')]";
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();
		ele.sendKeys(textToAdd);

		waitforSkillStateChange();
		}catch(Exception e)
		{Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-addTextToNamedRangeTextBox");}

	}
	public void addTextToNamedRangeTextBox(String textboxName, String textToAdd) throws Exception
	{
		try{
		Log.info("Inside PCTSkill-addTextToNamedRangeTextBox for textboxName: "+textboxName+"  and textToAdd: "+textToAdd);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String xpath=".//div[@class='skillUIinputarea']//div[@class='cellRangeInputArea']/label[contains(text(),'"+textboxName+"')]/../div/input";
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();
		ele.sendKeys(textToAdd);

		waitforSkillStateChange();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-addTextToNamedRangeTextBox");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-addTextToNamedRangeTextBox");
		}

	}

	public void selectValueFromRadio(String RadioName, String textToSelect) throws Exception
	{
		try{
			Log.info("Inside PCTSkill-selectValueFromRadio for RadioName: "+RadioName+"  and textToSelect: "+textToSelect);


		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//label[contains(text(),'"+RadioName+"')]//../../..//input[contains(@value,'"+textToSelect+"')]";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-selectValueFromRadio");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromRadio");
		}
	}


 public void selectImageFromMultiSelectByTitle(String SelectorName, String ImageTitle) throws Exception
	{
		try{
			Log.info("Inside PCTSkill-selectValueFromMultiSelectPicture for SelectorName: "+SelectorName+"  and ImageTitle: "+ImageTitle);


		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath= ".//label[contains(text(),'"+SelectorName+"')]/../..//ul[@class='unorderedImageList']/li/div/img[@title='"+ImageTitle+"']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();

		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-selectImageFromMultiSelectByTitle");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectImageFromMultiSelectByTitle");
		}
	}
 public void selectImageFromMultiSelectByName(String SelectorName, String ImageTitle) throws Exception
	{
		try{
			Log.info(Skillname+"("+ProjectID+")->Inside PCTSkill-selectValueFromMultiSelectPicture for SelectorName: "+SelectorName+"  and ImageTitle: "+ImageTitle);


		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath= ".//label[contains(text(),'"+SelectorName+"')]/../..//ul[@class='unorderedImageList']/li/div/input[@name='"+ImageTitle+"']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();

		}catch(Exception e)
		{Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectImageFromMultiSelectByTitle");}
	}

	public void selectImageFromMultiSelectByIndex(String DropDownName , String index) throws Exception
	{
		try{
			SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();

		WebDriverWait wait= new WebDriverWait(DriverObject.driver,180);
		String xpath=".//label[contains(text(),'"+DropDownName+"')]/../..//div[@class='BorderedDiv']/ul/li["+index+"]/div/input";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-selectImageFromMultiSelectByIndex");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectImageFromMultiSelectByIndex");
		}
	}

	public void selectAllFromMultiSelect(String DropDownName) throws Exception
	{
		try{
			SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();

		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//label[contains(text(),'"+DropDownName+"')]/..//p[contains(text(),'Select All')]/../span";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{Log.error("Failed: PCTSkill-selectAllFromMultiSelect");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectAllFromMultiSelect");
		}
	}
	public void selectVideoFromMultiSelect(String DropDownName, String textToSelect) throws Exception
	{
		try{
			Log.info(Skillname+"("+ProjectID+")->Inside PCTSkill-selectValueFromMultiSelect for DropDownName: "+DropDownName+"  and textToSelect: "+textToSelect);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//label[contains(text(),'"+DropDownName+"')]/..//..//div[@class='BorderedDiv']//input[contains(@name,'"+textToSelect+"')]";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromMultiSelect");}

	}
	public void switchToIFrame(String IFrameID) throws Exception
	{
		 	try{SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		 	DriverObject.driver.switchTo().frame(IFrameID);
		 	}catch(Exception e)
			{Log.error("Failed: PCTSkill-switchToIFrame");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-switchToIFrame");
			}
	}
	public void switchToIFrame1(String IFrameID) throws Exception
	{
		 	try{SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
			WebDriverWait wait= new WebDriverWait(DriverObject.driver,10);
		 	
	
		 	Thread.sleep(3000);
		 	while (true)
		 	{
		 		try{
		 			DriverObject.driver.switchTo().frame(DriverObject.driver.findElement(By.id("ReportFrame")));
		 			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='totalscore']")));
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='totalscore']")));
		 			DriverObject.driver.findElement(By.xpath(".//*[@id='totalscore']"));
		 			break;
		 		}
		 		catch (Exception e)
		 		{
		 			DriverObject.driver.switchTo().defaultContent();
		 			String xpath=".//label[@id='IntegratedReportCard'][@class='ReportButton TestSubmissionTab ui-button ui-button-text-only ui-state-default ui-state-active']/span";
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("IntegratedReportCard")));
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("IntegratedReportCard")));
					WebElement ele2=DriverObject.driver.findElement(By.id("IntegratedReportCard"));
					ele2.click();}
		 		
		 	}
		 	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='totalscore']/div[2]/span")));
		 	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='totalscore']/div[2]/span")));
			String score=DriverObject.driver.findElement(By.xpath(".//*[@id='totalscore']/div[2]/span")).getText();
			
			System.out.println("Score of Project"+score);
		 	}catch(Exception e)
			{Log.error("Failed: PCTSkill-switchToIFrame");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-switchToIFrame");
			}
	}
	public void switchToDefaultContent() throws Exception
	{
		 try{	
			 SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		 	DriverObject.driver.switchTo().defaultContent();
		 }
		 catch(Exception e)
			{
			 Log.error("Failed: PCTSkill-switchToDefaultContent");
			 throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-switchToDefaultContent");
			}
	}

	public void openAllPropertyGroups() throws Exception
	{
		try{
			Log.info("Inside PCTSkill-openAllPropertyGroups");

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		ListIterator<WebElement> list_ele=ElementRepository.findAllElements(DriverObject.driver,"AllPropertyGroups").listIterator();
		while(list_ele.hasNext())
		{
			list_ele.next().click();
		}

		waitforSkillStateChange();
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-openAllPropertyGroups");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-openAllPropertyGroups");

		}
	}

	public void selectAllProperties() throws Exception
	{
		try{
			Log.info("Inside PCTSkill-selectAllProperties");


		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		openAllPropertyGroups();
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(ElementRepository.findLocator("AllProperties")));
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(ElementRepository.findLocator("AllProperties")));
		Thread.sleep(5000);
		ListIterator<WebElement> list_ele=ElementRepository.findAllElements(DriverObject.driver,"AllProperties").listIterator();
		while(list_ele.hasNext())
		{
			WebElement ele=list_ele.next();
			ele.click();

		}
		}catch(Exception e)
		{Log.error("Failed: PCTSkill-selectAllProperties");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectAllProperties");
		}


	}
	public void selectAllProperties_groupcheckbox() throws Exception
		{
			try{
				Log.info(Skillname+"("+ProjectID+")->Inside PCTSkill-selectAllProperties");
			SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
			WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
			openAllPropertyGroups();
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(ElementRepository.findLocator("AllProperties1")));
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(ElementRepository.findLocator("AllProperties1")));
			Thread.sleep(5000);
			ListIterator<WebElement> list_ele=ElementRepository.findAllElements(DriverObject.driver,"AllProperties1").listIterator();
			while(list_ele.hasNext())
			{
				WebElement ele=list_ele.next();
				ele.click();

			}
			}catch(Exception e)
			{Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectAllProperties");}


	}
public void selectValueFromDropDownForSelectorOfSameName(String DropDownName,String textToSelect) throws Exception
	{
		try{
		Log.info(Skillname+"("+ProjectID+")->Inside PCTSkill-selectValueFromDropDown for DropDownName: "+DropDownName+"  and textToSelect: "+textToSelect);
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//select/option[contains(text(),'"+textToSelect+"')]/../../../label[contains(text(),'"+DropDownName+"')]/../div/select";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		Select ele=new Select((DriverObject.driver.findElement(By.xpath(xpath))));
		ele.selectByVisibleText(textToSelect);

		waitforSkillStateChange();
		}catch(Exception e)
		{Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromDropDown");}
	}

	public String addInstructiontoPCTProject() throws Exception
	{
		Log.info("Inside PCTSkill-addInstructiontoPCTProject");

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();

		click("AddInstructionButton");
		String inst_No=ElementRepository.findElement(DriverObject.driver,"Inst_No").getText();
		sendKeys("InstructionTextBox","Instruction text");
		sendKeys("ScoreTextBox","10");
	//	mouseMove("SaveInst");
		click("SaveInst");
		Log.info("Instruction no. "+inst_No+" saved");

		return inst_No;
	}

	public String addInstructiontoPCTProject(String instText, String hintText, String score) throws Exception
	{
		Log.info("Inside PCTSkill-addInstructiontoPCTProject");

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		click("AddInstructionButton");
		String inst_No=ElementRepository.findElement(DriverObject.driver,"Inst_No").getText();
		sendKeys("InstructionTextBox",instText);
		sendKeys("HintTextBox",hintText);
		sendKeys("ScoreTextBox","score");
		mouseMove("SaveInst");
		click("SaveInst");
		Log.info("Instruction no. "+inst_No+" saved");
		return inst_No;

	}

	@SuppressWarnings("deprecation")
	public void validateSkillSummary(String inst_no, String ExpectedText_CheckOn, String ExpectedText_ValidateFor) throws Exception
	{

		Log.info("Inside PCTSkill-validateSkillSummary");
		Log.info("inst_no: "+inst_no);
		//Log.info("ExpectedText_CheckOn: "+ExpectedText_CheckOn);
		//Log.info("ExpectedText_ValidateFor: "+ExpectedText_ValidateFor);

		int errorCheckOn=0;
		int errorValidateFor=0;

		String actualValueCheckOn = null;
		String actualValueValidateFor = null;
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();


		String toCompareExpectedText_CheckOn=ExpectedText_CheckOn.trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," ");
		String toCompareExpectedText_ValidateFor=ExpectedText_ValidateFor.trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," ");

		String Ele_Summary_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[1]/p/span[2]";
		String Ele_Summary_ValidateFor_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[2]/p/span[2]";

		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);

		try
		{


		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Ele_Summary_Check_xpath)));
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Ele_Summary_ValidateFor_xpath)));

		//Validate Summary-CheckOn
		 actualValueCheckOn=DriverObject.driver.findElement(By.xpath(Ele_Summary_Check_xpath)).getText().trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," ");

		}catch(Exception e){
			Log.error("Failed: PCTSkill-validateSkillSummary");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-validateSkillSummary");
			}


		if(actualValueCheckOn.equals(toCompareExpectedText_CheckOn))
		{
			Log.info("Validate Skill Summary-> CheckOn: Passed");
		}
		else
		{
			errorCheckOn=1;
			Log.error("Validate Skill Summary-> CheckOn: Failed");
			Log.error("Actual->"+actualValueCheckOn);
			Log.error("Expected->"+toCompareExpectedText_CheckOn);
		}

		//Validate Summary-ValidateFor
		try
		{
			actualValueValidateFor=DriverObject.driver.findElement(By.xpath(Ele_Summary_ValidateFor_xpath)).getText().trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," ");
		}catch(Exception e)	{
			Log.error("Failed: PCTSkill-validateSkillSummary");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-validateSkillSummary");
		}

		if(actualValueValidateFor.equals(toCompareExpectedText_ValidateFor))
		{
			Log.info("Validate Skill Summary-> ValidateFor: Passed");
		}
		else
		{
			errorValidateFor=1;
			Log.error("Validate Skill Summary-> ValidateFor: Failed");
			Log.error("Actual->"+actualValueValidateFor);
			Log.error("Expected->"+toCompareExpectedText_ValidateFor);
		}

		if(errorCheckOn==1)
			Assert.failNotEquals("Validate Skill Summary-> Failed for CheckOn",toCompareExpectedText_CheckOn, actualValueCheckOn);

		if(errorValidateFor==1)
			Assert.failNotEquals("Validate Skill Summary-> Failed for ValidateFor",toCompareExpectedText_ValidateFor,actualValueValidateFor);


	}

public void validateSkillSummary_image_presentInValidateFor(String inst_no, String ExpectedText_CheckOn, String ExpectedText_ValidateFor, String ImageSrcValidateFor) throws Exception
	{

		validateSkillSummary(inst_no,ExpectedText_CheckOn,ExpectedText_ValidateFor);

		int error=0;
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String Image_ValidateFor_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[2]/p/span[2]/img";
		  String Ele_Summary_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[1]/p/span[2]";
		  String Ele_Summary_ValidateFor_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[2]/p/span[2]";

		WebDriverWait wait= new WebDriverWait(DriverObject.driver,180);
		try{
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Ele_Summary_Check_xpath)));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Image_ValidateFor_Check_xpath)));
		if(!ImageSrcValidateFor.trim().equals("Errors in Getting Cell Data"))
		   {
			  String actualValue=DriverObject.driver.findElement(By.xpath(Image_ValidateFor_Check_xpath)).getAttribute("src");
			  if(actualValue.trim().equals(ImageSrcValidateFor.trim()))
			    {
			 	Log.info("Validate Summary For Image-> ValidateFor: Passed");
			     }
			 else
			    {
				error=1;

				Log.error("Validate Summary-> ValidateFor: Failed");
				Log.error("Actual->"+actualValue.trim());
				Log.error("Expected->"+ImageSrcValidateFor.trim());
			    }
		 }
		}
		
		catch(Exception e)
		{
			Log.error("Failed: PCTSkill-validateSkillSummary");
	     	throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-validateSkillSummary");
		}

		if(error==1)
		throw new Exception("Validate Summary(Image)-> Failed");
	}
	public void validateSkillSummary_image_presentInCheckOn(String inst_no, String ExpectedText_CheckOn, String ExpectedText_ValidateFor, String ImageSrcCheckOn) throws Exception
	{


		validateSkillSummary(inst_no,ExpectedText_CheckOn,ExpectedText_ValidateFor);

		int error=0;
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String Image_CheckOn_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[1]/p/span[2]/img";
	     String Ele_Summary_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[1]/p/span[2]";


		WebDriverWait wait= new WebDriverWait(DriverObject.driver,180);
		try{
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Ele_Summary_Check_xpath)));

		if((!ImageSrcCheckOn.trim().isEmpty())&&(!ImageSrcCheckOn.trim().equals("Errors in Getting Cell Data")))
		   {
			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Image_CheckOn_xpath)));
			  String actualValue=DriverObject.driver.findElement(By.xpath(Image_CheckOn_xpath)).getAttribute("src");
			  if(actualValue.trim().equals(ImageSrcCheckOn.trim()))
			    {
			 	Log.info("Validate Summary For Image-> CheckON: Passed");
			     }
			 else
			    {
				error=1;

				Log.error("Validate Summary-> ValidateFor: Failed");
				Log.error("Actual->"+actualValue.trim());
				Log.error("Expected->"+ImageSrcCheckOn.trim());
			    }
		 }
		}

		catch(Exception e)
		{
			Log.error("Failed: PCTSkill-validateSkillSummary");
	     	throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-validateSkillSummary");
		}

		if(error==1)
		throw new Exception("Validate Summary(Image)-> Failed");
	}

	public void selectValueFromMultiSelectImageAndText(String DropDownName, String textToSelect,  String ImageSrc) throws Exception
	{
		try{
			Log.info(Skillname+"("+ProjectID+")->Inside PCTSkill-selectValueFromMultiSelectImageAndText: "+DropDownName+"  and textToSelect: "+textToSelect);
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//label[contains(text(),'"+DropDownName+"')]/..//div[@class='rawOptionDiv']/input[contains(@name,'"+textToSelect+"')]";

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele=DriverObject.driver.findElement(By.xpath(xpath));
		if((!ImageSrc.trim().isEmpty())&&(!ImageSrc.trim().equals("Errors in Getting Cell Data")))
		{
			String Image_Selector_Check_xpath=".//label[contains(text(),'"+DropDownName+"')]/..//div[@class='rawOptionDiv']/input[contains(@name,'"+ImageSrc+"')]";
			WebElement ele2=DriverObject.driver.findElement(By.xpath(Image_Selector_Check_xpath));
			ele2.click();
		}
		else
		ele.click();

		waitforSkillStateChange();
		}catch(Exception e)
		{Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-selectValueFromMultiSelect");}

	}
	public void validateSkillSummary(String inst_no, String ExpectedText_CheckOn, String ExpectedText_ValidateFor, String ImageSrc) throws Exception
	{

		validateSkillSummary(inst_no,ExpectedText_CheckOn,ExpectedText_ValidateFor);

		int error=0;
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String Image_ValidateFor_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[2]/p/span[2]/img";
		String Ele_Summary_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[1]/p/span[2]";
		String Ele_Summary_ValidateFor_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[2]/p/span[2]";

		WebDriverWait wait= new WebDriverWait(DriverObject.driver,180);
		try{
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Ele_Summary_Check_xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Ele_Summary_ValidateFor_xpath)));
		if(!ImageSrc.trim().isEmpty())
		{
			String actualValue=DriverObject.driver.findElement(By.xpath(Image_ValidateFor_Check_xpath)).getAttribute("src");
			if(actualValue.trim().equals(ImageSrc.trim()))
			{
				Log.info("Validate Summary For Image-> ValidateFor: Passed");
			}
			else
			{
				error=1;

				Log.error("Validate Summary-> ValidateFor: Failed");
				Log.error("Actual->"+actualValue.trim());
				Log.error("Expected->"+ImageSrc.trim());
			}
		}
		}catch(Exception e)
		{Log.error("Failed: PCTSkill-validateSkillSummary");
		throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-validateSkillSummary");
		}

		if(error==1)
		throw new Exception("Validate Summary(Image)-> Failed");
	}

public void validateSkillSummary(String inst_no, String ExpectedText_CheckOn, String ExpectedText_ValidateFor, String ImageSrc_validationOn,String ImageSrc_validationFOr) throws Exception
	{

	    validateSkillSummary(inst_no,ExpectedText_CheckOn,ExpectedText_ValidateFor);

		int error=0;
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,180);

		if(!ImageSrc_validationOn.trim().isEmpty())
		{
			String Image_ValidateOn_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[1]/p/span[2]/img";
			String Ele_Summary_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[1]/p/span[2]";
		   try {
			    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Ele_Summary_Check_xpath)));
			    String actualValue_checkon_image=DriverObject.driver.findElement(By.xpath(Image_ValidateOn_Check_xpath)).getAttribute("src");
			   if(actualValue_checkon_image.trim().equals(ImageSrc_validationOn.trim()))
			    {
				Log.info(Skillname+"("+ProjectID+")->Validate Summary For Image-> ValidateOn: Passed");
				System.out.println("validationON");
			     }
			   else
			    {
				error=1;
				Log.error(Skillname+"("+ProjectID+")->Validate Summary-> ValidateOn: Failed");
				Log.error(Skillname+"("+ProjectID+")->Actual->"+actualValue_checkon_image.trim());
				Log.error(Skillname+"("+ProjectID+")->Expected->"+ImageSrc_validationOn.trim());
			    }
		      }
		     catch(Exception e)
				  {Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-validateSkillSummary");}
		   if(error==1)
				throw new Exception("Validate SummaryCheckON(Image)-> Failed");
	     	}


		if(!ImageSrc_validationFOr.trim().isEmpty())
		{
			String Image_ValidateFor_Check_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[2]/p/span[2]/img";
			String Ele_Summary_ValidateFor_xpath=".//div[@id='instructions']/div["+inst_no+"]//*[@id='innerdiv']/div[2]/p/span[2]";
		   try {
			    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Image_ValidateFor_Check_xpath)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Image_ValidateFor_Check_xpath)));

			    String actualValue_validateFor_image=DriverObject.driver.findElement(By.xpath(Image_ValidateFor_Check_xpath)).getAttribute("src");
			   if(actualValue_validateFor_image.trim().equals(ImageSrc_validationFOr.trim()))
			    {
				Log.info(Skillname+"("+ProjectID+")->Validate Summary For Image-> ValidateFor: Passed");
				System.out.println("validationFor");
			    }
			   else
			    {
				error=1;
				Log.error(Skillname+"("+ProjectID+")->Validate Summary-> ValidateOn: Failed");
				Log.error(Skillname+"("+ProjectID+")->Actual->"+actualValue_validateFor_image.trim());
				Log.error(Skillname+"("+ProjectID+")->Expected->"+ImageSrc_validationFOr.trim());
			    }
		      }
		 catch(Exception e)
				{
			    Log.error(Skillname+"("+ProjectID+")->Failed: PCTSkill-validateSkillSummary");
				}
		   if(error==1)
			    throw new Exception("Validate SummaryFor(Image)-> Failed");
		}

	}

	public void validate(String element,String expectedValue) throws Exception
	{
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String actualValue=ElementRepository.findElement(DriverObject.driver,element).getText();
		if(actualValue.trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," ").equals(expectedValue.trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," ")))
		{
			Log.info("Validate "+element+": Passed");
		}
		else
		{
			Log.error("Validate "+element+": Failed");
			Log.error("ActualValue: "+actualValue.trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," ")+"\nExpectedValue:"+expectedValue.trim().replaceAll("\\s+", " ").replaceAll("\\u00a0"," "));
		    throw new Exception("Validate "+element+": Failed");
		}
	}


	public String createPCTProjectForSkill() throws IOException
	{
		
		Log.info("Inside PCTSkill-createPCTProjectForSkill for ProjectID: "+ ProjectID);
		if(App.toLowerCase().contains("word"))
		{
		if (Version.equals("office2016"))
		AppID=14; else AppID=10;
		}
	else if(App.toLowerCase().contains("excel") )
		{
		if (Version.equals("office2016"))
		AppID=15; else AppID=11;
		}
	else if(App.toLowerCase().contains("ppt"))
	 {
		if (Version.equals("office2016"))
		 AppID=16; else AppID=12;
	 }
	else if (App.toLowerCase().contains("accounting"))
	{
		if (Version.equals("office2016"))
			 AppID=18; else AppID=5;
	}
	else 
	{
		System.out.println("Invalid application: "+App);
		return (Integer.toString((AppID)));
	}
		
	/*if(App.toLowerCase().contains("word"))
		AppID=10;
	else if(App.toLowerCase().contains("excel"))
		AppID=11;
	else if(App.toLowerCase().contains("ppt"))
		AppID=12;
	else
	{
		System.out.println("Invalid application: "+App);
		return (Integer.toString((AppID)));
	}*/
	ProjectActions.loginToPCT(Version);
	

	String ProjectID=ProjectActions.createPCTProject(AppID, Project_Name, Project_Desc);
		
	
			ProjectActions.uploadlDocumentToPCTProject(ProjectID, SkillDocsPath +"\\" +FinalDoc, "FinalDoc");
			ProjectActions.uploadlDocumentToPCTProject(ProjectID, SkillDocsPath +"\\" +StartingDoc, "StartDoc");
			

		return ProjectID;

	}



	public String testSubmission(String DocPath) throws Exception {

		Log.info("Inside PCTSkill-testSubmission for ProjectID: "+ ProjectID+" DocPath: "+DocPath);

		String subID=ProjectActions.testSubmission(ProjectID, DocPath);
		return subID;

	}

	public void validateScoreCard(String subID) throws Exception {
		Log.info("Inside PCTSkill-validateScoreCard for ProjectID: "+ ProjectID);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String projectScore=DriverObject.driver.findElement(By.xpath("//h3[@id='TotalScoreText']")).getText().toString().trim();
		//if (PCTLogin.url_Ext=="http")
		//{
		ProjectActions.validateScoreCard(subID, projectScore+".000",projectScore+".000");}
	//}

	public void validateScoreCard(String subID, String expectedScore) throws Exception {
		Log.info("Inside PCTSkill-validateScoreCard for ProjectID: "+ ProjectID);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String projectScore=DriverObject.driver.findElement(By.xpath("//h3[@id='TotalScoreText']")).getText().toString().trim();
		ProjectActions.validateScoreCard(subID, projectScore+".000",expectedScore);
	}

	
	public void testPerfectDocAndScore() throws Exception
	{
		
		Log.info("Inside PCTSkill-testPerfectDocAndScore for ProjectID: "+ ProjectID);
		
		//PCTShareProject();
		//ExportPCTProject();
		Thread.sleep(3000);
		//ImportPCTProject();
	      Thread.sleep(3000);
		String subID=testSubmission(SkillDocsPath +"\\" +FinalDoc);
		Thread.sleep(10000);
		//click on view submissison
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//*[@id='ViewResultButton']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele2=DriverObject.driver.findElement(By.xpath(xpath));
		ele2.click();
		if (Version1.equalsIgnoreCase("Accounting") || Version.equalsIgnoreCase("office2016") ){
			Thread.sleep(5000);
		switchToIFrame1("Reportframe");
		DriverObject.driver.switchTo().defaultContent();
		String xpath1="html/body/div[17]/div[3]/div/button";
		//DriverObject.driver.findElement(By.xpath(xpath1)));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath1)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1)));
		WebElement ele3=DriverObject.driver.findElement(By.xpath(xpath1));
		ele3.click();
		DriverObject.driver.switchTo().defaultContent();
	}
		else
		{
		validateScoreCard(subID);
		}
		
		
	}

	public void testStartingDocAndFeedback() throws Exception
	{
		Log.info("Inside PCTSkill-testStartingDocAndFeedback for ProjectID: "+ ProjectID);

		String subID=testSubmission(blankDocToCheckFeedbackText);
		Thread.sleep(10000);
		//click on view submissison
		if (Version1.equalsIgnoreCase("Accounting") || Version.equalsIgnoreCase("office2016") ){
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		String xpath=".//*[@id='ViewResultButton']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele2=DriverObject.driver.findElement(By.xpath(xpath));
		ele2.click();
		//if ((Version.equals("office2016"))||(Version1.equals("Accounting"))){
			Thread.sleep(5000);
		switchToIFrame1("Reportframe");
		DriverObject.driver.switchTo().defaultContent();
		String xpath1="html/body/div[17]/div[3]/div/button";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath1)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1)));
		WebElement ele3=DriverObject.driver.findElement(By.xpath(xpath1));
		ele3.click();
		DriverObject.driver.switchTo().defaultContent();
		
		}
		else{
			if (Version.equalsIgnoreCase("office2013"))
	{
		if (PCTLogin.url_Ext=="http" && Browser=="Chrome")
	{
		validateSummaryReport(subID);
		System.out.println(subID);
		}}}
	
	}
	public void testStartingDocAndFeedback(String feedbackTextFile) throws Exception
	{
		FeedbackTextFile = feedbackTextFile;
		testStartingDocAndFeedback();
	}

	public void validateSummaryReport(String subID) throws Exception {
		Log.info("Inside PCTSkill-validateSummaryReport for ProjectID: "+ ProjectID);
		Log.info("FeedbackTextFile: "+ FeedbackTextFile);

		File fileDir = new File(SkillDocsPath+"\\"+FeedbackTextFile);

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir)));
		String myText = null;
		String line = null;
		while ((line = reader.readLine()) != null) {
		    myText = line;
		}

//System.out.println(myText);
		ProjectActions.validateSummaryReportFeedbackText(subID, myText);
	}



	public void getSavedProjectIDsForSkillandUpgrade() throws Exception
	{
		Log.info("Inside PCTSkill-getSavedProjectIDsForSkillandUpgrade for ProjectID: "+ ProjectID);

		try
		{
       //Get scanner instance
	   Scanner scanner = new Scanner(new File(SkillDocsPath+"\\"+"ProjectIDs.txt"));
	   scanner.useDelimiter("\r\n");

	   	while (scanner.hasNext())
	   	{
    		String ProjectID=scanner.next();
	   		if(ProjectID!="")
	   			upgradePCTProjectForSkill(ProjectID);
	   	}

	    //Do not forget to close the scanner
	    scanner.close();
		}catch(Exception e)
		{
			Log.error("getSavedProjectIDsForSkillandUpgrade Failed");
			throw new Exception(Skillname+"("+ProjectID+")->getSavedProjectIDsForSkillandUpgrade Failed");
		}

	}

	private void upgradePCTProjectForSkill(String ProjectID) throws Exception
	{
		try
		{
		Log.info("Inside PCTSkill-upgradePCTProjectForSkill for ProjectID: "+ ProjectID);

		String ClonedprojectID=ProjectActions.clonePCTProject(ProjectID);
		ProjectActions.upgradeProject(ClonedprojectID);

		ProjectActions.reportSkillsinWarningMode(ClonedprojectID);
		ProjectActions.saveSkillsinWarningMode(ClonedprojectID);

		ProjectActions.reportSkillsinErrorMode(ClonedprojectID);
		ProjectActions.deleteSkillsinErrorMode(ClonedprojectID);

		ProjectActions.reportSkillsinOpenState(ClonedprojectID);

		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		String subID=ProjectActions.testSubmission(ClonedprojectID, SkillDocsPath +"\\" +FinalDoc);
		String projectScore=DriverObject.driver.findElement(By.xpath("//h3[@id='TotalScoreText']")).getText().toString().trim();
		ProjectActions.validateScoreCard(subID, projectScore+".000",projectScore+".000");
		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-upgradePCTProjectForSkill");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-upgradePCTProjectForSkill");

		}
	}
	
	
	private void PCTCommentonProject(String ProjectID) throws Exception
	{
		try
		{
		Log.info("Inside PCTSkill-PCTCommentonProject for ProjectID: "+ ProjectID);
		
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,100);
		String xpath=".//*[@id='ProjectCommentIcon']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele2=DriverObject.driver.findElement(By.xpath(xpath));
		ele2.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ElementRepository.findLocator("CommentEditIconCurtain")));
		String xpath1= ".//*[@id='CommentDialog']/div[3]/div/div[1]";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath1)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1)));
		
		for (int Cmntcnt=1; Cmntcnt<4; Cmntcnt++){
			Thread.sleep(2000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ElementRepository.findLocator("CommentEditIconCurtain")));
		wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator("AddCommentBox")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator("AddCommentBox")));
		sendKeys("AddCommentBox","Comment text");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ElementRepository.findLocator("CommentEditIconCurtain")));
		String xpath2=".//*[@id='CommentDialog']/div[3]/div/div[2]/span[1]";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath2)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath2)));
		WebElement ele3=DriverObject.driver.findElement(By.xpath(xpath2));
		ele3.click();
		}
		Thread.sleep(1000);
		WebElement CommentList=DriverObject.driver.findElement(By.xpath(".//*[@id='comment-list']/li[3]/div/div[2]"));
		WebElement DeleteComment=DriverObject.driver.findElement(By.xpath(".//*[@id='comment-list']/li[3]/div/div[2]/div[2]/div[3]"));
		Actions actions = new Actions(DriverObject.driver);
		actions.moveToElement(CommentList);
		DeleteComment.click();
		Thread.sleep(1000);
		WebElement DeleteCommentOk=DriverObject.driver.findElement(By.xpath(".//*[@id='comment-list']/div/div/div[1]"));
		actions.moveToElement(DeleteCommentOk);
		DeleteCommentOk.click();
		Thread.sleep(1000);
		String xpath3="html/body/div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable dialog-border']/div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ThemeDarkColor']/a[@class='ui-dialog-titlebar-close ui-corner-all HeaderMouseOverDarkColor']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath3)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath3)));
		WebElement ele3=DriverObject.driver.findElement(By.xpath(xpath3));
		ele3.click();
		Thread.sleep(3000);


		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-PCTCommentonProject");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-PCTCommentonProject");

		}
		
	}
	public void PCTShareProject() throws Exception
	{
		try
		{
		Log.info("Inside PCTSkill-PCTShareProject for ProjectID: "+ ProjectID);
		
		ProjectActions.PCTShareProject_View(ProjectID, Project_Name);
		ProjectActions.PCTShareProject_Edit(ProjectID, Project_Name);

		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-PCTShareProject");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-upgradePCTProjectForSkill");
		}
	}
	
	public void ExportPCTProject() throws Exception
	{
		try
		{
		Log.info("Inside PCTSkill-PCTShareProject for ProjectID: "+ ProjectID);
		
		filename=ProjectActions.ExportPCTProject(ProjectID, Project_Name);

		}catch(Exception e)
		{
			Log.error("Failed: PCTSkill-PCTShareProject");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-ExportPCTProject");
		}
	}
	public void ImportPCTProject() throws Exception
	{
		try
		{
		Log.info("Inside PCTSkill-PCTShareProject for ProjectID: "+ ProjectID);
		
		ProjectActions.ImportPCTProject(ProjectID, filename,Project_Name);

		}catch(Exception e)
		{
			System.out.println(e);
			Log.error("Failed: PCTSkill-PCTShareProject");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-ImportPCTProject");
		}
	}
	public String ImportPCTProjectDefault() throws Exception
	{
		String filepath,Mig_value;
		try
		{
		Log.info("Inside PCTSkill-PCTShareProject for ProjectID: "+ ProjectID);
		if (Version1.equalsIgnoreCase("Accounting") && Version.equalsIgnoreCase("office2016") )
		{ 	
			if(Project_Name.contains("Group"))
	    {
		Mig_value="true";
		
		filepath="PCT16\\PCT_Skill_Documents\\Accounting\\grp_check_package.pct";
	    }  
	     
		else
		{ 
			Mig_value="true";
		  filepath="PCT16\\PCT_Skill_Documents\\Accounting\\trialbalance_package.pct";
			
		}
		
		}
		else
			if (Version1.equalsIgnoreCase("Accounting") && Version.equalsIgnoreCase("office2013") )
			{ 	
				if(Project_Name.contains("Group"))
		    {
			Mig_value="false";
			filepath="PCT_Skill_Documents\\Accounting\\grp_check_package.pct";
		    }  
		    	
			else
			{ Mig_value="false";
			  filepath="PCT_Skill_Documents\\Accounting\\trialbalance_package.pct";
				
			}
		}
	
		else if (Version.equalsIgnoreCase("office2016"))
		{
			Mig_value="true";
			filepath="10686_rupsi_package.pct";
		}
		else
		{
			Mig_value="false";
			filepath="11288_check_package2.pct";
			System.out.println(filepath);
		}
	
		String Projid =ProjectActions.ImportPCTProjectDefault(filepath,Mig_value,Project_Name);
		if (Version1.equalsIgnoreCase("Accounting")){
		ProjectID=Projid;}
		return Projid;

		}catch(Exception e)
		{
			System.out.println(e);
			Log.error("Failed: PCTSkill-PCTShareProject");
			throw new Exception(Skillname+"("+ProjectID+")->Failed: PCTSkill-ImportPCTProject");
		}
	}
@AfterSuite
public void terminate() throws Exception
{
	SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
	/*if (PCTLogin.url_Ext=="http")
	{
	String Projid=ImportPCTProjectDefault();
	System.out.println(Projid+" \nProject is imported and ready for delete");
	ProjectActions.deletePCTProject(Projid);
//	if (PCTLogin.url_Ext=="http")
	//{
	String Projid1=ImportPCTProjectDefault();
	ProjectActions.publish(Projid1);
	System.out.println(Projid1+" \nProject is imported and publish");
	//}
	}*/
	DriverObject.driver.quit();
	}

}
