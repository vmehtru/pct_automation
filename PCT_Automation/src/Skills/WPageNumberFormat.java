package Skills;



import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class WPageNumberFormat extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Page Number Format "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Page Number Format";
	 super.FinalDoc="Page Number Format.docx";
	 super.StartingDoc="Page Number Format.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=4;
	 super.Setup("WPageNumberFormat");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HEADER_AND_FOOTER, SkillName.Word.Page_Number_Format);

		super.selectValueFromMultiSelect(Selector.Section,Selector1);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
