package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PAudioPlaybackOptions extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="PPT";
	 super.Project_Name="PPT - Audio Playback Options "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Palak";
	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Audio Playback Options";
	 super.FinalDoc="AudioPlaybackOptions.pptx";
	 super.StartingDoc="AudioPlaybackOptions.pptx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";	
	 super.excelRowCount=3;
	 super.excelColCount=5;

	 super.Setup("PAudioPlaybackOptions");

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.AUDIO, SkillName.PPT.Audio_Playback_Options);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectImageFromMultiSelectByIndex("Audio",Selector2);		
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
