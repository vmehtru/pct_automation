package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PNotesAndHandoutsHdrAndFtr extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Notes and Handouts - Headers and Footers";

		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=7;
	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases_v15(String Iteration_no,String FinalDocument,String StartDocument,String Selector1, String ValidateOn, String ValidateFor, String SummaryFile) throws Exception
	{
		super.Project_Name="PPT - Notes and Handouts - Headers and Footers "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created By Harshit";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("PNotesAndHandoutsHdrAndFtr");

		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Notes_and_Handouts_Headers_and_Footers);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);
		setMasterProjectAndSaveProjectID();

	}



	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}

