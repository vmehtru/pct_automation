package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PFontFormattingWordCharacterLevel extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Font Formatting Word/Character Level "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Aastha";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Font Formatting Character Level";
		super.FinalDoc="Font formatting wordcharacter level.pptx";
		super.StartingDoc="Font formatting wordcharacter level.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=8;
		super.Setup("PFontFormattingWordCharacterLevel");
     }
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String Selector5, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Font_Formatting_Word_Character_Level);
		
			super.selectValueFromDropDownForSelectorOfSameName(Selector.Slide,Selector1);
			super.selectValueFromDropDown(Selector.ApplyTo,Selector2);
		if(Selector2.equals("Shape Paragraphs"))
		{
			super.selectValueFromDropDownForSelectorOfSameName(Selector.Shape,Selector3);
			super.selectValueFromDropDown(Selector.Para,Selector4);
			super.selectValueFromMultiSelect(Selector.Select_the_Text, Selector5);
		}
		else if (Selector2.equals("Table Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector3);
			super.selectValueFromDropDown(Selector.Cell,Selector4);
			super.selectValueFromMultiSelect(Selector.Text,Selector5);
		}
		//super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
		
		
		

}
