package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WTableOfFigure extends PCTSkill
{

	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Table Of Figures "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created By Prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Table of Figures";
	 super.FinalDoc="testcase.docx";
	 super.StartingDoc="testcase.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=4;

	 super.Setup("WTableOfFigures");

	}


	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.REFERENCES, SkillName.Word.Table_Of_Figures);
		
		super.selectValueFromDropDown(Selector.Table,Selector1);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
