package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WTableProps extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Table Properties "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Ajay Pal";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Table Properties";
	 super.FinalDoc="Table Properties.docx";
	 super.StartingDoc="Table Properties.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=5;
	 super.excelColCount=6;

	 super.Setup("WTableProps");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.TABLE, SkillName.Word.Table_Properties);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Document Tables"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
		}
		else if (Selector1.equals("Shape Tables"))
		{
			super.selectValueFromDropDown(Selector.Shape,Selector2);
			super.selectValueFromDropDown(Selector.Table,Selector3);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();

		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
