package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PFormatShapeFillandLine extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Format Shape - Fill and Line "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Aastha";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Format Shape - Fill and Line";
		super.FinalDoc="Format Shape - Fill and Line.pptx";
		super.StartingDoc="Format Shape - Fill and Line.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=7;
		super.excelColCount=11;
		super.Setup("PFormatShapeFillandLine");

	}

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4,String Selector5,String Selector6, String imagesrc, String ValidateOn, String ValidateFor, String image) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DRAWING, SkillName.PPT.Format_Shape_Fill_and_Line);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Slides"))
		{
		super.selectValueFromDropDown(Selector.Slide,Selector2);
		super.selectValueFromDropDownForSelectorOfSameName(Selector.ApplyTo,Selector3);
		if(Selector3.equals("Shapes"))
		{
			    if(Selector4.contains("Picture"))
				 super.selectValueFromMultiSelectImageAndText(Selector.Shape,Selector4,imagesrc);
			    else
			    	super.selectValueFromMultiSelect(Selector.Shape,Selector4);
		}
		else
		{   super.selectValueFromDropDown(Selector.SmartArt,Selector4);
		    if(Selector5.contains("Picture"))
			 super.selectValueFromMultiSelectImageAndText(Selector.Shape,Selector5,imagesrc);
		    else
		    	super.selectValueFromMultiSelect(Selector.Shape,Selector5);
		}
		}
		else if(Selector1.equals("Slide Master"))
		{
			super.selectValueFromDropDown(Selector.Slide_Master,Selector2);
			super.selectValueFromDropDownForSelectorOfSameName(Selector.ApplyTo,Selector3);
			if(Selector3.equals("Shapes"))
			{
				    if(Selector4.contains("Picture"))
					 super.selectValueFromMultiSelectImageAndText(Selector.Shape,Selector4,imagesrc);
				    else
				    	super.selectValueFromMultiSelect(Selector.Shape,Selector4);
			}
			else
			{   super.selectValueFromDropDown(Selector.SmartArt,Selector4);
			    if(Selector5.contains("Picture"))
				 super.selectValueFromMultiSelectImageAndText(Selector.Shape,Selector5,imagesrc);
			    else
			    	super.selectValueFromMultiSelect(Selector.Shape,Selector5);
			}
		}
		else if(Selector1.equals("Slide Layout"))
		{
			super.selectValueFromDropDown(Selector.Slide_Master,Selector2);
			super.selectValueFromDropDown(Selector.Slide_Layout,Selector3);
			super.selectValueFromDropDownForSelectorOfSameName(Selector.ApplyTo,Selector4);
			if(Selector4.equals("Shapes"))
			{
				if(Selector5.contains("Picture"))
					 super.selectValueFromMultiSelectImageAndText(Selector.Shape,Selector5,imagesrc);
				    else
				    	super.selectValueFromMultiSelect(Selector.Shape,Selector5);
			}
			else
			{   super.selectValueFromDropDown(Selector.SmartArt,Selector5);

			    if(Selector6.contains("Picture"))
				 super.selectValueFromMultiSelectImageAndText(Selector.Shape,Selector6,imagesrc);
			    else
			    	super.selectValueFromMultiSelect(Selector.Shape,Selector6);
			}
		}

		super.selectAllProperties();
		saveSkill();
		 if(Selector6.contains("Picture"))
		       super.validateSkillSummary(instNo,ValidateOn,ValidateFor,image);
		 else
			   super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
