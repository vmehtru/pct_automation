package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EConditionalFormatting extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

		super.App="Excel";
		 super.Project_Name="Excel - Conditional Formatting"+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By Aastha";
		 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Conditional Formatting";
		 super.FinalDoc="ConditionalFormatting.xlsx";
		 super.StartingDoc="ConditionalFormatting.xlsx";
		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=5;
		 super.excelColCount=5;
		 super.Setup("EConditionalFormatting");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Conditional_Formatting);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		
		//De-selecting default selected value
		super.selectValueFromMultiSelectByIndex(Selector.Conditional_Formatting,"1");
		super.selectValueFromMultiSelect(Selector.Conditional_Formatting, Selector2);
		
		//De-selecting default selected value
		//super.selectValueFromMultiSelectByIndex(Selector.Rule,"1");
		//super.selectValueFromMultiSelect(Selector.Rule, Selector3);
		
		
		super.selectAllProperties_groupcheckbox();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}

