package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PChartElementsTextOptions extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Chart Elements Text Options "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Sahil Gupta";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Chart Elements Text Options";
		super.FinalDoc="Chart Elements Text Options.pptx";
		super.StartingDoc="Chart Elements Text Options.pptx";
		
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=5;
		super.excelColCount=6;
		super.Setup("PChartElementsTextOptions");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.CHART, SkillName.PPT.Chart_Elements_Text_Options);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.Chart,Selector2);
		super.selectValueFromDropDown(Selector.ChartElement,Selector3);

		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
