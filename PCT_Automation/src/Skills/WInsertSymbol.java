package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WInsertSymbol extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Insert Symbol "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" by DhruvD";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Insert Symbol";
	 super.FinalDoc="Insert Symbol.docx";
	 super.StartingDoc="Insert Symbol.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=8;
	 super.excelColCount=7;

	 super.Setup("WInsertSymbol");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Word.Insert_Symbol);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Document Paragraphs"))
		{
			super.selectValueFromMultiSelect(Selector.Para,Selector2);
			super.selectValueFromMultiSelect("Symbol",Selector3);
		}
		else if (Selector1.equals("Table"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
			super.selectValueFromDropDown(Selector.Cell,Selector3);
			super.selectValueFromMultiSelect("Symbol",Selector4);
		}
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
