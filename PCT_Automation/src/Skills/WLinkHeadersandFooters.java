package Skills;





import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Skills.PCTSkill.Selector;



public class WLinkHeadersandFooters extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Link Headers and Footers "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" By Rupsi Mehta ";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Link Headers and Footers";
	 super.FinalDoc="Link Headers and Footers.docx";
	 super.StartingDoc="Link Headers and Footers.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=5;
	 super.Setup("WLinkHeadersandFooters");

	}


	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HEADER_AND_FOOTER, SkillName.Word.Link_Headers_and_Footers);

		if (super.Version.equals("Office16"))
		{
		super.selectValueFromDropDown(Selector.CheckPCTFeatureon,Selector1);
		}
		else
		super.selectValueFromDropDown(Selector.CheckSkillOn,Selector1);
		if(Selector1.equals("Header"))
		{
			super.selectValueFromMultiSelect(Selector.Header,Selector2);
		}
	  else if(Selector1.equals("Footer"))
		{
			super.selectValueFromMultiSelect(Selector.Footer,Selector2);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
