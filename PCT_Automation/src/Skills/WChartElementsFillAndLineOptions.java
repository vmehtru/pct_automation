package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WChartElementsFillAndLineOptions extends PCTSkill
{

	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Chart Elements Fill and Line options"+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created By Prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Chart Elemets fill and line options";
	 super.FinalDoc="ChartElementsFillAndLineOptions.docx";
	 super.StartingDoc="ChartElementsFillAndLineOptions.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=5;

	 super.Setup("WChartElementsFillAndLineOptions");

	}


	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.CHART, SkillName.Word.Chart_Elements_Fill_and_Line_Options);

		super.selectValueFromDropDown(Selector.Chart,Selector1);
		super.selectValueFromDropDown(Selector.ChartElement,Selector2);

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();

		testStartingDocAndFeedback();
	}

       @Test(groups= {"saveProject"})
        public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
