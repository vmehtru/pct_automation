package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class EInsertHyperlink extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Insert Hyperlink "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Sahil";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Insert Hyperlink";
	 super.FinalDoc="Insert Hyperlink.xlsx";
	 super.StartingDoc="Insert Hyperlink.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=5;

	 super.Setup("WInsertHyperlink");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Excel.Insert_Hyperlink);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromMultiSelect(Selector.Hyperlink,Selector2);

		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
