package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class POrderObjects extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Order Objects "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By DhruvD";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Order Objects";
		super.FinalDoc="Order Objects.pptx";
		super.StartingDoc="Order Objects.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=6;
		super.excelColCount=5;
		super.Setup("POrderObjects");

	}

	@Test(dataProvider="dp")
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Order_Objects);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.Object,Selector2);
		super.selectAllFromMultiSelect(Selector.Object);
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	 }


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
