package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WFormatPicturePictureOptions extends PCTSkill
{

	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Format Picture -Picture Options "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created by prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Format picture-picture options";
	 super.FinalDoc="Format picture-picture options.docx";
	 super.StartingDoc="Format picture-picture options.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=4;

	 super.Setup("WFormatPicturePictureOptions");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1 , String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.PICTURE, SkillName.Word.Format_Picture_Picture_Options);

		super.selectImageFromMultiSelectByIndex(Selector.Picture , Selector1);
		super.selectAllProperties();

		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

    @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
