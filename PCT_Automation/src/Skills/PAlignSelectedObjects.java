package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PAlignSelectedObjects extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Align Selected Objects "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Aastha";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Align Selected Objects";
		super.FinalDoc="AlignSelectedObjects.pptx";
		super.StartingDoc="AlignSelectedObjects.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=4;
		super.Setup("PAlignSelectedObjects");
     }
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases123(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DRAWING, SkillName.PPT.Align_Selected_Objects);
        super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectAllFromMultiSelect(Selector.Object);
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}

}
