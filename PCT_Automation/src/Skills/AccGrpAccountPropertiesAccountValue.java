package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import PCTProjects.LaunchProject;



public class AccGrpAccountPropertiesAccountValue extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Accounting";
		super.Project_Name="GroupAcc Prop-Acc/Value "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Megha";
		super.SkillDocsPath="PCT_Skill_Documents\\Accounting\\Group Account Properties_AccountValue";
		super.FinalDoc="Group_final.xlsx";
		super.StartingDoc="Group_final.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=2;
		super.excelColCount=6;
		
		String ProjcectID=super.ImportPCTProjectDefault();
		LaunchProject.Launch_PCTProjectID(ProjcectID);
		super.blankDocToCheckFeedbackText= strStartPath +"PCT_Skill_Documents\\Accounting\\ForSummaryReport.xlsx";	
		

	}

	@Test(dataProvider="dp")
	public void TestCases123(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		
		super.addSkill(instNo, SkillCategory.GROUPFEATURES, SkillName.Accounting.Group_Account_Properties_Account_Value);
		super.selectValueFromDropDown(Selector.Group,Selector1);
		super.addTextToNamedRangeTextBox(Selector.Title ,Selector2);
		super.addTextToNamedRangeTextBox(Selector.Value ,Selector3);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
