package Skills;




import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PInsertMedia extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Insert Media "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Insert Media";
		super.FinalDoc="Insert Media.pptx";
		super.StartingDoc="Insert Media.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=6;
		super.excelColCount=5;
		super.Setup("PInsertMedia");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Insert_Media);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectVideoFromMultiSelect(Selector.Media,Selector2);
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
