package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Utils.MouseKeyboardActions;
public class WAddModifyParagraphText extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Add/Modify Paragraph Text "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Add Modify Paragraph Text";
	 super.FinalDoc="Add Modify Paragraph Text.docx";
	 super.StartingDoc="Add Modify Paragraph Text.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=6;
	 super.Setup("WAddModifyParagraphText");
	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Add_Modify_Paragraph_Text);

		super.selectValueFromRadio(Selector.Validation, Selector1);

		if(Selector1.equals("Whole Paragraph"))
		{
			super.selectValueFromMultiSelect(Selector.Para, Selector3);
		}
		
		else if(Selector1.equals("Partial Text"))
		{
			MouseKeyboardActions.sendKeys("PartialTextBox",Selector2);
			super.selectValueFromMultiSelect(Selector.Para, Selector3);
		}

		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}


        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
