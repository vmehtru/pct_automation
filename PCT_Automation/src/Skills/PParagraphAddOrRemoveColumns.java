package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PParagraphAddOrRemoveColumns extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="PPT";
	 super.Project_Name="PPT - Merge cells "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Palak";
	 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Paragraph - Add or Remove Columns";
	 super.FinalDoc="testcase.pptx";
	 super.StartingDoc="testcase.pptx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";	
	 super.excelRowCount=4;
	 super.excelColCount=5;

	 super.Setup("PParagraphAddOrRemoveColumns");
	}
	

	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Paragraph_Add_or_Remove_Columns);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromMultiSelect(Selector.Shape,Selector2);
		super.selectAllProperties();
        super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}
	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
