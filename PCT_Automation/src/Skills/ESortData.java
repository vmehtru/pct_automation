package Skills;



import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class ESortData extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Sort Data "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Sort Data";
	 super.FinalDoc="Sort Data.xlsx";
	 super.StartingDoc="Sort Data.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=3;
	 super.excelColCount=6;

	 super.Setup("ESortData");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Sort_Data);

		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromDropDown(Selector.SortRange,Selector2);
		super.selectValueFromMultiSelect(Selector.SortingLevel,Selector3);

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
