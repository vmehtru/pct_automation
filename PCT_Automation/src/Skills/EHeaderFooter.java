package Skills;





import java.util.Calendar;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class EHeaderFooter extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Excel";
	 super.Project_Name="Excel - Headers and Footers "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="DhruvD";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Headers and Footers";
	 super.FinalDoc="Headers and Footers.xlsx";
	 super.StartingDoc="Headers and Footers.xlsx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=4;

	 super.Setup("EHeaderFooter");

	}



	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String Selector1,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Excel.Headers_and_Footers);

		super.selectValueFromMultiSelect(Selector.Sheet,Selector1);
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
