package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import PCTProjects.LaunchProject;




public class AccTrialBalanceTotals extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Accounting";
		super.Project_Name="Accounting -Trial Balance - Totals  "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\Accounting\\Trial Balance - Totals";
		super.FinalDoc="hha10_e2-22-orig - Solution.xlsx";
		super.StartingDoc="hha10_e2-22-orig - Solution.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=1;
		super.excelColCount=4;
		//super.Setup("AccTrialBalanceAccountProperties");
		String ProjcectID=super.ImportPCTProjectDefault();
		LaunchProject.Launch_PCTProjectID(ProjcectID);
		super.blankDocToCheckFeedbackText= strStartPath +"PCT_Skill_Documents\\Accounting\\ForSummaryReport.xlsx";	
		

	}

	@Test(dataProvider="dp")
	public void TestCases123(String Iteration_no,String Selector1, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		
		super.addSkill(instNo, SkillCategory.TRIALBALANACE, SkillName.Accounting.Trial_Balance_Totals);
		super.selectValueFromDropDown(Selector.Trial_Balance_with_title,Selector1);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
