package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PCharacterSpacing extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Character Spacing "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Harshit";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Character Spacing";
		super.FinalDoc="Character_Spacing.pptx";
		super.StartingDoc="Character_Spacing.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=7;
		super.Setup("PCharacterSpacing");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Character_Spacing);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector2);
		if(Selector2.equals("Shape Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Shape,Selector3);
			super.selectValueFromMultiSelect(Selector.Para,Selector4);
		}
		else if (Selector2.equals("Table Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector3);
			super.selectValueFromMultiSelect(Selector.Para,Selector4);
		}
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
