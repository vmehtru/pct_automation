package Skills;



import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WContentControlProperties extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Word";
	 super.Project_Name="Word - Content Control Properties "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Content Control Properties";
	 super.FinalDoc="Content Control Properties.docx";
	 super.StartingDoc="Content Control Properties.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=6;
	 super.excelColCount=6;

	 super.Setup("WContentControlProperties");

	}


	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DEVELOPER, SkillName.Word.Content_Control_Properties);

		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);

		if(Selector1.equals("Document Paragraphs"))
		{
			//Deselecting the default selected control
			//super.selectValueFromMultiSelectByIndex(Selector.Control, "1");

			super.selectValueFromMultiSelect(Selector.Control, Selector2);
		}
		else if(Selector1.equals("Table Paragraphs"))
		{
			super.selectValueFromDropDown(Selector.Table,Selector2);
			super.selectValueFromMultiSelect(Selector.Control, Selector3);
		}

		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
