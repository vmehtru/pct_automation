package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class AccGenericTotals extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Accounting";
		super.Project_Name="Generic - Totals"+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\Accounting\\Generic_Totals";
		super.FinalDoc="hha10_e2-22-orig - Solution.xlsx";
		super.StartingDoc="hha10_e2-22-orig - Solution.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=1;
		super.excelColCount=6;
		super.Setup("AccGenericTotals");

	}

	@Test(dataProvider="dp")
	public void TestCases123(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.GENERICSKILLS, SkillName.Accounting.Generic_Totals);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.addTextToNamedRangeTextBox(Selector.Total_1, Selector2);
		super.addTextToNamedRangeTextBox(Selector.Total_2, Selector3);
		
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
