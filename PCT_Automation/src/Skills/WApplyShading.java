package Skills;

import java.util.Calendar;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WApplyShading extends PCTSkill{
	
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="Word";
	 super.Project_Name="Word - Apply Shading "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Palak Bansal";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Apply Shading";
	 super.FinalDoc="ApplyShading.docx";
	 super.StartingDoc="ApplyShading.docx";
	 
	 super.excelDataSourcePath="Data Source.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=6;
	
	 super.Setup("WApplyShading");

	}	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		
		String instNo=super.addInstructiontoPCTProject();
		
		
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Word.Apply_Shading);
		
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		
		if(Selector1.equals("Table"))
		 {  super.selectValueFromDropDown(Selector.Table,Selector2);

			}
	else if (Selector1.equals("Cell"))
		{   super.selectValueFromDropDown(Selector.Table,Selector2);
			super.selectValueFromMultiSelect("Cell",Selector3);
		}

	else if (Selector1.equals("Paragraph"))
	{
		super.selectValueFromMultiSelect(Selector.Para,Selector2);
		
	}
		
		super.selectAllProperties();
		//System.out.println(Iteration_no);
		saveSkill();
		
		
		
		
	} 
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
		
}


