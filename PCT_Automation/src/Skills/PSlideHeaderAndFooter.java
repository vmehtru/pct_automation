package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PSlideHeaderAndFooter extends PCTSkill
{

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Slide - Header and Footer"+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created by Prateek";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Slide Header and Footer";
		super.FinalDoc="Slide-header and Footer.pptx";
		super.StartingDoc="Slide-header and Footer.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=6;
		super.Setup("PSlideHeaderAndFooter");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String Selector3,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Slide_Headers_and_Footers);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Slides"))
			super.selectValueFromDropDown(Selector.Slide,Selector2);
		if(Selector1.equals("Slide Master"))
			super.selectValueFromDropDown(Selector.Slide_Master,Selector2);	
		if(Selector1.equals("Slide Layout"))
		{
			super.selectValueFromDropDown(Selector.Slide_Master,Selector2);
			super.selectValueFromDropDown(Selector.Slide_Layout,Selector3);
		}
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
