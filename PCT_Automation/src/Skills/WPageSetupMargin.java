package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WPageSetupMargin extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\PageSetup-Magin";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=7;

	}

	@Test(dataProvider="dp")
	public void TestSkill(String Iteration_no,String FinalDocument,String StartDocument,String Selector1, String ValidateOn, String ValidateFor,String SummaryFile) throws Exception
	{
		super.Project_Name="Page Setup - Margin "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Created By Prateek";
		super.FinalDoc=FinalDocument;
		super.StartingDoc=StartDocument;
		super.Setup("WPageSetupMargin");
		
		String instNo=super.addInstructiontoPCTProject();
		
		super.addSkill(instNo, SkillCategory.PAGE_LAYOUT, SkillName.Word.Page_Setup_Margin);
		super.selectValueFromMultiSelect(Selector.Section,Selector1);
		
		super.selectAllProperties_groupcheckbox();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(SummaryFile);

		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
