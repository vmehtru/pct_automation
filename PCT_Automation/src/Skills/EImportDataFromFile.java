package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EImportDataFromFile extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.Project_Name="Excel - Import Data From File  "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Aastha";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Import Data from File";
		super.FinalDoc="Import Data.xlsx";
		super.StartingDoc="Import Data.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=5;
		super.excelColCount=5;
		super.Setup("EImportDataFromFile");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases123(String Iteration_no,String Selector1,String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.DATA, SkillName.Excel.Import_Data_From_File);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromMultiSelect(Selector.Data,Selector2);

		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
}
