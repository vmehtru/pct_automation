package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class AccGenericAccountPropertiesMultipleAccountValue extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Accounting";
		super.Project_Name="Accounting -Generic Account Properties - Account/Multiple Values"+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Megha";
		super.SkillDocsPath="PCT_Skill_Documents\\Accounting\\Generic Account Properties_ MultipleAccountValue";
		super.FinalDoc="bma04_solution.xlsx";
		super.StartingDoc="bma04_solution.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=7;
		super.Setup("AccGenericAccountPropertiesMultipleAccountValue");

	}

	@Test(dataProvider="dp")
	public void TestCases123(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.GENERICSKILLS, SkillName.Accounting.Generic_AccountProperties_MultipleAccountValue);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.addTextToNamedRangeTextBox(Selector.Title, Selector2);
		super.addTextToNamedRangeTextBox(Selector.Value, Selector3);
		super.addTextToNamedRangeTextBox(Selector.Columntitle, Selector4);
		
		super.selectAllProperties_groupcheckbox();
		
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
