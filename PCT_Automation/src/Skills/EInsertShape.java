package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EInsertShape extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.Project_Name="Excel - Insert Shape "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Insert Shape";
		super.FinalDoc="Insert Shape.xlsx";
		super.StartingDoc="Insert Shape.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=4;
		super.excelColCount=7;
		super.Setup("EInsertShape");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String Selector3, String Selector4,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.Excel.Insert_Shape);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector2);
		if(Selector2.equals("Chart Shapes"))
		{
			super.selectValueFromDropDown(Selector.Chart,Selector3);
			super.selectValueFromMultiSelect(Selector.Shape,Selector4);
		}
		else
		if(Selector2.equals("Worksheet Shapes"))
		{
		super.selectValueFromMultiSelect(Selector.Shape,Selector3);
		}
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	@Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
