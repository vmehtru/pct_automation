package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PInsertPicture extends PCTSkill {

	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Insert Picture "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Insert Picture";
		super.FinalDoc="Insert Picture.pptx";
		super.StartingDoc="Insert Picture.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=3;
		super.excelColCount=7;
		super.Setup("PInsertPicture");

	}

	@Test(dataProvider="dp")
	public void TestCases(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.INSERT, SkillName.PPT.Insert_Picture);
		super.selectValueFromDropDown(Selector.ApplyTo,Selector1);
		if(Selector1.equals("Slides"))
		{
	
		super.selectValueFromDropDown(Selector.Slide,Selector2);

		super.selectImageFromMultiSelectByName(Selector.Picture,Selector3);
		}
		if(Selector1.equals("Slide Master"))
		{
	
		super.selectValueFromDropDown(Selector.Slide_Master,Selector2);
		super.selectImageFromMultiSelectByName(Selector.Picture,Selector3);
		}
		if(Selector1.equals("Slide Layout"))
		{
		super.selectValueFromDropDown(Selector.Slide_Master,Selector2);
		super.selectValueFromDropDown(Selector.Slide_Layout,Selector3);
		super.selectImageFromMultiSelectByName(Selector.Picture,Selector4);
		}
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
