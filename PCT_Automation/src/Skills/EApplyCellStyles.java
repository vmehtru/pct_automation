package Skills;


import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class EApplyCellStyles extends PCTSkill {
	
	
	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Excel";
	 super.Project_Name="Excel - Apply Cell Styles "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Rupsi";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Apply Cell Styles";
	 super.FinalDoc="Apply Cell Styles.xlsx";
	 super.StartingDoc="Apply Cell Styles.xlsx";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=20;
	 super.excelColCount=5;
	 super.Setup("EApplyCellStyles");

	}

	
	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Apply_Cell_Styles);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.addTextToNamedRangeTextBox(Selector.Cell, Selector2);
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
	
	

}
