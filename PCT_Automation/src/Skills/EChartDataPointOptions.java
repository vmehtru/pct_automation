package Skills;


import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EChartDataPointOptions extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="Excel";
		super.Project_Name="Excel - Chart Data Point Options  "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By DhruvD";
		super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Chart Data Point Options";
		super.FinalDoc="TestCase.xlsx";
		super.StartingDoc="TestCase.xlsx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=6;
		super.excelColCount=6;
		super.Setup("EChartDataPointOptions");

	}

	@Test(dataProvider="dp")
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2,String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.CHART, SkillName.Excel.Chart_Data_Point_Options);
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.selectValueFromDropDown(Selector.Chart,Selector2);
		super.selectValueFromDropDown(Selector.Data_Point,Selector3);
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
