package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EFormatPainter extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="Excel";
	 super.Project_Name="Excel - Format Painter"+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="By Versha Garg";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Format Painter";
	 super.FinalDoc="FormatPainter.xlsx";
	 super.StartingDoc="FormatPainter.xlsx";
	 super.excelDataSourcePath="Data Source.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=7;
	 super.Setup("EFormatPainter");
    }	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void EFormatPainter(String Iteration_no,String Selector1,String Selector2,String Selector3,String Selector4, String ValidateOn, String ValidateFor) throws Exception
	{
		
		String instNo=super.addInstructiontoPCTProject();
		
		
		super.addSkill(instNo, SkillCategory.HOME, SkillName.Excel.Format_Painter);
		
		super.selectValueFromDropDown(Selector.Sheet,Selector1);
		super.addTextToNamedRangeTextBox(Selector.Source_cell,Selector2);
		super.selectValueFromDropDown(Selector.Destination_worksheet,Selector3);
		super.addTextToNamedRangeTextBox(Selector.Destination_cell,Selector4);
		
		super.selectAllProperties();
		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	@Test(groups= {"testSubmission"})
	public void ProjectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
		
}
