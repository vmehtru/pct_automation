package Skills;





import java.util.Calendar;

import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;


public class WTableColumnProps extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

	 super.App="Word";
	 super.Project_Name="Word - Table Column Properties "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc=" By Sahil Gupta";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Table Column Properties";
	 super.FinalDoc="Table Column Properties.docx";
	 super.StartingDoc="Table Column Properties.docx";

	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=5;

	 super.Setup("WTableColumnProperties");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1,String Selector2,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.TABLE, SkillName.Word.Table_Column_Properties);

		super.selectValueFromDropDown(Selector.Table,Selector1);
		super.selectValueFromMultiSelect(Selector.Column,Selector2);

		super.selectAllProperties();
		saveSkill();

		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}

}
