package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PAddGuide extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Add Guide "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Sahil Gupta";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Add Guide";
		super.FinalDoc="Add Guide.pptx";
		super.StartingDoc="Add Guide.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=2;
		super.excelColCount=4;
		super.Setup("PAddGuide");

	}

	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestCases_v15(String Iteration_no,String Selector1,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.HOME, SkillName.PPT.Add_Guide);
		super.selectValueFromMultiSelect(Selector.Guide,Selector1);
		
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
