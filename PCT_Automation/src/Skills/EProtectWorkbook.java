package Skills;

import java.util.Calendar;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EProtectWorkbook extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{
	 super.App="Excel";
	 super.SkillDocsPath="PCT_Skill_Documents\\Excel\\Protect Workbook";
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=2;
	 super.excelColCount=6;

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String FinalDoc, String StartingDoc, String FeedbackTextFile, String ValidateOn, String ValidateFor) throws Exception
	{
		super.Project_Name="Excel - Protect Workbook "+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Sahil Gupta";
		super.FinalDoc=FinalDoc;
		super.StartingDoc=StartingDoc;
		super.Setup("EProtectWorkbook");

		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.FILE, SkillName.Excel.Protect_Workbook);

		super.selectAllProperties();

		super.saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);

		testPerfectDocAndScore();
		testStartingDocAndFeedback(FeedbackTextFile);
		setMasterProjectAndSaveProjectID();

	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}



}
