package Skills;

import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WFormatShapeFillAndLine extends PCTSkill
{
	@BeforeClass
	public void Setup() throws Exception
	{
		
	 super.App="Word";
	 super.Project_Name="Word - Format shape fill and Line "+Calendar.getInstance().getTime().toString();
	 super.Project_Desc="Created by Prateek";
	 super.SkillDocsPath="PCT_Skill_Documents\\Word\\Format shape Fill and Line";
	 super.FinalDoc="Format shape Fill and Line.docx";
	 super.StartingDoc="Format shape Fill and Line.docx";
	 
	 super.excelDataSourcePath="DataSource.xlsx";
	 super.excelSheetname="Sheet1";
	 super.excelRowCount=4;
	 super.excelColCount=4;
	
	 super.Setup("WFormatShapeFillAndLine");

	}	
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1,String ValidateOn, String ValidateFor) throws Exception
	{
		
		String instNo=super.addInstructiontoPCTProject();
		
		
		super.addSkill(instNo, SkillCategory.DRAWING, SkillName.Word.Format_Shape_Fill_and_Line_Options);
		super.selectValueFromMultiSelect(Selector.Shapes_starting_with_text,Selector1 );
		super.selectAllProperties();
		saveSkill();
		
	} 
	@Test(groups= {"testSubmission"})
	public void testScore2AndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
	  @Test(groups= {"saveProject"})
		public void saveProject() throws Exception
		{
			setMasterProjectAndSaveProjectID();
		}
		

		@Test(groups= {"upgrade"})
		public void testUpgrade() throws Exception
		{
			getSavedProjectIDsForSkillandUpgrade();
		}
}
