package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

public class PPictureBorder extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="PPT";
		 super.Project_Name="Picture Border "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="By DhruvD";
		 super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Picture Border";
		 super.FinalDoc="Picture Border.pptx";
		 super.StartingDoc="Picture Border.pptx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=5;
		 super.excelColCount=5;

		 super.Setup("PPictureBorder");

	}



	@Test(dataProvider="dp", groups= {"testCase","Mar15"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		addSkill(instNo, SkillCategory.PICTURE, SkillName.PPT.Picture_Border);
		selectValueFromDropDown(PCTSkill.Selector.Slide, Selector1);
		selectImageFromMultiSelectByIndex(PCTSkill.Selector.Picture, Selector2);
		selectAllProperties();
		saveSkill();
		validateSkillSummary(instNo, ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void projectScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}


	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
