package Skills;




import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class WSmartArtContent extends PCTSkill {


	@BeforeClass
	public void Setup() throws Exception
	{

		 super.App="Word";
		 super.Project_Name="Word - SmartArt Content "+Calendar.getInstance().getTime().toString();
		 super.Project_Desc="Rupsi";
		 super.SkillDocsPath="PCT_Skill_Documents\\Word\\SmartArt Content";
		 super.FinalDoc="SmartArt Content.docx";
		 super.StartingDoc="SmartArt Content.docx";

		 super.excelDataSourcePath="DataSource.xlsx";
		 super.excelSheetname="Sheet1";
		 super.excelRowCount=8;
		 super.excelColCount=6;

		 super.Setup("WSmartArtContent");

	}



	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestSkill(String Iteration_no,String Selector1, String Selector2,String Selector3,String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.SMARTART, SkillName.Word.SmartArt_Content);
		super.selectValueFromMultiSelect(Selector.SmartArt_starting_with_text, Selector1);
		super.selectValueFromMultiSelect(Selector.Shapes_starting_with_text, Selector2);
		super.selectValueFromMultiSelect(Selector.Para, Selector3);
		
		saveSkill();
		validateSkillSummary(instNo,ValidateOn,ValidateFor);
	}

	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}

        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}


}
