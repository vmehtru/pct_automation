package Skills;



import java.util.Calendar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class PChartDataPointOptions extends PCTSkill {
	@BeforeClass
	public void Setup() throws Exception
	{
		super.App="PPT";
		super.Project_Name="PPT - Chart Data Point Options"+Calendar.getInstance().getTime().toString();
		super.Project_Desc="By Rupsi";
		super.SkillDocsPath="PCT_Skill_Documents\\PPT\\Chart Data Point Options";
		super.FinalDoc="Chart Data Point Options.pptx";
		super.StartingDoc="Chart Data Point Options.pptx";
		super.excelDataSourcePath="DataSource.xlsx";
		super.excelSheetname="Sheet1";
		super.excelRowCount=6;
		super.excelColCount=6;
		super.Setup("PChartDataPointOptions");
		
	}
		
	@Test(dataProvider="dp", groups= {"testCase"})
	public void TestCases_v15(String Iteration_no,String Selector1,String Selector2 , String Selector3, String ValidateOn, String ValidateFor) throws Exception
	{
		String instNo=super.addInstructiontoPCTProject();
		super.addSkill(instNo, SkillCategory.CHART, SkillName.PPT.Chart_Data_Point_Options);
		super.selectValueFromDropDown(Selector.Slide,Selector1);
		super.selectValueFromDropDown(Selector.Chart,Selector2);
		super.selectValueFromDropDown(Selector.Data_Point,Selector3); 
		super.selectAllProperties();
		saveSkill();
		super.validateSkillSummary(instNo,ValidateOn,ValidateFor);
	} 
	
	@Test(groups= {"testSubmission"})
	public void testScoreAndFeedback() throws Exception
	{
		testPerfectDocAndScore();
		testStartingDocAndFeedback();
	}
        
        @Test(groups= {"saveProject"})
	public void saveProject() throws Exception
	{
		setMasterProjectAndSaveProjectID();
	}
	

	@Test(groups= {"upgrade"})
	public void testUpgrade() throws Exception
	{
		getSavedProjectIDsForSkillandUpgrade();
	}
	
	
}
