package Common;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.testng.annotations.Test;

import PCTProjects.ProjectActions;

public class generateFeedbackTextFile {
	
	String SubID="54900";
	//String FeedbackFilePath="PCT_Skill_Documents\\Accounting\\Generic Account Properties_ AccountValue";
	
	String FeedbackFilePath="PCT_Skill_Documents\\PPT\\Add Chart Elements";
	
	@Test
	public void getfile() throws Exception
	{
		 int callSummaryReportAgain=1;
			int count=0;
			PrintWriter out1 = new PrintWriter(new BufferedWriter(new FileWriter(FeedbackFilePath+"\\Summary.htm", true)));
			
			do
			{
					callSummaryReportAgain=0;
					count++;
					
						
				String  url =""+PCTLogin.url_Ext+"://"+PCTLogin.machine_name+"/reportingmodule/SummaryReport.aspx?SubmissionID="+SubID;
				URL obj = new URL(url);
				HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			
				 BufferedReader in = new BufferedReader(
					        new InputStreamReader(conn.getInputStream()));
				 
				 StringBuffer response = new StringBuffer();
				 String inputLine;
				 				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);}
				in.close();
				
				if(response.toString().contains("UNAUTHORIZED REQUEST"))
				{
					out1 = new PrintWriter(new BufferedWriter(new FileWriter(FeedbackFilePath+"\\Summary.htm", true)));
					out1.println("TestSubmission: UNAUTHORIZED REQUEST");
					out1.close();
					throw new Exception("TestSubmission: UNAUTHORIZED REQUEST");
				}
				else if(response.toString().contains("grading of your submission is failed"))
				 {
					out1 = new PrintWriter(new BufferedWriter(new FileWriter(FeedbackFilePath+"\\Summary.htm", true)));
					out1.println("TestSubmission: Grading of your submission is failed");
					out1.close();
					throw new Exception("TestSubmission: Grading of your submission is failed");
					
				 }
				else if(response.toString().contains("THE SUBMISSION DETAILS COULD NOT BE RETRIEVED"))
				{
					out1 = new PrintWriter(new BufferedWriter(new FileWriter(FeedbackFilePath+"\\Summary.htm", true)));
					out1.println("TestSubmission: THE SUBMISSION DETAILS COULD NOT BE RETRIEVED");
					out1.close();
					throw new Exception("TestSubmission: THE SUBMISSION DETAILS COULD NOT BE RETRIEVED");
					
				 }
				else if(response.toString().contains("not yet published, please try again shortly."))
				{
					
				 callSummaryReportAgain=1;
				}
				else {
					out1 = new PrintWriter(new BufferedWriter(new FileWriter(FeedbackFilePath+"\\Summary.htm", true)));
					out1.println(response.toString().replace("v.style.display = \"none\"","v.style.display = \"none\";"));
					out1.close();
				}
				Thread.sleep(1000);
					
			}while(callSummaryReportAgain==1 && count<100);
			
			if(count>=100)
			{
				out1 = new PrintWriter(new BufferedWriter(new FileWriter(FeedbackFilePath+"\\Summary.htm", true)));
				out1.println("TestSubmission: Result not yet published.");
				out1.close();
				throw new Exception("TestSubmission: Result not yet published.");
			}
	  
	}
	

}
