package Common;
import Skills.PCTSkill;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class Log {
	public static final Logger appLog = Logger.getLogger("debugLogger");
	public static final Logger errorLog = Logger.getLogger("reportsLogger");

	
	static
	{
		 PropertyConfigurator.configure("log4j.properties");
		 appLog.setLevel(Level.INFO);
		 errorLog.setLevel(Level.ERROR);
		 appLog.error("applog error");
			errorLog.error(" error log error");
			appLog.info("applog info");
	}
	public static void error(String message){
		
		appLog.error("For Skill:"+PCTSkill.Skillname+"("+PCTSkill.ProjectID+")"+message);
		errorLog.error("For Skill:"+PCTSkill.Skillname+"("+PCTSkill.ProjectID+")"+message);
		
	}
	
	public static void info(String message){
		
		appLog.info("For Skill:"+PCTSkill.Skillname+"("+PCTSkill.ProjectID+")"+message);
	}
}

