package Common;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.MouseKeyboardActions;
import  Common.Log;


public class PCTLogin {

    //public static String machine_name="grader18";
	//public static String machine_name="grader3/mil";
	//public static String machine_name="graderprod.pearsoncmg.com";
	  //public static String machine_name="pct.gradeit.pearsoncmg.com";
   //public static String machine_name="graderstg.pearsoncmg.com/xl";
	//public static String machine_name="grader18";
	public static String machine_name="grader18/peg";
	public static String userName="ws4";
	public static String pswd="ws4@123";
    private String  browser;
    DesiredCapabilities dc;
    public static String url_Ext="http";
	public static void loginToPCT(String Version_ID, String Browser) throws Exception
	{ 
		SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		//DriverObject.driver= new FirefoxDriver();
		
		
		/* System.setProperty("webdriver.chrome.driver", 
			     "chromedriver_win32\\chromedriver.exe");
			//driver=new ChromeDriver();
		DriverObject.driver=new ChromeDriver();*/
		if (Browser.equalsIgnoreCase("Chrome"))
		instantiateChrome();
		if (Browser.equalsIgnoreCase("Firefox"))
		instantiateMozilla();
		Log.info("Launched Driver");
		//DriverObject.driver.get(""+url_Ext+"s://"+machine_name+"/pctui/pctlogin.aspx");
		DriverObject.driver.get(""+url_Ext+"://"+machine_name+"/PCTUI/PCTLogin.aspx");
		Log.info("Launched PCT Home Page");
		
		DriverObject.driver.manage().window().maximize();
		Log.info("Browser window maximized");
		
		MouseKeyboardActions.sendKeys("Login_Name", userName);
		Log.info("Login user name entered");
		
		MouseKeyboardActions.sendKeys("Login_Password", pswd);
		Log.info("Login Password entered");
		
		
		//Selecting 2016 and 2013
		WebDriverWait wait= new WebDriverWait(DriverObject.driver,60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(Version_ID)));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Version_ID)));
		wait.until(ExpectedConditions.elementToBeClickable(By.id(Version_ID)));
		WebElement ele=DriverObject.driver.findElement(By.id(Version_ID));
		ele.click();
		
		MouseKeyboardActions.click("Login_Button");
		Log.info("Login button clicked");
		
	}
	
	 public static void instantiateChrome ()
	    {
		 SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
		 System.setProperty("webdriver.chrome.driver", 
			     "chromedriver_win32\\chromedriver.exe");
		DriverObject.driver=new ChromeDriver();
	    }

		// Instantiate Mozilla Browser
	    public static void instantiateMozilla()
	    {
	    	SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
			DriverObject.driver = new FirefoxDriver();
	    }

		// Instantiate IE Browser - using driver version 2.45
	    public void instantiateIE()
	    { SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
			System.setProperty("webdriver.ie.driver","src/lib/IEDriverServer_2.45.exe");
			dc = DesiredCapabilities.internetExplorer();
			DriverObject.driver = new InternetExplorerDriver();
	    }
	// Instantiate Chrome Browser
   

}
