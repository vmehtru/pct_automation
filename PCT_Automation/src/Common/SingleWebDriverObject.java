package Common;

import org.openqa.selenium.WebDriver;

public class SingleWebDriverObject {

	   //create an object of SingleObject
	   private static SingleWebDriverObject instance = new SingleWebDriverObject();

	   //make the constructor private so that this class cannot be
	   //instantiated
	   private SingleWebDriverObject(){}

	   //Get the only object available
	   public static SingleWebDriverObject getInstance(){
	      return instance;
	   }

	   public WebDriver driver;
	   
	}
