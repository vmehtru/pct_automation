package Common;

import  Common.Log;

 
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;

public class ElementRepository {
	
	static List<String> Ele_name= new ArrayList<String>();
	static List<String> Ele_xpath= new ArrayList<String>();
	
	@BeforeClass
	public static void createElementRepository()
	{
		
	
		try
		{
		int i=0;//as the first row is heading Row


		int csvRowCount=0; 
		
		 //Get scanner instance
	   Scanner scanner = new Scanner(new File("Element_Repository.csv"));
	   scanner.useDelimiter("\r\n");
	   
	   while (scanner.hasNext()) 
	   {
	   	csvRowCount++;
	   	scanner.next();
	   }

		 //Get scanner instance
	     scanner = new Scanner(new File("Element_Repository.csv"));
	  
	    //Set the delimiter used in file
	    scanner.useDelimiter(",|\r\n");
	  
	    String s;
	    while (i<csvRowCount) 
	    { 
	    	s=scanner.next().toString();
	    	Ele_name.add(s);
	    	s=scanner.next().toString();
	    	Ele_xpath.add(s);
	    	
	        i++;
	    }
	   

	    //Do not forget to close the scanner  
	    scanner.close();
		}catch(Exception e){
			Log.error("Element Repository creation failed");
			
		}
		Log.info("Element Repository created");
	}

public static By findLocator(String ele_name)
{
	int index=Ele_name.indexOf(ele_name);
	Log.info("Returning locator for "+ele_name);
    return (By.xpath(Ele_xpath.get(index).toString()));
    
}

public static WebElement findElement(WebDriver driver, String ele_name)
{
	int index=Ele_name.indexOf(ele_name);
	Log.info("Returning Element for "+ele_name);
    return driver.findElement(By.xpath(Ele_xpath.get(index).toString()));
}

public static List<WebElement> findAllElements(WebDriver driver, String ele_name)
{
	int index=Ele_name.indexOf(ele_name);
	Log.info("Returning All elements for "+ele_name);
	return driver.findElements(By.xpath(Ele_xpath.get(index).toString()));
}

}
