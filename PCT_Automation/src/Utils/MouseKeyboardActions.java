package Utils;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Common.ElementRepository;
import Common.SingleWebDriverObject;
import  Common.Log;

public class MouseKeyboardActions {

static WebDriverWait wait;
public static void click(String ele_name) throws Exception
{
	
	try
	{
		
	
	SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
	wait = new WebDriverWait(DriverObject.driver,60 );
	wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator(ele_name)));
	wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator(ele_name)));
	wait.until(ExpectedConditions.elementToBeClickable(ElementRepository.findLocator(ele_name)));
	ElementRepository.findElement(DriverObject.driver,ele_name).click();
	Log.info(ele_name+ "clicked");
	
	}catch(Exception e)
	{
		Log.error("Click on element: "+ele_name+ "failed");
		throw new Exception(e);
	}
	
}

public static void mouseMove(String ele_name) throws Exception
{
	try
	{
	SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
	wait = new WebDriverWait(DriverObject.driver,60 );
	wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator(ele_name)));
	wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator(ele_name)));
	
	Actions actions= new Actions(DriverObject.driver);
	actions.moveToElement(ElementRepository.findElement(DriverObject.driver,ele_name));
	actions.perform();
	Log.info(ele_name+ "- mouse hover");
	}catch(Exception e)
	{
		Log.error("Mouse hover on element: "+ele_name+ "failed");
		throw new Exception(e);
	}
}

public static void dragAndDrop(String ele_name_drag,String ele_name_DropTo) throws Exception
{
	try
	{
	SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
	wait = new WebDriverWait(DriverObject.driver,60 );
	wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator(ele_name_drag)));
	wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator(ele_name_drag)));
	wait.until(ExpectedConditions.elementToBeClickable(ElementRepository.findLocator(ele_name_drag)));
	wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator(ele_name_DropTo)));
	wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator(ele_name_DropTo)));

	Actions actions = new Actions(DriverObject.driver);
	actions.dragAndDrop(ElementRepository.findElement(DriverObject.driver,ele_name_drag), ElementRepository.findElement(DriverObject.driver,ele_name_DropTo));
	actions.perform();
	Log.info(ele_name_drag+ " drag and dropped on  "+ele_name_DropTo);
	}catch(Exception e)
	{
		Log.error(ele_name_drag+ " drag and drop on  "+ele_name_DropTo+ " Failed");
		throw new Exception(e);
	}
}

public static void sendKeys(String ele_name, Keys keys) throws Exception
{	
	try
	{SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
	wait = new WebDriverWait(DriverObject.driver,60 );
	wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator(ele_name)));
	wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator(ele_name)));
	ElementRepository.findElement(DriverObject.driver,ele_name).sendKeys(keys);
	Log.info("Send keys:"+keys +" to "+ele_name);
	}catch(Exception e)
	{
		Log.error("Send keys:"+keys +" to "+ele_name+ " Failed");
		throw new Exception(e);
	}
	
	
}

public static void sendKeys(String ele_name, String keys) throws Exception
{
	try
	{
	SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
	wait = new WebDriverWait(DriverObject.driver,60 );
	wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator(ele_name)));
	wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator(ele_name)));
	ElementRepository.findElement(DriverObject.driver,ele_name).sendKeys(keys);
	Log.info("Send keys:"+keys +" to "+ele_name);
	}catch(Exception e)
	{
		Log.error("Send keys:"+keys +" to "+ele_name+ " Failed");
		throw new Exception(e);
	}
	
}
public static void check(String ele_name) throws Exception
{
	
	try
	{
		
	
	SingleWebDriverObject DriverObject = SingleWebDriverObject.getInstance();
	wait = new WebDriverWait(DriverObject.driver,60 );
	wait.until(ExpectedConditions.presenceOfElementLocated(ElementRepository.findLocator(ele_name)));
	wait.until(ExpectedConditions.visibilityOfElementLocated(ElementRepository.findLocator(ele_name)));
	wait.until(ExpectedConditions.elementToBeClickable(ElementRepository.findLocator(ele_name)));
	ElementRepository.findElement(DriverObject.driver,ele_name).click();
	Log.info(ele_name+ "clicked");
	
	}catch(Exception e)
	{
		Log.error("Click on element: "+ele_name+ "failed");
		throw new Exception(e);
	}
	
}
}
